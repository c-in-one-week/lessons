---
layout: nonlesson
title: About
---

<!---
about/index.md   About page in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include content/other/about.md -%}
