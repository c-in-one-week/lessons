---
layout: lesson
title: Questions For The Teacher
pageTitle: C In One Week - Questions For Teacher
---

<!---
questions-teacher/index.md   questions for teacher in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

## {{ page.title }}

{% include lessonhead.html %}

{% include content/extra/q/comments.md %}
