---
layout: lesson
title: 6. Highway 66
---

<!---
6-highway-66/index.md   lesson 6 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/6/lesson.md -%}

{% include content/extra/6/comments.md %}
