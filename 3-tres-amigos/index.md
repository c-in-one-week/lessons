---
layout: lesson
title: 3. Tres Amigos
---

<!---
3-tres-amigos/index.md   lesson 3 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/3/lesson.md -%}

{% include content/extra/3/comments.md %}
