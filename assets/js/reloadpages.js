// reload instead of using browser cache if new site pages have been uploaded

// For each HTML file there is a separate file that contains a 32 bit hash
// of the file as 8 hex digits, plus a newline. The name of the hash file
// is the name of the HTML file with '-hash' before the '.html'. Each HTML
// file has a call to CWKSetupReload. The build process creates these hash
// files, and modifies the CWKSetupReload call so that the first argument
// is the name of the hash file, and the second argument is the 8 hex digit
// hash code value as a string for the HTML file being loaded. CWKSetupReload
// calls CWKReloadPage, which checks that the hash in the file matches
// the hash of the loaded file. If they do not match, then the file has been
// changed and a reload of the HTML file is performed, bypassing the cache.
// CWKReloadPage then continues to call itself every 10 seconds, so long
// as the page is visible, checking to see if new content for the page has
// been uploaded. If there is no mouse activity on the page, the interval
// gradually increases.
// This function
// uses sessionStorage to store the new hash code it is attempting to
// load for the page, and if it has already made the attempt it will not
// retry, avoiding infinite loops in case something goes wrong or the reload
// process does not actually avoid the cache for some particular browser.
//
// There is another mechanism for files in the assets directory tree, based
// on suffixing the file name with a number based on the hash code during
// the build process. Therefore there is no separate hash file or modificition
// of CWKSetupReload for any file in the assets directory tree. That is,
// this function reloads the HTML page being viewed. The assets,
// such as CSS and javascript, are cached separately. Reloading
// of the assets is performed by suffixing asset file names with a hash code
// based on the file content by the public_deploy.sh script during deployment
// of the public web site.
//
// The build process creates a file deploy.txt at the top level, containing
// the time in seconds since the epoch. The CWKReloadPage function reads
// this file before the hash file for the current page. The first time,
// it saves the build time. After that, if the build time has not changed,
// it does not bother reading the hash file for the current page.
// This is to reduce the load on the server if there are multiple simultaneous
// viewers of different site pages. The time is also used to increase the time
// between checks if the site has not been modified recently. At some point,
// the build time file might come from a server not optimized for static pages.
// The deploy.txt has the short commit SHA and CI_JOB_ID appended to the time.
//
// Also note that none of this is active during development, it is all set
// up by the Gitlab CI/CD deployment process controlled by .gitlab-ci.yml.
// In development, the loaded hash code argument to CWKSetupReload is null,
// disabling the check for autoload, and asset file names do not have
// the hash code suffix added.
// This should allow the developer to more directly interact with what they are
// developing. A developer needs to explicitly clear the cache in development,
// i.e. when using 'build_dev', whenever assets are changed. The 'build_dev'
// script uses the Jekyll livereload function, which reloads HTML bypassing the
// cache during development more effectively than 'CWKReloadPage'.

// thank you-
// stackoverflow.com/questions/1922910/force-browser-to-clear-cache#38290439

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


function CWKReloadPage(setTimeoutOnly) {
    function CWKlog(msg) {
      window.console && console.log(msg);
    }
    function CWKwarn(msg) {
      window.console && console.warn(msg);
    }
    function CWKdebug(msg) {
      window.console && console.debug(msg);
    }

    function CWKinvisible(spew) {
      if (typeof document.addEventListener === "undefined" ||
          typeof document.visibilityState === "undefined") {
        // require addEventListener so only check visibilityState
        // if can catch visibilitychange event to restart timer after invisible
        if (spew) {
          CWKdebug('CWKReloadPage- time: ' + Date() + ', visibility state not available');
        }
        return false;
      }
      const vState = document.visibilityState;
      if (spew) {
        CWKdebug('CWKReloadPage- time: ' + Date() + ', visibility: ' + vState);
      }
      return vState === "hidden" || vState === "unloaded";
    }

    function CWKclearTimeout() {
      if (typeof CWKReloadPage.CWKTimer !== "undefined"
          && CWKReloadPage.CWKTimer) {
        clearTimeout(CWKReloadPage.CWKTimer);
      }
      CWKReloadPage.CWKTimer = false;
      CWKReloadPage.CWKCheckMouse = false;
    }
    function CWKsetTimeout() {
      if (CWKinvisible(false)) {
        CWKclearTimeout();
        return;
      }
      // try again in tryInterval seconds if page not hidden
      var tryInterval = 10;
      // bump up the interval if there has not been a build for a while
      CWKReloadPage.CWKCheckVisibility = false;
      if (typeof CWKReloadPage.CWKbuildTime !== "undefined"
          && ! isNaN(CWKReloadPage.CWKbuildTime)
          && CWKReloadPage.CWKbuildTime != 0) {
        var bTime = Date.now() / 1000 - CWKReloadPage.CWKbuildTime;
        bTime = bTime / (60.0 * 60.0 * 24.0);
        if (bTime > 1) {
          const tx = 600.0 / (1.0 + 400.0 / bTime) - 5;
          // 4 day -> 0.9 sec, 1 wk -> 5 sec, 1 mon -> 37 sec
          // 6 mon -> 3 min, 1 yr -> 5 min, infinite -> 10 min
          if (tx > 0) {
  	    tryInterval = tryInterval + tx;
  	    if (tx > 5) {
  	      CWKReloadPage.CWKCheckVisibility = Date.now() + 1000*tryInterval;
  	    }
  	    if (tx > 30) {
              localStorage.setItem('CWK_check_build', Date.now() + 1000*tx);
  	    }
          }
        }
      }
      CWKReloadPage.CWKCheckMouse = Date.now() + 1000 * (tryInterval + 5);
      // bump up the interval if the mouse has not moved for a while;
      // on some browsers, visibilityState is not "hidden" even if
      // another window completely covers the browser window, and the only
      // way to know if the browser is active is if the mouse moves on it
      if (typeof CWKReloadPage.CWKMouseTime !== "undefined") {
        const mouseTime = ( Date.now() - CWKReloadPage.CWKMouseTime )/1000 ;
        if (mouseTime > 60) {
          const t1 = (mouseTime + 0.0) * mouseTime ;
          const t2 = (mouseTime + 0.0) * mouseTime * mouseTime ;
          const tDelta = 1 / ( 45.0 * Math.sqrt(1.0 / t1 + 50000.0 / t2) ) ;
          // 10min -> 11.5s, 1 hr -> 30s, 6 hr -> 5min, 24 hr -> 25 min
          tryInterval = tryInterval + tDelta ;
          CWKdebug('mouseTime: '+mouseTime+', tDelta: '+tDelta + ', tryInterval: '+tryInterval);
        }
      }
      CWKReloadPage.CWKTimer = setTimeout(CWKReloadPage, 1000 * tryInterval);
    }

    function CWKresponse(xhr, fName) {
      if (xhr.status == 0) {
        // typically from timeout
        CWKwarn("CWKReloadPage- Error reading '" + fName + "'");
        return false;
      }
      if (xhr.status !== 200) {
        CWKwarn("CWKReloadPage- Error reading '" + fName +
            "': " + xhr.statusText);
        return false;
      }
      const sResult = xhr.responseText && xhr.responseText.replace(/\s/gm, '');
      if (sResult == 0) {
        CWKwarn("CWKReloadPage- '" + fName + "' is empty");
        return false;
      }
      return sResult;
    }


    // the code for CWKReloadPage

    if (typeof setTimeoutOnly !== "undefined" && setTimeoutOnly) {
      CWKsetTimeout();
      return;
    }

    if (CWKReloadPage.CWKImRunning) {
      // not an error - visibility change or mouse while waiting for ajax read
      CWKdebug('CWKReloadPage: called while already running');
      return;
    }
    const sFile = CWKReloadPage.CWKsFile;
    if (! sFile || sFile == 0) {
      CWKwarn('CWKReloadPage- comparison file name missing');
      return;
    }
    const sLoaded = CWKReloadPage.CWKsLoaded;
    if (! sLoaded || sLoaded == 0) {
      CWKwarn('CWKReloadPage- comparison hash code value missing');
      return;
    }
    if (CWKinvisible(true)) {
      CWKclearTimeout();
      CWKdebug('CWKReloadPage- not visible at: ' + Date());
      return;
    }
    CWKReloadPage.CWKImRunning = true;
    const ckBuild = localStorage.getItem('CWK_check_build');
    const lclKey = 'CWK_pbuild_' + location.pathname;
    const pBuild = localStorage.getItem(lclKey);
    const cBuild = localStorage.getItem('CWK_curBuild');
    if (ckBuild && pBuild && cBuild) {
      // put off any build check if no builds for a long time (month or more)
      // -- put off until ckBuild which may be set by any page on site
      // -- put off by between 30 seconds and 10 minutes, continuously variable
      // -- if there are multiple tabs, the other tab could be incrementing
      // ckBuild and then this tab might not check because of that, but it will
      // also read the build info into cBuild and that is also checked here
      if (Date.now() < ckBuild - 1000 && pBuild == cBuild) {
        CWKclearTimeout();
        CWKReloadPage.CWKTimer = setTimeout(CWKReloadPage, ckBuild-Date.now());
        CWKdebug('CWKReloadPage- no build check until ' + ckBuild);
        CWKReloadPage.CWKImRunning = false;
        return;
      }
    }

    const xhrbuild = new XMLHttpRequest();
    xhrbuild.onreadystatechange = function() {
      CWKclearTimeout();
      if (xhrbuild.readyState != 4) {
        return;
      }
      CWKReloadPage.CWKbuildTime = 0;
      const buildTime = CWKresponse(xhrbuild, 'build.txt');
      if (buildTime != 0) {
        // store in localStorage for all pages, needed by CWK_check_build check
        localStorage.setItem('CWK_curBuild', buildTime);
        // buildTime is stored in localStorage once the hash code for that page
        // has been successfully read so that re-reading
        // the same page later will be able to check against it
        CWKReloadPage.CWKbuildTime = parseInt(buildTime, 10);
        if (pBuild === buildTime) {
          CWKdebug("CWKReloadPage- same build: '" + pBuild + "', not reading hash code");
          CWKsetTimeout();
          CWKReloadPage.CWKImRunning = false;
          return;
        }
      }
  
      const xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        CWKclearTimeout();
        if (xhttp.readyState != 4) {
          return;
        }
        CWKReloadPage.CWKImRunning = false;
        const sCurrent = CWKresponse(xhttp, sFile);
        if (sCurrent && sCurrent != 0) {
CWKdebug("setting localStorage '" + lclKey + "' to value '" + buildTime + "'");
          localStorage.setItem(lclKey, buildTime);
        }
        if (sCurrent && sCurrent != 0 && sLoaded != sCurrent) {
          const myPath = location.pathname;
          const stgKey = 'CWK_loading_' + myPath;
          if (sessionStorage.getItem(stgKey) == sCurrent) {
            // make one try to reload this page to the current sCurrent;
            // if the try fails, need this test to avoid infinite loop
            CWKwarn("CWKReloadPage- reloading '" + myPath + "' to code: " + sCurrent + " failed.");
            return;
          }
          sessionStorage.setItem(stgKey, sCurrent);
          CWKlog('CWKReloadPage- Reloading, Loaded Code: '+ sLoaded + ', Current: '+ sCurrent);
          // - load new page instead of using the cache
          location.href = location.href.replace(/#.*$/, '');
          location.reload(true);
          // reload(true) forces cache bypass on some,
          // resetting href needed for older firefox and IE (maybe)
        } else {
          CWKdebug('CWKReloadPage- Not reloading, Loaded Code: '+ sLoaded + ', Current: '+ sCurrent);
          CWKsetTimeout();
        }
        // this is the end
        // at the end of the state == 4 code of the second request
      }

      // this XMLHttpRequest is done second when first one is in state == 4
      // add timestamp to URL to bypass cache when reading sFile
      xhttp.open("GET", sFile + '?' + (Date.now() / 1000), true);
      xhttp.timeout = 2000; // no reload if no read of sFile in reasonable time
      xhttp.send();
      // at the end of the state == 4 code of the first request
    }

    // this XMLHttpRequest is done first, when CWKReloadPage is called.
    // add timestamp to URL to bypass cache when reading 'build.txt'
    xhrbuild.open("GET", '/build.txt' + '?' + (Date.now() / 1000), true);
    xhrbuild.timeout = 2000; // ignore 'build.txt' if not readable
    xhrbuild.send();
}

function CWKMouseCatch() {
    const curTime = Date.now();
    CWKReloadPage.CWKMouseTime = curTime;
    // if mouse moves and had no check for a while, check again
    if (typeof CWKReloadPage.CWKCheckMouse !== "undefined"
        && CWKReloadPage.CWKCheckMouse != 0
        && curTime >= CWKReloadPage.CWKCheckMouse) {
      CWKReloadPage();
    }
}

function CWKVisibilityChange() {
    const vState = document.visibilityState;
    if (vState === "hidden" || vState === "unloaded") {
      CWKReloadPage();
      return;
    }
    const curTime = Date.now() ;
    if (typeof CWKReloadPage.CWKCheckVisibility === "undefined"
        || CWKReloadPage.CWKCheckVisibility == 0
        || curTime >= CWKReloadPage.CWKCheckVisibility) {
      CWKReloadPage();
    }

    // if it has been a while since last build,
    // do not check for a new build every time page becomes visible
    CWKReloadPage(true);
}

function CWKSetupReload(sFile, sLoaded) {
    if (! sLoaded || sLoaded == 0) {
      window.console && console.log('CWKSetupReload: page hash code not set, no auto reload.');
      return;
    }
    if (! sFile || sFile == 0) {
      window.console && console.warn('CWKSetupReload: sFile is missing');
      return;
    }
    CWKReloadPage.CWKsFile = sFile;
    CWKReloadPage.CWKsLoaded = sLoaded;

    if (typeof document.addEventListener !== "undefined") {
        CWKMouseCatch() ; // avoid big initial tDelta from old value of this
        document.addEventListener('mousemove', CWKMouseCatch);
        if (typeof document.visibilityState !== "undefined") {
          document.addEventListener("visibilitychange", CWKVisibilityChange);
        }
    }

    CWKReloadPage();
}
