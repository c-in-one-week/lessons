---
layout: center
title: Source
pageTitle: C In One Week - Source
---

<!---
source/index.md   Source page in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

<center>
<h2 id="{{ page.title | replace: " ", "-"
 		      | downcase }}">{{ page.title }}</h2>
</center>

<br style="line-height:25%;" />

{% include content/other/source.md -%}
