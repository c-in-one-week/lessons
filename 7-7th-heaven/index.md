---
layout: lesson
title: 7. 7th Heaven
---

<!---
7-7th-heaven/index.md   lesson 7 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/7/lesson.md -%}

{% include content/extra/7/comments.md %}
