---
layout: lesson
title: 1. In The Beginning
---

<!---
1-in-the-beginning/index.md   lesson 1 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/1/lesson.md -%}

{% include content/extra/1/comments.md %}
