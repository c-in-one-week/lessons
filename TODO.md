The site is finished,
but nothing is ever finished.

### To Do list for C In One Week (c-in-one-week.com)

#### Ideas for Lesson 6

- Better examples in ASCII strings section
- Better examples in internationalize section
- Command line tools
    - condense the command line tools descriptions
    - summaries of command line tools descriptions
    - clearly state can code without command line tools
    - profiling tool, including memory and mem leakage
- Add Visual Studio Code section
    - maybe Codium and not MS VSC
    - maybe clangd and clang-tidy, not MS intellisense
    - supported systems for Native Debug extension
    - pick tool to verify or automate Makefile dependencies
    - profiling extension, including memory and mem leakage
- Add organized C library functions section
    - standard I/O
    - strings
    - character classification
    - exit, system, popen, etc
    - networking
    - system calls
    - and so on
- Add section about using _found_ open source code.
- Add code samples section, with links to other lessons

#### Lesson 7

Completely rewrite lesson 7 - 7th Heaven.
It should be much shorter.
It should have a quick recap of all 7 days.
It should lead people to figure out their own project ideas
and which ones make sense to actually do first for learning C.
It should not end with `Done`, but with an open ended
discussion to help people get started with their own projects.
It should inspire people to write awesome code.

#### Old ideas for things to fix, listed on the site Source tab

- Little gaps (sometimes big ones) here and there on the rendered pages
- The icon was me playing at favicon.cc for 30 minutes
   - No particular reason it was created online or at that site
   - The blue circle around the C is not bold enough
   - The C is not really a very good C
   - Things should be drawn from some viewing perspective with shading
   - The first and last animation frames are not as bad as the middle seven
      - The green slush going around the C does not step evenly each frame
      - The green slush does not show motion well in each frame
      - In each frame, older green slush should be rounder against the circle
- Styling, especially inline code and the lessons in general, could be better
   - Fonts, font sizes, colors
- Spelling and grammar
- Shall I go on for hours regarding how I strive to be concise?
- Rethinking how the top navigation scrolls off the screen
- Scrollbar placement
- Responsive - check for smaller page size e.g. a phone, adjust accordingly

#### New ideas from a couple of weeks ago (July 2024)

Hello! tab, talk about Hello World at top, comment tab with comment box that sends to domain name email, allows replies including to replies, radio button pair each for public and notify, scripts for site owner to look at current state of comments on site, current state of comments in dev dir, and comment emails and allows easy post or remove post, if pushing post first time send notification if enabled to poster and all levels of replied posts

I think the top bar should end with "Hello! Hi!", where Hi! is the comment page, I guess by "Hello World" I meant something about the history of C it's been a couple of weeks it was the middle of the night when I wrote this

Admin tab, with button to send to domain name email all instructions for site maintenance to site admin email

Code tab, link to possibly long list of sample/example/study projects. Use keywords to cut down list, such as “featured” or “basic” (the default), “advanced”, etc. In addition to On/Off keywords have keyword sets that are select 1 of n. For each project: title line which is line in list, longer description of what it does, details of how to build install use, the code. Something on the top of how to submit your own code or do a pull request for an existing project. All parts of project would be in a git tree, perhaps a particular named file gives title, description, source of project, etc. Somehow make sure all is open source.

Expand Resources tab, with 1 paragraph descriptions each, for further info such as more classes, C specs, the Linux coding style standard, lists of other languages and how they relate to C, lists of major systems using C such as Linux and iOS if you count objective C, getting a job in C (meetup, indeed, linked in), getting a C coder for a job (the same as getting a job, upwork, …)

Somewhere put in my basic philosophy: if you want features that are really for some other language, you might as well use the other language, 80 is a nice line length with 8 tabs but tabs just should be consistent, ~~it is more important for C to be understandable by someone not versed in every new coding language feature than to be a way for kids to show off their esoteric knowledge~~

Review everything!

Little things:

- make sure pointers stand out from the beginning
- make sure arrays introduced sensibly
- make sure doesn’t say float promotes to double
- say recursion may not be the most efficient, eg for processing hierarchical trees, but it is a great simple way to process trees of arbitrary size until the stack runs out
- say macros are used for efficiency sometimes but silly to do this if no real gain
- say profile vs guess
- say check the assembly
- specifics about where to go for more, e.g. C++, GUI

#### Random stuff

Some people may not be pleased with the inconsistency between the basic page<br>
format of the lesson pages and the others. Well, consistency is the hob-goblin<br>
of small minds, says someone with plenty of small-minded experience.

Please let me know and push when you make it better!
My email account: info, domain: c-in-one-week.com

Lesson content is licensed under CC-BY 4.0
This text (TODO.md) is licensed under CC-BY 4.0
Development scripts are licensed under GPL-v2
