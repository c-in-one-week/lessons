---
layout: nonlesson
title: Resources
---

<!---
resources/index.md   other learning resources related to c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include content/other/resources.md -%}
