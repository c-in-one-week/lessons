linked lists
arrays
of characters, ints, doubles, pointers to structures, structures
of pointers vs arrays and arrays
argc / argv
malloc / free / realloc

C preprocessor
/** #define sizes for arrays, how #include works, #ifdef/#if/#endif **/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int x ;
	double fact(int somenum) ;
	struct factoid {
		struct factoid *nextfact ;
		int num ;
		double numfact ;
	} *factlist = NULL, *lastfact = NULL ;
	
	while (printf("enter x: ") , fflush(stdout), scanf("%d", &x)) {
		double y =   fact(x) ;
		struct factoid *f ;
		if (! (f = (struct factoid *)malloc(sizeof(struct factoid)))) {
			fprintf(stderr, "malloc failed\n") ;
			exit(1) ;
		}
		f->nextfact = NULL ;
		printf("x = %d fact(x) = %f\n", x, y) ;
		f->num = x ;
		f->numfact = y ;
		if (lastfact) lastfact = lastfact->nextfact = f ;
		else lastfact = factlist = f ;
	}
	if (factlist) {
		struct factoid *f ;
		printf("factorials just computed:") ;
		for (f = factlist; f; f = f->nextfact)
			printf("  %d! = %f", f->num, f->numfact) ;
		printf("\n") ;
	}
	return 0 ;
}

/****
	fact(int n)	- factorial
	This algorithm is slow- a loop would be much faster.
****/
double fact(int n)
{
	if (n <= 0) {
		fprintf(stderr, "can't take factorial of non-positive number\n") ;
		exit(1) ;
	}
	if (n == 1) return 1 ;
	return n * fact(n - 1) ;
}
