#include <stdio.h>
#include <stdlib.h>

struct element {
    struct element *next ;
    float somedata ;
} ;

int main()
{
    struct element *pfirst, *plast ;
    int neednewlist = 1 ;
    float newval ;

    while (printf("next value: "), fflush(stdout), scanf("%f", &newval) == 1) {
        if (neednewlist) {
            neednewlist = 0 ;
            pfirst = plast = malloc(sizeof *pfirst) ;
            plast->next = NULL ;
        } else {
            plast->next = malloc(sizeof *plast->next) ;
            plast = plast->next ;
            plast->next = NULL ;
        }

        plast->somedata = newval ;

        float total = 0.0 ;
        for (struct element *p = pfirst; p != NULL; p = p->next) {
            printf("%.2f", p->somedata) ;
            if (p->next != NULL) printf(" + ") ;
            total = total + p->somedata ;
        }
        printf(" = %.2f\n", total) ;
    }
}
