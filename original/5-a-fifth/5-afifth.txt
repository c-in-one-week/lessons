A Fifth

Today's lesson directly builds on yesterday's lesson, Four Square. If you have any doubts about yesterday's lesson, if you haven't gone through it completely, it would be good to let me know before starting today's lesson. Yesterday's lesson had 9 compilable code examples. I copied each one into a compiler and ran them all before posting the lesson, so they should all work. Hopefully you understand them all by now, hopefully they all worked for you.

Much of this lesson was originally planned to go into Four Square, but I decided while writing it to make Four Square focus on the basics, so you would have a solid understanding of them before you went on. So even though Four Square was long, I am afraid this one may be even longer. Sorry. Almost done with the whole class, and the last two lessons are shorter.

**This Lesson**

This lesson is about "making a bunch of something". There are multiple ways to put together the bunch. There are multiple ways to access the bunch. There are multiple ways to access each thing put into the bunch. I will not cover all of these ways, just the basics. Hopefully, the concepts should be clear, so that extending these basics later, when you need to for some particular project, is easy.

When you have "a bunch of something", in C the "something" is multiple of the same type of thing. In many other languages, the things do not have to be the same type. Technically, you can cheat in C and bunch together things that are not the same type also, using various constructs. I have found this to almost always be a very bad idea! The C semantics does not really support this. It can work, on one system, but if the code is moved to another system it may not work. What if the hardware on the one system where it originally worked is upgraded, and your code stops working? That is no fun. What if you end up eventually adding a lot more features than you initially expected, you put a lot more effort into your code than you thought you would, then you realize you can't move that code to another machine because you did not follow C semantics, and you are sad because you can't use that code you put so much effort into on the machine you now want to use. I don't want you to be sad, so a way to avoid that is only to bunch things of the same type in C. If you are working on a project where you really need to bunch things of different types, let me or some other advanced programmer know, and they can suggest a better language for doing that. If you do bunch things of different types in C, let me know and I will try to point out what I suspect might happen in various cases, and we can all learn from the experience.

There are two ways to "make a bunch of something". The first way is to just make an array. When the "things" being bunched are simple, that is usually the best way. Once the things get complicated, then there are other ways using 'struct'. The first part of the lesson will be about arrays, the second part about the other ways. Btw, it is possible to make a simple array of 'struct', there is an example of that in the first part.

**Arrays**

In C, arrays are intimately associated with pointers. Huh??? you say.

An array is just a bunch of variables of a type, one after the other. If you have a pointer to one of the variables in the array, then you just add '1' to the pointer, and you now have a pointer to the next variable. If you subtract '1' from the pointer, you go back to the previous variable. The variables in arrays are called 'elements'. Getting a pointer to the particular element you want by adding or subtracting is the key to access array elements. Once you have a pointer to the right element, you can just use '*' on the pointer to work with the value of that particular element. Simple, once you see it.

One way to create arrays is to declare an array variable. Individual array elements do not have names, you just access them by pointing at them. An array variable is made up of array elements, and the whole array, the array variable, is given a name when you declare it. In C, the name of the array variable is converted to a pointer to the first element whenever it is used. An array variable is declared by putting '[' *number-of-elements* ']' after the variable name. The following declares an array of 10 'int'.
`	int x[10] ;`

So, how can I access that? Using just 'x' gets a pointer to the first element, so
`	*x = 5 ;`
will set the first element to be five. If you want to second the second element to be 6, use
`	*(x+1) = 6 ;`
To set the fifth element to be 42, use
`	*(x+4) = 42 ;`
Note that you have to subtract one from which element you want. `*x` is the first, `*(x+1)` is the second, `*(x+4)` is the fifth, and so on. This is inherent from the definitions as given in the previous paragraphs, if 'x' becomes a pointer to the first element, then adding one goes on to the next, `*(x+1) is the second. Nothing complicated here, it's just the way it works.

So, how is this used in real programs? Do people type `*(x+4)`? No, they use a pointer variable to the same type as the array elements.
`	int *my_pointer_to_int ;

	my_pointer_to_int = x + 4 ;
	*my_pointer_to_int = 42 ;`
Why? It is much more efficient to set the pointer variable once, then use it multiple times, instead of having the computer determine the element location every time. Also, going on to the next element is very fast machine-wise. Here is an example:
`	for (my_pointer_to_int = x; my_pointer_to_int < x+10; ++my_pointer_to_int)
		*my_pointer_to_int = 42 ;`
Boom! In two lines, I just set the whole array to 42. Note that pointers to array elements when  there are 10 elements start at 'x' and end at 'x+9', so 'x+10' is actually one past the end. Hot poker alert: in simple loops like the above, if you are off by one at the end of the array it will likely crash pretty soon afterwards. Whatever variable happens to be just after the array elements will be smashed, and hopefully you will notice right away. However, for something more complicated, it may not crash until Steve Jobs is demoing the program on live TV (has happened). So be careful with your array end calculations: it is easy to get them off by one, and not a good thing at all.

I usually do the above loop from the end of the array back to the beginning, because the condition expression that ends the loop is done every time around, so the above loop code calculates 'x+10' every time around. Much faster to just calculate the far size of the array once:
`	for (my_pointer_to_int = x + 9; my_pointer_to_int >= x; my_pointer_to_int--)
		*my_pointer_to_int = 42 ;`
However, for modern optimizing compilers, it probably makes little difference. They likely notice that the loop end condition is calculated many times, and do it once first and save the value in a register. But the modern optimizing solution uses up a machine register, so my way is still a little better. It is up to your judgement as to whether my way from end to beginning is less clear than the other way from beginning to end, and to whether the slight efficiency gain is worth it.

Note that the above 'for' loop can be written using 'while', and it is exactly the same thing.
`	my_pointer_to_int = x + 9 ;
	while (my_pointer_to_int >= x) {
		*my_pointer_to_int = 42 ;
		my_pointer_to_int-- ;
	}`
Remember that the post-decrement operator returns the old value, before it was decremented? This loop is always written using that value by experienced programmers.
`	my_pointer_to_int = x + 9 ;
	while (my_pointer_to_int >= x) {
		*my_pointer_to_int-- = 42 ;
	}`
Using the value of the post-decrement like this is usually much more efficient, because for many data types the machine has special hardware to implement the decrement for this case. In this case, the machine performs the post-decrement and the assignment to 42 at the same time, running much faster.

Another way to write this same thing uses the pre-decrement instead of the post-decrement.
`	my_pointer_to_int = x + 10 ;
	while (my_pointer_to_int > x) {
		*--my_pointer_to_int = 42 ;
	}`
That is probably the way I would write it. On some machines, because of the way the special hardware works, pre-decrement is faster than post-decrement. However, in other loops with pointers going up instead of down, post-increment, `*my_pointer_to_int++ = 42 ;` is almost always faster, but that does not apply in this case because we are going down because it is slightly more efficient in this case, and personally I am used to doing it this way in this case. There are other cases where going up is more efficient, we will see them shortly.

There is another syntax for accessing an array value, just 'x[4]' to access the 5th element. This syntax is identical in meaning to `*(x+4)`, which also accesses the 5th element. When the '4' is not a constant, e.g. if you want to set a whole array to 42 like the above example, using this syntax is several times less efficient than just using pointers like in the above example. 'x[i]' is just so much slower than '*my_pointer_to_int', where the pointer uses an increment or decrement operator to get to each element. In fact, most of the time real programs just don't use the '[' ']' syntax for accessing array elements at all. They could, and with a constant value inside the '[' ']' it is not that much less efficient, but it is so much less efficient than just '*' pointer for variables values inside the '[' ']' that people stay away from this. I have seen plenty of code that uses `*(x+4)` or some other constant. Writing it this way is consistent with the pointer arithmetic which is the only efficient way when the element being selected is not constant, so many people just always write it this way.

Now for a bit of a digression.

From the 'four-square' lesson, we learned that NULL is just 0. How is this set up? How do people normally do constants in C?

`#define NULL 0`

Easy. The actual definition of NULL is typically a little more complicated as I will show later in this lesson, but it is still done with a '#define'. The '#' character has to be the first character on a line of C source. The '#define' line is not really part of C semantics, it just sets up a simple text macro, whenever you type NULL, after that '#define' the compiler will see 0. You can '#define' any word to be any combination of digits, letters, punctuation, whatever. It is just a simple text substitution of a word with what you '#define' the word to be using the end part on the '#define' line.

Why do I bring up '#define' now? Because it is a bad idea to put an array size all over your code. What if you want to change the size from 10 to 20, if 10 turns out not to be big enough? So, use '#define'. Here is a complete code example. Sorry that it has no output.

`#include <stdio.h>
#include <stdlib.h>

#define XSIZE	10

int main()
{
	int x[XSIZE] ;
	int *my_pointer_to_int ;

	my_pointer_to_int = x + XSIZE ;
	while (my_pointer_to_int > x) {
		*--my_pointer_to_int = 42 ;
	}

	return 0
}`

That brings up a question. What are the '#include' lines? A long time ago, the '#define' operation was done by a whole other program that was run before the C compiler. The other program was called the "preprocessor". The '#include' lines are just lines that tell the preprocessor to insert the contents of some other file at that location. If the file name is sourounded by '<' '>', then the preprocessor looks in some standard system folders for the other file. So, 'stdio.h' is just a C source file in some system folder that declares many functions you might want to use, such as 'printf' and 'scanf', and many other things as well. If you want to write your own header files, say "myheader.h", just include it as `#include "myheader.h"`. A good place to put the '#define' for XSIZE, and many other '#define', is in a header file you create. That way it is available to any source file that needs to use XSIZE by using '#include' of that header file. I will show examples of this in a future lesson.

Going on. Can arrays be made out of other types? Of course! Here is an entire sample program that inputs an array of float heights, then calculates the average height. Note, you could do this more efficiently by calculating the height total each time a height is read, without arrays, but that would not be a good example for this lesson.

`#include <stdio.h>
#include <stdlib.h>

#define MAXHT	10

int main()
{
	float ht[MAXHT] ;
	float *htptr = NULL ;	/* always good to initialize pointers */
	int numht = 0 ;	/* start with no heights in the array */
	float nextht ;
	double htotal ; /* C actually does float calculations in double,
			   so why not save the extra already calculated bits? */

	htptr = ht ;
	while (printf("enter another height: ), fflush(stdout), scanf("%f", &nextht)) {
		if (numht > MAXHT) {
			fprintf(stderr, "Maximum of %d heights exceeded.\n", MAXHT) ;
			exit(1) ;
		}
		*htptr++ = nextht ;
		++numht ;
	}

	htotal = 0 ;
	while (htptr > ht) {
		htotal += *--htptr ;
	}

	printf("The average of these %d heights is %1.2f.\n", numht, htotal / numht) ;

	return 0 ;
}`

There is a big hot-poker issue here. Think! about the top and bottom conditions. When writing code like this, make double sure you never use a pointer at an array element when it is past the top or bottom of the array. Both reading and writing can cause problems, but writing is the worst.

So far, we have always used variables defined inside functions. I have mentioned that you can define variables outside any function, by placing the variable definition outside any function definition, but this can cause huge code maintenance problems when somebody in some source file modifies such a variable and somebody in some other place doesn't realize it. It is much better to just create a 'struct' with all your variables that you might want to use in multiple functions, and just pass a pointer to the 'struct' between the functions. People normally do it this way, so compilers are optimized for it, the generated code is frequently faster and tighter when you pass variables as structure members than when you use named variables outside of functions.

There is a third way of making variables. This ways is particularly useful for arrays.

There are four main parts in the memory space of any process. There is the code itself. It usually can't be modified, it is set up when the process starts running some executable. There are the statically defined variables, such as the ones defined outside any function, which are not used very much at all and can cause big problems when they are used as mentioned above. There are the variables defined in functions. Then, there is this other space. For big complicated programs it usually beomces the biggest space.

This other space is called the "dynamic memory". It is called dynamic, because a process can get more of it dynamically, at any time, until the whole computer runs out of memory space. Doing this allows you to create new variables. Please, don't let your mind be blown by this; it is very simple, once you have done it a few times. The new variables are not named, they are always accessed through pointers. The main way to get "dynamic memory" is through the library routine called 'malloc'.

The return from 'malloc' must be casted into the correct pointer type for the variable you want to create. This is the only good time to cast from one pointer type to another. The 'malloc' function is very careful to return a value which can be cast to any type of pointer. Otherwise, you can easily generate non-portable code, or code that causes mysterious crashes.

Here is an example of 'malloc' to create a new 'int' variable.

`	*my_pointer_to_int = (int *)malloc(sizeof(int)) ;`

The case to the 'int' pointer is done with the `(int *)` construct. The 'sizeof' tells 'malloc' how much memory space to get.

So, you can use this 'int' pointer just like any other.

`	*my_pointer_to_int = (int *)malloc(sizeof(int)) ;
	*my_pointer_to_int = 5 ;
	printf("new value: %d\n", *my_pointer_to_int) ;`

One cool thing about dynamic memory is that the created variable still exists even when the function calling malloc returns. Here is a complete sample program.

`#include <stdio.h>
#include <stdlib.h>

int *make_a_new_int()
{
	int *my_pointer_to_int ;
	my_pointer_to_int = (int *)malloc(sizeof(int)) ;
	if (! my_pointer_to_int) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	return my_pointer_to_int ;
}

int main()
{
	int *pointer_to_int_in_main = NULL ;

	pointer_to_int_in_main = make_a_new_int() ;
	*pointer_to_int_in_main = 5 ;
	printf("new value: %d\n", *pointer_to_int_in_main) ;

	return 0 ;
}`

Note that 'malloc' returns NULL (0) when the computer is out of memory space. Always a good idea to check for this.

The variables in the new space returned by 'malloc' are not initialized. They may be all 0 in your test cases, then when running the real program suddenly be something else. Do not asume the value of the variables in the new space are anything until you set them to something, the same as you have to set variables defined in a particular function to some value before you know they have a well defined value.

Let's put these concepts together. What if you don't want just a single 'int', but an array of 'int'? No problem at all, just multiply the 'sizeof' the single array element, by the number of elements you want. An array in C is always memory space of the size of the each array element, times the number of elements.

`#include <stdio.h>
#include <stdlib.h>

int *make_a_new_int_array(int nelem)
{
	int *ptr ;
	if (! (ptr = (int *)malloc(nelem * sizeof(int))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	return ptr ;
}

int main()
{
	int *int_array_ptr = NULL ;

	int_array_ptr = make_a_new_int_array(10) ;
	int_array_ptr += 4 ;	/* go to the fifth element */
	*int_array_ptr = 6 ;
	printf("new value for 5th element: %d\n", *int_array_ptr) ;

	return 0 ;
}`

Ok, then. Let's modify our average height program to allow the user to set the maximum number of heights. It's easy using 'malloc'!

`#include <stdio.h>
#include <stdlib.h>

int *make_floatarray(int nelem)
{
	float *ptr ;
	if (! (ptr = (float *)malloc(nelem * sizeof(float))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	return ptr ;
}

int main()
{
	float *ht ;
	int maxht ;
	float *htptr = NULL ;	/* always good to initialize pointers */
	int numht = 0 ;	/* start with no heights in the array */
	float nextht ;
	double htotal ; /* C actually does float calculations in double,
			   so why not save the extra already calculated bits? */

	if (printf("enter maximum number of heights: "), fflush(stdout), ! scanf("%d", &maxht)) {
		fprintf(stderr, "Maximum number of heights must be a number.\n") ;
		exit(1) ;
	}
	if (maxht <= 0) {
		/* malloc code will not work right if array size is not positive */
		fprintf(stderr, "Silly maximum number of heights: %d.\n", maxht) ;
		exit(1) ;
	}

	ht = make_float_array(maxht) ;

	htptr = ht ;
	while (printf("enter another height: ), fflush(stdout), scanf("%f", &nextht)) {
		if (numht > maxht) {
			fprintf(stderr, "Maximum of %d heights exceeded.\n", maxht) ;
			exit(1) ;
		}
		*htptr++ = nextht ;
		++numht ;
	}

	htotal = 0 ;
	while (htptr > ht) {
		htotal += *--htptr ;
	}

	printf("The average of these %d heights is %1.2f.\n", numht, htotal / numht) ;

	return 0 ;
}`

There is another function called 'calloc'. It is just the same as malloc, except it clears the memory to zero before it returns. Some people like 'calloc', since if they forget to initialize the memory, it will be initialized the same way every time and the program will crash the same way every time. Also, if you need the memory to be zeroed, then 'calloc' is frequently much more efficient that using 'malloc' and clearing it yourself. The 'calloc' function takes two arguments, first the number of elements, the second is the element size, but internally it is pretty much the same as 'malloc' with the number of elements and element size multiplied together. Up to your judgement which one to use.

There is another function called 'free'. You can use 'free' on anything returned from 'malloc' or 'calloc' when you are done with it, and the space will be reclaimed for any later memory needed in the same process. When a process exits, e.g. when 'main' returns, all of the memory used in that process is automatically returned so that other processes can use it, even if the process never calls 'free'.

There is another function called 'realloc'. It is used to make dynamically allocated arrays longer, e.g. you can start out with the array being 10 elements long, then if you fill that up make it 20, then if you fill that up make it 40, and so on. Or whatever you want, make it 5 more elements longer each time. We will not be using 'realloc' in these lessons, it is not used very much, but it can come in very handy if you are not sure how long to make an array when you first allocate it. Note, 'realloc' is a very hot poker item if you have pointers at or into any of the space that has been dynamically allocated, because sometimes it has to move the memory space to make it bigger, and a pointer at or into it then points at the wrong variable. Crash. Oops. Hopefully it will crash immediately, and not fail in front of an audience of millions of your peers. Be careful when using 'realloc'!

You can make an array of structures. For example:

`struct simple_date twodates[2] ;`

Then, to access the first element, use `*twodates`, or for a 'struct simple_date' pointer at the first simple_date, just use `twodates`. For a 'struct simple_data pointer at the second simple date, just use `twodates + 1`. So simple. You could redo the Four Square homework assignment using this array, but I think you probably get it so no need.

However, this is not how programmers handle multiple structs. Usually, they make an array of pointers to the structs, and then dynamically allocate the structs one at a time.

`struct simple_date *make_a_date()
{
	struct simple_date *ptr ;

	if (! (ptr = malloc(sizeof(struct simple_date)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	return ptr ;
}

int main()
{
	struct simple_date *twodates[2] ;

	*twodates = make_a_date() ;
	*(twodates+1) = make_a_date() ;

	... use them pointers for something in the rest of the function ...`

It depends on the algorithm whether an array of 'struct', or an array of pointers to that 'struct', is more efficient. For sorting based algorithms, the array of pointers is usually more efficient. Otherwise, it requires thought, based on what the algorithm is doing.

Here is the homework assignment from yesterday, using an array of pointers for twodates.

`#include <stdio.h>
#include <stdlib.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

struct simple_date *make_a_date()
{
	struct simple_date *ptr ;

	if (! (ptr = malloc(sizeof(struct simple_date)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	return ptr ;
}

void date_print(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_print\n") ;
		exit(4) ;
	}

	printf("years = %d months = %d days = %d\n",
		date_ptr->year, date_ptr->month, date_ptr->day) ;
}

void date_scan(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_scan\n") ;
		exit(4) ;
	}

	if (printf("enter year: "), fflush(stdout), ! scanf("%d", & date_ptr->year)) {
		fprintf(stderr, "invalid year entry\n") ;
		exit(1) ;
	}
	if (printf("enter month: "), fflush(stdout), ! scanf("%d", &date_ptr->month)) {
		fprintf(stderr, "invalid month entry\n") ;
		exit(1) ;
	}
	if (printf("enter day: "), fflush(stdout), ! scanf("%d", &date_ptr->day)) {
		fprintf(stderr, "invalid day entry\n") ;
		exit(1) ;
	}
}

void twodates_scan(struct simple_date **twodates_ptr)
{
	printf("Enter first date (day1):\n") ;
	date_scan(*twodates_ptr) ;

	printf("Enter second date (day2):\n") ;
	date_scan(*(twodates_ptr + 1)) ;
}
`
void twodates_subtract(
	struct simple_date *d_result,
	struct simple_date **twodates_ptr
    )
{
	struct simple_date *d1 = NULL ;
	struct simple_date *d2 = NULL ;

	if (! d_result || ! twodates_ptr) {
		fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
		exit(4) ;
	}

	d1 = *twodates_ptr ;
	d2 = *(twodates_ptr + 1) ;
	if (! d1 || ! d2) {
		fprintf(stderr, "software error: NULL pointer in argument to date_subtract\n") ;
		exit(4) ;
	}

	d_result->year = d2->year - d1->year ;
	d_result->month = d2->month - d1->month ;
	d_result->day = d2->day - d1->day ;
	if (d_result->day < 0) {	/* fix negative days */
		-- d_result->month ;	/* use the decrement operator on the month */
		d_result->day += 31 ;	/* get rid of silly looking negative number */
	}
	if (d_result->month < 0) {	/* fix negative months */
		-- d_result->year ;
		d_result->month += 12 ;
	}
}

int main()
{
	struct simple_date dur ;
	struct simple_date twodates[2] ;

	twodates[0] = make_a_date() ;
	twodates[1] = make_a_date() ;

	twodates_scan(twodates) ;

	twodates_subtract(&dur, twodates) ;

	printf("Change from day1 to day2\n") ;
	date_print(&dur) ;

	return 0 ;
}`

**A Way To Make A Bunch Of Stuff Using Struct**

In C, the following concepts are used more than arrays.

A variable of type 'struct' can link to another variable of type 'struct' just by having a pointer to that other variable. This is particularly useful of the second variable is actually the same type of 'struct', because then it can have a link to another one, and so on and on, and you can make an arbitrarily long list of linked variables. These linked 'struct' are generally not made of variables defined in a particular function, they are generally made of dynamically allocated 'struct'. A list of linked 'struct' is called a linked list. Here is an example.

`struct my_list_struct {
	struct my_list_struct *nextptr ;
}`

And that's it. The 'struct' has a pointer to another variable of the same type of 'struct' as itself. Mind blowingly simple, once you see it.

The following example makes such a list.

`struct my_list_struct {
	struct my_list_struct *nextptr ;
}

struct my_list_struct *make_list_struct()
{
	struct my_list_struct *ptr ;
	if (! (ptr = malloc(sizeof(struct my_list_struct)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	/* always a good idea to initialize pointers to NULL,
	   especially pointer members in a 'struct' */
	ptr->nextptr = NULL ;
	return ptr ;
}

main()
{
	struct my_list_struct *p ;

	p = make_list_struct() ;
	p = p->nextptr = make_list_struct() ;
	p = p->nextptr = make_list_struct() ;
	p = p->nextptr = make_list_struct() ;
	p = p->nextptr = make_list_struct() ;

	......
}`

This example makes a 5 long linked list. Looks a little dumb: why not just use an array? Well, what if you don't know how many elements to use in the first place? Here is a complete example.

`#include <stdio.h>
#include <stdlib.h>

struct someht {
	struct someht *nextht ;
	double ht ;
} ;

struct someht *make_ht()
{
	struct someht *ptr ;

	if (! (ptr = malloc(sizeof(struct someht)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	ptr->nextht = NULL ;
}

int main()
{
	struct someht *htlist = NULL ;
	struct someht *htptr = NULL ;
	int numht = 0 ;	/* start with no heights in the list */
	float nextht ;
	double htotal ;

	while (printf("enter another height: ), fflush(stdout), scanf("%f", &nextht)) {
		*htptr = make_ht() ;
		htptr->ht = nextht ;
		htlist = htlist->nextht = htptr ;
		++numht ;
	}

	htotal = 0 ;
	for (htptr = htlist; htptr; htptr = htptr->nextht) {
		htotal += htptr->nextht ;
	}

	printf("The average of these %d heights is %1.2f.\n", numht, htotal / numht) ;

	return 0 ;
}`

This code is simpler than the original array based code, once you get used to linked lists. It is very flexible, it is possible to remove something from the list just by adjusting some pointers if your algorithm needs that, or insert something new. And, there is no limit to the number of heights being entered, until there is no more memory space for the entire computer.

Frequently linked lists have a pointer to a next element and a previous element. That allows the same list to be processed either from the first to the last, or from the last to the first, or in one direction at one moment then in the other direction a moment later, as needed by the algorithm. Here is an example of this. In the case of a doubly-linked list, the list is usually like a circle, where each element goes on to the next, until you go around the whole circle back to the beginning. Typically, doubly-linked lists have an unused element at the beginning to indicate the beginning of the list. This makes looping over the entire list easier, once you get to the usused element at the beginning, you know you have gone all the way around and you are done.

`struct someht {
	struct someht *nextht ;
	struct someht *prevht ;
} ;

struct someht *make_new_htlist()
{
	struct someht *ptr ;

	if (! (ptr = malloc(sizeof(struct someht)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	
	/* set up unused element for the head of the list */

	/* since there is nothing else, and going around a doubly-linked list
	   gets back to where you started in either direction,
	   both the next and prev pointers point back at the head element itself */
	ptr->nextht = ptr->prevht = ptr ;

	return ptr ;
}

void insert_ht(struct someht *htlist)
{
	if (! htlist) {
		/* there should always be at least the unused element at the head of the list */
		fprintf(stderr, "Software error: inserting into non-existent list.\n") ;
		exit(4) ;
	}

	if (! (ptr = malloc(sizeof(struct someht)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	
	/* set the next and previous values for the new element */
	/* previous before the new is the old value last value just before the unused head */
	ptr->prevht = htlist->prevht ;
	/* next value after the new is the unused head */
	ptr->nextht = htlist ;
	
	/* set next pointer of old list element at end of list just before unused head */
	htlist->prevht->nextht = ptr ;

	/* set prev pointer of unused head element */
	htlist->prevht = ptr ;
}`

All of this code will complicate a simple program like our example. We don't care which order the list elements are traversed in this case. Not only can doubly-linked lists be traversed in either direction, it is also easier to manipulate them, such as moving the elements around in them. So, for more complicated examples, a little overhead code, like setting up doubly-linked list management functions such as these, can go a long way in simplifying the overall code itself. Here is the example of doubly-linked lists.

`#include <stdio.h>
#include <stdlib.h>

struct someht {
	struct someht *nextht ;
	struct someht *prevht ;
	double ht ;
} ;

struct someht *make_new_htlist()
{
	struct someht *ptr ;

	if (! (ptr = malloc(sizeof(struct someht)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	
	/* set up unused element for the head of the list */

	/* since there is nothing else, and going around a doubly-linked list
	   gets back to where you started in either direction,
	   both the next and prev pointers point back at the head element itself */
	ptr->nextht = ptr->prevht = ptr ;

	return ptr ;
}

struct someht *insert_new_ht(struct someht *htlist)
{
	if (! htlist) {
		/* there should always be at least the unused element at the head of the list */
		fprintf(stderr, "Software error: inserting into non-existent list.\n") ;
		exit(4) ;
	}

	if (! (ptr = malloc(sizeof(struct someht)))) {
		fprintf(stderr, "Out of memory.\n") ;
		exit(3) ;
	}
	
	/* set the next and previous values for the new element */
	/* previous before the new is the old value last value just before the unused head */
	ptr->prevht = htlist->prevht ;
	/* next value after the new is the unused head */
	ptr->nextht = htlist ;
	
	/* set next pointer of old list element at end of list just before unused head */
	htlist->prevht->nextht = ptr ;

	/* set prev pointer of unused head element */
	htlist->prevht = ptr ;

	return ptr ;
}

int main()
{
	struct someht *htlist = NULL ;
	struct someht *htptr = NULL ;
	int numht = 0 ;	/* start with no heights in the list */
	float nextht ;
	double htotal ;

	htlist = make_new_htlist() ;
	while (printf("enter another height: ), fflush(stdout), scanf("%f", &nextht)) {
		htptr = insert_new_ht(htlist) ;
		htptr->ht = nextht ;
		++numht ;
	}

	htotal = 0 ;
	for (htptr = htlist->nextht; htptr != htlist; htptr = htptr->nextht) {
		htotal += htptr->nextht ;
	}

	printf("The average of these %d heights is %1.2f.\n", numht, htotal / numht) ;

	return 0 ;
}`

There are many other ways to group bunches of things using pointers inside of a 'struct' to the same type of 'struct'. A singly-linked list is just a linear structure from the beginning to the end, and a doubly-linked list is usually set up as a ring structure. Also frequently seen are trees, where each 'struct' element has multiple child 'struct' elements of the same type, which can have multiple child elements, and so on. Binary trees are a case where each element that has children has exactly two children. There are many algorithms that use trees, you should be able to understand them pretty well now based on your understanding of the basic singly-linked and doubly-linked lists. There are also various forms of "maps", where each 'struct' can have pointers to multiple other 'struct' elements, but they are not organized as a list or tree. These are cool for implementing advanced algorithms, a little hard to understand sometimes. The linked lists are by far the most used, so much that many programmers don't even notice how often they are used.

So that's it. A lesson filled with a bunch of stuff, on how to maintain and operate on a bunch of stuff.

**Homework:**

Modify the example of doubly-linked lists to print out all the heights that have been entered. Then, modify this code to print out the heights, but in the opposite order, from last entered to first. Have fun!
