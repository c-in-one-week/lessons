#include <stdio.h>
#include <stdlib.h>

float *makearray(int num_elements)
{
    float *p = malloc(num_elements * sizeof *p) ;
    if (p == NULL) {
        fprintf(stderr, "Out of memory.\n") ;
        exit(3) ;
    }
    for (float *p1 = p; p1 < p + num_elements; )
        *p1++ = 0.0 ;
    return p ;
}

int main()
{
    float *valarray ;
    int i = 0 ;
    int n ;
    float newval = 0.0 ;

    if (printf("array size: "), fflush(stdout), scanf("%d", &n) != 1)
        return 1 ;
    
    if (n <= 0) {
        printf("array size must be positive\n") ;
        return 1 ;
    }
    
    valarray = makearray(n) ;

    do {
        float total ;
        if (i < 0 || i > n - 1) {
            printf("element is out of bounds\n") ;
        } else {
            valarray[i] = newval ;
        }
        for (i = 0; i <= n - 1; ++i)
            printf("valarray[%d] = %.2f\n", i, valarray[i]) ;
        total = 0.0 ;
        for (float *p = valarray; p <= valarray + n - 1; )
            total += *p++ ;
        printf("total is %.2f\n", total) ;
    } while (printf("type which element to change and new value: "),
                  fflush(stdout),
                  scanf("%d %f", &i, &newval) > 0) ;
    
    free(valarray) ;
    
    return 0 ;
}
