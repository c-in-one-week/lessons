#include <stdio.h>
#include <stdlib.h>

/********************************************
*
*          calculation tree operations
*
********************************************/

#define ISNUMBER    1
#define ISMULTIPLY  2
#define ISADD       3

struct node {
    int type ; /* ISNUMBER for leaf, ISMULTIPLY, or ISADD */
    float number ;
    struct node *child1 ;
    struct node *child2 ;
} ;

float evaluate(struct node *n)
{
    if (n->type == ISNUMBER)
        return n->number ;

    if (n->type == ISMULTIPLY)
        return evaluate(n->child1) * evaluate(n->child2) ;

    if (n->type == ISADD)
        return evaluate(n->child1) + evaluate(n->child2) ;

    fprintf(stderr, "invalid node type: %d\n", n->type) ;
    return 0.0 ;
}

struct node *makenode(int type)
{
    struct node *n ;

    if (! (n = malloc(sizeof *n))) {
        fprintf(stderr, "Out of memory.\n") ;
        exit(3) ;
    }

    n->type = type ;
    return n ;
}

struct node *makenumber(float number)
{
    struct node *n = makenode(ISNUMBER) ;
    n->number = number ;
    return n ;
}

struct node *makeoperation(int type, struct node *child1, struct node *child2)
{
    struct node *n = makenode(type) ;
    n->child1 = child1 ;
    n->child2 = child2 ;
    return n ;
}

void treeprint(struct node *n)
{
    if (! n) {
        printf("NULL tree pointer") ;
        return ;
    }
    if (n->type == ISNUMBER) {
        printf("%.2f", n->number) ;
        return ;
    }
    if (n->type == ISMULTIPLY) printf("MULTIPLY(") ;
    else if (n->type == ISADD) printf("ADD(") ;
    else printf("invalid_node_type(") ;
    treeprint(n->child1) ;
    printf(", ") ;
    treeprint(n->child2) ;
    printf(")") ;
}

/********************************************
*
*        calculator that uses the tree
*
********************************************/

struct element {
    struct element *next ;
    struct element *previous ;
    struct node *val ;
} ;

struct element *addelement(  /* add an element to a doubly linked list */
  struct element *list,      /* pointer to list head, NULL for new list */
  struct node *newval        /* calculation tree to put in list */
)
{
    struct element *p ;

    if (! (p = malloc(sizeof *p))) {
        fprintf(stderr, "Out of memory.\n") ;
        exit(3) ;
    }

    if (! list) {    /* make new empty list */
        p->previous = p->next = p ;
        p->val = NULL ;
    } else {         /* add another element at end of existing list */
        p->previous = list->previous ;
        p->next = list ;
        list->previous->next = p ;
        list->previous = p ;
        p->val = newval ;
    }

    return p ;
}

void printlist(struct element *list)
{
    int cnt = 0 ;
    for (struct element *p = list->next; p != list; p = p->next) ++cnt ;
    for (struct element *p = list->next; p != list; p = p->next) {
        printf("[%d] ", cnt) ;
        treeprint(p->val) ;
        printf(" = %.2f\n", evaluate(p->val)) ;
        --cnt ;
    }
}

struct element *finditem(struct element *list, int n)
{
    struct element *p ;
    
    if (n <= 0) {
        printf("need positive number to go back\n") ;
        return NULL ;
    }
    
    /* go backwards through the doubly linked list */
    for (p = list->previous; n > 1; --n, p = p->previous) {
        if (p == list) {
            /* got back to empty element at head of list */
            printf("going back before first item\n") ;
            return NULL ;
        }
    }
    
    if (p == list) {
        /* got back to empty element at head of list */
        printf("going back before first item\n") ;
        return NULL ;
    }
    
    return p ;
}

int main()
{
    struct element *list = addelement(NULL, NULL) ; /* create empty list */
    int n ;
    float total = 0 ;

    while (
           printf(
      "type 0 for print, 1 for a number, 2 for multiply, 3 for add: "),
           fflush(stdout),
           scanf("%d", &n) == 1) {
        if (n == 0) {
            printlist(list) ;
        } else if (n == 1) {
            float newval ;
            if (printf("number: "), fflush(stdout), scanf("%f", &newval) == 1) {
                addelement(list, makenumber(newval)) ;
            }
        } else if (n == 2 || n == 3) {
            int c1, c2 ;
            int type ;
            if (n == 2) type = ISMULTIPLY ;
            else type = ISADD ;
            printf("type two numbers saying how far to go back: ") ;
            fflush(stdout) ;
            if (scanf("%d", &c1) != 1) c1 = -1 ;
            while (c1 == 0) {
                printlist(list) ;
                if (scanf("%d", &c1) != 1) c1 = -1 ;
            }
            if (scanf("%d", &c2) != 1) c2 = -1 ;
            while (c2 == 0) {
                printlist(list) ;
                if (scanf("%d", &c2) != 1) c2 = -1 ;
            }
            if (c1 > 0 && c2 > 0) {
                struct element *v1 = finditem(list, c1) ;
                struct element *v2 = finditem(list, c2) ;
                if (v1 && v2) {
                    struct node *n = makeoperation(type, v1->val, v2->val) ;
                    addelement(list, n) ;
                    printf("the value is: %.2f\n", evaluate(n)) ;
                }
            }
        } else {
            printf("must type something between 0 and 3") ;
        }
    }
}
