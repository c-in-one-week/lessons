#include <stdio.h>

#define VALSIZE 8

int main()
{
    int i = 0 ;
    float newval = 0.0 ;
    float valarray[VALSIZE] ;
    for (float *p = valarray; p <= valarray + VALSIZE - 1; )
        *p++ = 0.0 ;
    do {
        float total ;
        if (i < 0 || i > VALSIZE - 1) {
            printf("element is out of bounds\n") ;
        } else {
            valarray[i] = newval ;
        }
        for (i = 0; i <= VALSIZE - 1; ++i)
            printf("valarray[%d] = %.2f\n", i, valarray[i]) ;
        total = 0.0 ;
        for (float *p = valarray; p <= valarray + VALSIZE - 1; )
            total += *p++ ;
        printf("total is %.2f\n", total) ;
    } while (printf("type which element to change and new value: "),
                  fflush(stdout),
                  scanf("%d %f", &i, &newval) > 0) ;

    return 0 ;
}
