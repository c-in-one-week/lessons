#include <stdio.h>
#include <stdlib.h>

struct element {
    struct element *next ;
    struct element *previous ;
    int qty ;
    float price ;
} ;

struct element *addelement(  /* add an element to a doubly linked list */
  struct element *list       /* pointer to list head, NULL for new list */
)
{
    struct element *p ;

    if (! (p = malloc(sizeof *p))) {
        fprintf(stderr, "Out of memory.\n") ;
        exit(3) ;
    }

    if (! list) {
        p->previous = p->next = p ;
    } else {
        p->previous = list->previous ;
        p->next = list ;
        list->previous->next = p ;
        list->previous = p ;
    }

    return p ;
}

void printlist(struct element *list)
{
    int cnt = 0 ;
    for (struct element *p = list->next; p != list; p = p->next) ++cnt ;
    for (struct element *p = list->next; p != list; p = p->next) {
        printf("[-%d] quantity=%d price=%.2f cost=%.2f\n",
                            cnt, p->qty, p->price, p->qty * p->price) ;
        --cnt ;
    }
    printf("type the negative number to edit that item\n") ;
}

struct element *finditem(struct element *list, int n)
{
    struct element *p ;

    /* n is negative. change it to be the number of elements to go back. */
    n = -n ;

    for (p = list->previous; n > 1; --n, p = p->previous) {
            if (p == list) {
                printf("going back before first item\n") ;
                return NULL ;
            }
    }
    if (p == list) {
        printf("going back before first item\n") ;
        return NULL ;
    }

    return p ;
}

float changeitem(struct element *list, int n)
{
    float change ;
    float newval ;
    struct element *p ;
    p = finditem(list, n) ;
    if (p == NULL) return 0.0 ;
    printf("old quantity and price: %d %.2f\n", p->qty, p->price) ;
    if (printf("new quantity: "), fflush(stdout), scanf("%d", &n) != 1) {
        printf("invalid quantity\n") ;
        return 0.0 ;
    }
    if (n < 0) {
        printf("negative quantity not allowed\n") ;
        return 0.0 ;
    }
    if (n == 0) {
        /* for quantity 0, remove the item from the list */
        change = - p->qty * p->price ;
        p->previous->next = p->next ;
        p->next->previous = p->previous ;
        free(p) ;
        printlist(list) ;
        return change ;
    }
    if (printf("new price: "), fflush(stdout), scanf("%f", &newval) != 1) {
        printf("invalid price\n") ;
        return 0.0 ;
    }
    change = n * newval - p->qty * p->price ;
    p->qty = n ;
    p->price = newval ;
    printlist(list) ;
    return change ;
}


int main()
{
    struct element *list = addelement(NULL) ; /* create an empty list */
    int n ;
    float total = 0 ;

    while (printf("quantity: "),
           fflush(stdout), scanf("%d", &n) == 1) {
        if (n == 0) {
            printlist(list) ;
        } else if (n < 0) {
            total += changeitem(list, n) ;
        } else {
            float newval ;
            if (printf("price: "), fflush(stdout), scanf("%f", &newval) == 1) {
                struct element *p = addelement(list) ;
                p->qty = n ;
                p->price = newval ;
                printf("cost for %d items at price %.2f = %.2f\n",
                                             n, newval, n * newval) ;
                total += n * newval ;
            }
        }
        printf("total = %.2f\n", total) ;
        printf("(type 0 to list items) ") ;
    }

    /* free all elements in the linked list */
    struct element *plast = list->previous ;
    for (struct element *p = list->next; p != list; p = p->next) {
        free(p->previous) ; /* do not free p since still need p->next */
    }
    free(plast) ; /* loop does not free last element list->previous */
}
