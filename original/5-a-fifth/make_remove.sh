
set -e

convert remove-orig.jpg remove-orig.png
convert remove-orig.png -fuzz 34% -transparent white remove.png
convert remove.png remove.gif
if [ -e ../../assets/images ] ; then
  # using svg embed now so no- mv remove.gif ../../assets/images -v
  rm -f remove.png remove-orig.png
fi
