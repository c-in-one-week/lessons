#include <stdio.h>
#include <stdlib.h>

struct element {
    struct element *next ;
    struct element *previous ;
    float val ;
} ;

struct element *addelement(  /* add an element to a doubly linked list */
  struct element *list       /* pointer to head of existing list */
  /* if the list parameter is NULL, create an empty head for a new list */
)
{
    struct element *p ;

    if (! (p = malloc(sizeof *p))) {
        fprintf(stderr, "Out of memory.\n") ;
        exit(3) ;
    }

    if (list == NULL) { /* create a new empty list with empty head */
            /* the head of an empty list points at itself */
        p->previous = p->next = p ;
    } else {      /* add another element at the end of the list */
            /* element before the new last element is the old last */
        p->previous = list->previous ;
            /* head element goes after the new last element */
        p->next = list ;
            /* element after the old last element is new last element */
        list->previous->next = p ;
            /* new element goes at the end just before the head */
        list->previous = p ;
    }

    return p ;
}

int main()
{
    struct element *list = addelement(NULL) ; /* create an empty list */
    float newval ;

    while (printf("next value: "), fflush(stdout), scanf("%f", &newval) == 1) {
        struct element *p = addelement(list) ; /* add new element to list */
        p->val = newval ;
        /* loop through the list and print the values in it */
        float total = 0.0 ;
        for (struct element *p = list->next; p != list; p = p->next) {
            printf("%.2f", p->val) ;
            if (p->next != list) printf(" + ") ;
            total += p->val ;
        }
        printf(" = %.2f\n", total) ;
    }

    /* free all elements in the linked list */
    struct element *plast = list->previous ;
    for (struct element *p = list->next; p != list; p = p->next) {
        free(p->previous) ; /* do not free p since still need p->next */
    }
    free(plast) ; /* loop does not free last element list->previous */
}
