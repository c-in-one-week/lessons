	#include <stdio.h>

	int my_big_task()
	{
		int n ;
		if (printf("enter n: "), fflush(stdout), ! scanf("%d", &n))
			return 0 ;

		printf("5 * n = %d\n", 5 * n) ;
		return 1 ;
	}

	int main()
	{
		if (! my_big_task()) {
			fprintf(stderr, "Error: my_big_task failed.\n") ;
			return 1 ;
		}
		return 0 ;
	}
