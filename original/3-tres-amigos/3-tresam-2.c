	#include <stdio.h>

	int my_big_task(int x)
	{
		int n ;
		if (! (printf("enter n: "), fflush(stdout), scanf("%d", &n)))
			return 0 ;

		printf("x * n = %d\n", x * n) ;
		return 1 ;
	}

	int main()
	{
		if (! my_big_task(7)) {
			fprintf(stderr, "Error: my_big_task failed.\n") ;
			return 1 ;
		}
		return 0 ;
	}
