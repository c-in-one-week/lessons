/** talk about processes **/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int x ;
	double fact(int somenum) ;
	
	while (printf("enter x: ") , fflush(stdout), scanf("%d", &x)) {
		double y =   fact(x) ;
		printf("x = %d fact(x) = %f\n", x, y) ;
	}
	return 0 ;
}

/****
	fact(int n)	- factorial
	This algorithm is slow- a loop would be much faster.
****/
double fact(int n)
{
	if (n <= 0) {
		fprintf(stderr, "can't take factorial of non-positive number\n") ;
		exit(1) ;
	}
	if (n == 1) return 1 ;
	return n * fact(n - 1) ;
}
