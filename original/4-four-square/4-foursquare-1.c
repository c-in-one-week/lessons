#include <stdio.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

int main()
{

	struct simple_date bday, tday ;

	tday.year = 2021 ;
	tday.month = 6 ;
	tday.day = 4 ;

	bday.year = 1987 ;
	bday.month = 3 ;
	bday.day = 2 ;

	printf("You are %d years, %d months, and %d days old\n",
		tday.year - bday.year, tday.month - bday.month, tday.day - bday.day) ;

	return 0 ;
}
