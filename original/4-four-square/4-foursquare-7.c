#include <stdio.h>
#include <stdlib.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

void date_print(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_print\n") ;
		exit(4) ;
	}

	printf("years = %d months = %d days = %d\n",
		date_ptr->year, date_ptr->month, date_ptr->day) ;
}

void date_scan(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_scan\n") ;
		exit(4) ;
	}

	if (printf("enter year: "), fflush(stdout), ! scanf("%d", & date_ptr->year)) {
		fprintf(stderr, "invalid year entry\n") ;
		exit(1) ;
	}
	if (printf("enter month: "), fflush(stdout), ! scanf("%d", &date_ptr->month)) {
		fprintf(stderr, "invalid month entry\n") ;
		exit(1) ;
	}
	if (printf("enter day: "), fflush(stdout), ! scanf("%d", &date_ptr->day)) {
		fprintf(stderr, "invalid day entry\n") ;
		exit(1) ;
	}
}

void date_subtract(
	struct simple_date *d_result,
	struct simple_date *d1,
	struct simple_date *d2
    )
{
	if (! d_result || ! d1 || ! d2) {
		fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
		exit(4) ;
	}
	d_result->year = d2->year - d1->year ;
	d_result->month = d2->month - d1->month ;
	d_result->day = d2->day - d1->day ;
	if (d_result->day < 0) {	/* fix negative days */
		-- d_result->month ;	/* use the decrement operator on the month */
		d_result->day += 31 ;	/* get rid of silly looking negative number */
	}
	if (d_result->month < 0) {	/* fix negative months */
		-- d_result->year ;
		d_result->month += 12 ;
	}
}

int main()
{
	struct simple_date bday, tday ;
	struct simple_date aday ;

	printf("Todays date:\n") ;
	date_scan(&tday) ;

	printf("Birthday date:\n") ;
	date_scan(&bday) ;

	date_subtract(&aday, &bday, &tday) ;

	printf("Your age:\n") ;
	date_print(&aday) ;

	return 0 ;
}
