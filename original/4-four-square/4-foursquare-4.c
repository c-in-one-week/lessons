#include <stdio.h>
#include <stdlib.h>

void my_pointer_function(int *my_first_int_pointer, int *my_second_int_pointer)
{
	if (! my_first_int_pointer) {
		fprintf(stderr, "software error: my_pointer_function received NULL in arg 1.\n") ;
		exit(4) ;
	}
	if (! my_second_int_pointer) {
		fprintf(stderr, "software error: my_pointer_function received NULL in arg 2.\n") ;
		exit(4) ;
	}
	*my_first_int_pointer = 5 ;
	*my_second_int_pointer = 6 ;
}

int main()
{
	int x, y ;

	my_pointer_function(&x, &y) ;

	printf("value set for x is: %d, for y is: %d\n", x, y) ;

	return 0 ;
}
