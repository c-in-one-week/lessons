#include <stdio.h>
#include <stdlib.h>

void my_pointer_function(int *my_int_pointer)
{
	if (! my_int_pointer) {
		fprintf(stderr, "software error: my_pointer_function received NULL.\n") ;
		exit(4) ;
	}
	*my_int_pointer = 6 ;
}

int main()
{
	int x ;

	my_pointer_function(&x) ;

	printf("value set for x is: %d\n", x) ;

	return 0 ;
}
