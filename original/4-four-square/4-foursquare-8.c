#include <stdio.h>
#include <stdlib.h>

struct some_struct {
	int *x_ptr ;
} ;

void f(struct some_struct *some_pointer)
{
	* some_pointer->x_ptr = 6 ;
}

int main()
{
	struct some_struct my_struct ;
	int x ;

	x = 5 ;
	my_struct.x_ptr = &x ;

	printf("original value for x: %d\n", x) ;

	f(&my_struct) ;
	
	printf("final value for x: %d\n", x) ;

	return 0 ;
}
