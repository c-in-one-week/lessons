#include <stdio.h>
#include <stdlib.h>

int main()
{
	int *my_int_pointer = NULL ;
	int x = 5 ;

	printf("originally x is: %d\n", x) ;

	/* remember from scanf: &x creates a pointer to the x variable */
	my_int_pointer = &x ;

	if (! my_int_pointer) {
		/* This test is not really necessary in this case- the code just
		   set my_int_pointer to &x in the previous line, it is going to be set.
                   But if there is any code in between, possibly with 'if' statements
		   and so on, it is a good idea to check that my_int_pointer is set */
		fprintf(stderr, "software error: pointer still set to NULL.\n") ;
		exit(4) ;
	}

	*my_int_pointer = 6 ;

	printf("x becomes: %d\n", x) ;

	return 0 ;
}
