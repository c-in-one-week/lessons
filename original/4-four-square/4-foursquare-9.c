#include <stdio.h>
#include <stdlib.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

struct twodates {
	struct simple_date *date1 ;
	struct simple_date *date2 ;
} ;

void date_print(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_print\n") ;
		exit(4) ;
	}

	printf("years = %d months = %d days = %d\n",
		date_ptr->year, date_ptr->month, date_ptr->day) ;
}

void date_scan(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to date_scan\n") ;
		exit(4) ;
	}

	if (printf("enter year: "), fflush(stdout), ! scanf("%d", & date_ptr->year)) {
		fprintf(stderr, "invalid year entry\n") ;
		exit(1) ;
	}
	if (printf("enter month: "), fflush(stdout), ! scanf("%d", &date_ptr->month)) {
		fprintf(stderr, "invalid month entry\n") ;
		exit(1) ;
	}
	if (printf("enter day: "), fflush(stdout), ! scanf("%d", &date_ptr->day)) {
		fprintf(stderr, "invalid day entry\n") ;
		exit(1) ;
	}
}

void twodates_scan(struct twodates *twodates_ptr)
{
	printf("Enter first date (day1):\n") ;
	date_scan(twodates_ptr->date1) ;

	printf("Enter second date (day2):\n") ;
	date_scan(twodates_ptr->date2) ;
}

void twodates_subtract(
	struct simple_date *d_result,
	struct twodates *twodates_ptr
    )
{
	struct simple_date *d1 = NULL ;
	struct simple_date *d2 = NULL ;

	if (! d_result || ! twodates_ptr) {
		fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
		exit(4) ;
	}

	d1 = twodates_ptr->date1 ;
	d2 = twodates_ptr->date2 ;
	if (! d1 || ! d2) {
		fprintf(stderr, "software error: NULL pointer in argument to date_subtract\n") ;
		exit(4) ;
	}

	d_result->year = d2->year - d1->year ;
	d_result->month = d2->month - d1->month ;
	d_result->day = d2->day - d1->day ;
	if (d_result->day < 0) {	/* fix negative days */
		-- d_result->month ;	/* use the decrement operator on the month */
		d_result->day += 31 ;	/* get rid of silly looking negative number */
	}
	if (d_result->month < 0) {	/* fix negative months */
		-- d_result->year ;
		d_result->month += 12 ;
	}
}

int main()
{
	struct simple_date day1, day2 ;
	struct simple_date dur ;
	struct twodates bothdates ;

	bothdates.date1 = &day1 ;
	bothdates.date2 = &day2 ;

	twodates_scan(&bothdates) ;

	twodates_subtract(&dur, &bothdates) ;

	printf("Change from day1 to day2\n") ;
	date_print(&dur) ;

	return 0 ;
}
