#include <stdio.h>
#include <stdlib.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

void setyearnow(struct simple_date *date_ptr)
{
	if (! date_ptr) {
		fprintf(stderr, "software error: NULL argument to setyearnow\n") ;
		exit(4) ;
	}
	date_ptr->year = 2021 ;
}

int main()
{
	struct simple_date tday ;

	setyearnow(&tday) ;

	printf("the year was set to %d\n", tday.year) ;

	return 0 ;
}
