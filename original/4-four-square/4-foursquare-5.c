#include <stdio.h>
#include <stdlib.h>

struct simple_date {
	int year ;
	int month ;
	int day ;
} ;

void date_print(struct simple_date *date_ptr)
{
	printf("year = %d month = %d day = %d\n",
		date_ptr->year, date_ptr->month, date_ptr->day) ;
}

int main()
{
	struct simple_date tday, bday ;

	tday.year = 2021 ;
	tday.month = 6 ;
	tday.day = 4 ;

	bday.year = 1987 ;
	bday.month = 3 ;
	bday.day = 2 ;

	date_print(&tday) ;
	date_print(&bday) ;
}
