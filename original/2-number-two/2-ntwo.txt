Here is a more-or-less complete definition of C in just a few pages. You might want to read these pages a few times.

A C source file consists of a series of declarations. These declarations can be of variables, functions, or types. Mixed in are other syntactic things, such as C comments. A C variable declaration is of the form `type variable, variable, ... ;`, it is possibly to put in an initial value as `variable = value` instead of just `variable`. A function declaration is of the form:
`function_name ( parameters ) ;`
for a function that is defined elsewhere, and
`function_name ( parameters )
{
	statement
	statement
	...
}`
to define a function, which will be covered in the next lesson. Declarations of types define new types, which will be covered in a later lesson.
There are relatively few C statements. We have already seen the 'if' statement:
`	if ( expression ) statement
	else statement`
Anywhere there can be a statement, there can be a list of statements surrounded by '{' and '}'.
Right after any '{' there can be a list of declarations before the list of statements. The variables declared here are only valid within the '{' '}' block of statements. We have seen variable declarations after the opening '{' for a function, yet they can be after any '{', which is a good way of saying that a particular variable is only used in the particular list of statements contained in the '{' '}' list.
Other statements are:
`	while ( expression ) statement`, which repeats the statement while the expression is true.
`	do statement while ( expression ) ;`, which does the statement first without checking the expression until it is done the first time.
`	return expression ;`, which returns a value from the current function.
One of the most used statements is just an expression, called an expression statement:
`	expression ;`, which does this expression and that's all.
In C, assignments are actually expressions, so you can type:
`	a = b = c ;`, which is equivalent to `a = (b = c) ;`, i.e. the value of c is assigned to b, and then the result of that assignment expression, which is still the value c, is assigned to a. Once you think of C assignments as just expressions this becomes a very simple concept.
Also, the comma operator discussed in the beginning lesson is used in expressions, so
`	f(x) , g(x)` is an expression consisting of calling f, then calling g, and the value of the expression is g(x).
Expressions are things that have values, as shown in the following expression statement using the assignment and comma operators:
`	a = ( f(x), g(x) ) ;`
However, the value can be ignored, as shown in the following expression statement using just the comma operator:
`	f(x) , g(x) ;`
The final statement that is frequently used is the 'for' statement, which is written:
`	for ( expr1 ; expr2 ; expr3 ) statement`
The semantics of the 'for' statement are that expr1 is done first, only once. Then, while expr2 is true, the statement after the parenthesis is repeatedly done, and each time after that statement expr3 is done. For example:
`	for (n = 1, total = 0; n <= 10; n = n + 1) total = total + n * n ;`
This finds the sum of the squares of 1 through 10, and is equivalent to
`	n = 1, total = 0 ;
	while (n <= 10) {
		total = total + n * n ;
		n = n + 1 ;
	}`
but the 'for' loop construct is generally a lot more concise and easier to read.
`	;` Just semicolon is a null statement that does nothing. It is occasionally useful as the body statement of a 'for' where the meat is in the expressions.
There are a various statements for non-local control transfer: 'goto', 'switch', 'break', and 'continue'. None of these is very complicated, and they are not used as often as the ones mentioned above. They can be studied in a later lesson.
Note that every C statement ends with a C statement inside of it, such as the statement inside of the 'if' statement, or explicitly ends with a ';', such as the 'return' statement. Thus, the single ';' always ends a statement, including a statement containing a statement inside of it. The exception to this is when a statement contains a list of statements surrounded by '{' '}' inside of it, in which case the statement ends with '}' and not ';'.

There are many expression operators in C, such as comma, assignment, '+', '-'. Going over them all would require another lesson.

There are relatively few basic C data types. There are 'int', 'char', 'float'. For 'int' there is 'long int' with more bits, and 'short int' with less bits. Just 'long' or 'short' implies 'long int' or 'short int'. If more or less bits are not available for the hardware being used, then 'long' or 'short' may still be used so that code still compiles, and they are the same as 'int'. A 'char' variable is one byte. The 'float' type comes with a 'long float', which has more bits if more bits are available in the hardware. A 'long float' is also called a 'double', these are synonyms for the same thing. The 'float' type is for "floating point", aka base 2 decimal fractions. They also internally have exponents, so one float variable can store 3.57 * 10^42, which is the number of elementary particles in the universe (prove I'm wrong, eh). In C code, this value would be written "3.57e42". Not so complicated.

So there you have it. You have learned all of C by the end of Number Two. You can now take the rest of the week off. May I suggest a nice beach?
