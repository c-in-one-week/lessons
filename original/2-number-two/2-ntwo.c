#include <stdio.h>

int main()
{
	float x ;
	
	while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) {
		double y =   x * x * x / 42 + 7.0 / 11 ;
		printf("x = %f y = %f\n", x, y) ;
	}
	return 0 ;
}
