split source files into modules
create headers - includes

extern

many library functions -
stdio:
  files: FILE *, stdin/stdout/stderr,
         fopen/fclose, printf/fprintf/puts/fputs, fflush,
         scanf/fscanf/fgets (not gets), putchar/putc, getchar/getc
stdlib (are all these in stdlib?):
  malloc/calloc/realloc/free
  atof(), atoi(), atol(), atoll(), int abs(int), long labs(long)
  strings: strcpy, strncpy, strcat, strncat, strcmp, strcasecmp
	utf8 vs wide characters. utf8:
		Thompson's design was outlined on September 2, 1992,
		on a placemat in a New Jersey diner with Rob Pike.
		In the following days, Pike and Thompson implemented it...
  qsort - sort/scan algorithms
  outside: argc/argv getenv / setenv / system
  rand/srand random/srandom
  all those math functions
time:
  time, ctime, localtime
manual pages

make

create your own libraries in subdirectories
