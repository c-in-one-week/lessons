---
layout: lesson
title: Test Programs
---

<!---
test-programs/index.md   test programs in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

## {{ page.title }}

{% include lessonhead.html %}

{% include content/extra/t/comments.md %}
