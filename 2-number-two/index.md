---
layout: lesson
title: 2. Number Two
---

<!---
2-number-two/index.md   lesson 2 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/2/lesson.md -%}

{% include content/extra/2/comments.md %}
