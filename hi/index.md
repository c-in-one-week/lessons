---
layout: nonlesson
title: Hi!
pageTitle: C In One Week - Hi, Hello there!
---

<!---
hi/index.md   hi, hello there! page in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include content/other/hi.md -%}
