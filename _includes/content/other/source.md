<!---
_includes/content/other/source/index.md   Source page content in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

This web site is hosted including sources at gitlab.com.<br>
To clone it<br>&nbsp;&nbsp;&nbsp;
`git clone git@gitlab.com:c-in-one-week/lessons.git`
<br>&nbsp;or<br>&nbsp;&nbsp;&nbsp;
`git clone https://gitlab.com/c-in-one-week/lessons.git`

The content is simply markdown and is in &nbsp;`lessons/_includes/content`.

There are two custom development scripts in the site clone.

- Use `./install_dev` to make sure setup for '`./build_dev`' is performed.

- Use `./build_dev` for local development builds with a built-in mini web server.

Sometimes `./build_dev` misses changes. If so, try '`./build_dev -scratch`'.

These development scripts are not specific to c-in-one-week.<br>
This site uses Jekyll. These scripts should work for developing any jekyll web site,<br>
with modifications to change the c-in-one-week.com deployment during git push.<br>
These scripts were written under Debian.

The development scripts set up a git pre-push hook so git push locally rebuilds<br>
site pages and pushes them to site-pages.git, deploying them using gitlab CI/CD.<br>
Before `./install_dev` is first run, content pushes do not show on this web site.

This is open source. Lesson content is CC-BY 4.0. The custom scripts are GPL-v2.
<br>See [License](/license/ "Source licensing details") for more info.

An easy way allowing merging changes you make back to the site is [contact me](mailto:info@remove-this-first_no_spam_c-in-one-week.com "my email")<br>
first, and I can make you a branch that you can clone and work with. Another way<br>
is [fork the site source](https://gitlab.com/c-in-one-week/lessons/-/forks/new "fork c-in-one-week.com"), and request a merge. Either way, you need a GitLab account.

<hr style="margin-top:3%;margin-bottom:2%;margin-left:-35px;margin-right:-35px;">

<center><h2>Some Things That Could Be Fixed</h2></center>

- Little gaps (sometimes big ones) here and there on the rendered pages
- The icon was me playing at favicon.cc for 30 minutes
   - No particular reason it was created online or at that site
   - The blue circle around the C is not bold enough
   - The C is not really a very good C
   - Things should be drawn from some viewing perspective with shading
   - The first and last animation frames are not as bad as the middle seven
      - The green slush going around the C does not step evenly each frame
      - The green slush does not show motion well in each frame
      - In each frame, older green slush should be rounder against the circle
- Styling, especially inline code and the lessons in general, could be better
   - Fonts, font sizes, colors
- Spelling and grammar
- Shall I go on for hours regarding how I strive to be concise?
- Rethinking how the top navigation scrolls off the screen
- Scrollbar placement
- Responsive - check for smaller page size e.g. a phone, and adjust accordingly
- The file `TODO.md` in the web site repo has a more complete list

Some people may not be pleased with the inconsistency between the basic page<br>
format of the lesson pages and the others. Well, consistency is the hob-goblin<br>
of small minds, says someone with plenty of small-minded experience.

<hr style="margin-top:2%;margin-bottom:2%;margin-left:-35px;margin-right:-35px;">

<a name="me"></a>

Please let me know and merge when you make it better!<br>
My email account: info, domain: c-in-one-week.com
