<!---
_includes/content/other/about/index.md   About page content in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

This is a quick class to learn C.

It is intended to teach the core of the language. After taking this class, you should be able to write simple C programs yourself. It covers all that is really going on in C. After taking this class, you should be able to write large C programs as well, with full understanding, with a little help from an advisor and from google for details.

This class is not intended for someone making a survey of programming languages. It is not intended to provide an overview of the language for someone not interested in programming in C. There is a lot of meat here targeted toward making you a C coder, and I suggest you might find a better class for an overview of C elsewhere. See the [ok](/ok "Ok, so you want to code in C") page.

This class is not intended to provide snippets. If you are looking for snippets of code, without understanding what they are really doing, I suggest you might find more useful snippets elsewhere. If you are looking for snippets of knowledge, such as how to use strings or call various library functions, without knowledge of how they fit into C as a whole, I suggest you might find better self contained snippets of knowledge elsewhere. This class is intended to be taken in one shot, from the beginning to the end, and then you can write your own code. And it will be beautiful.

The class is intended for technically savvy people who may have very little knowledge of what is actually happening inside their computer, and no experience with any programming languages. Besides C, it covers what is going on inside the box, how things are organized, including the operating system and how it interacts with your programs, mostly in terms of the semantics of C. After all, everything in the box is <s>all</s> mostly in C anyway.

This class also covers the origin and future of computing. Really, but in a non-serious way. I tried to be accurate, but not pedantic.

This class is not intended to teach every last detail of the C language. You should be able to look at any C code and understand basically what is going on. If you see something, or want to write something, that is not covered in this class, google is your friend. After this class, you will be able to see how the pieces fit together, what is going on, and the details can be filled in as needed.

TLDR;<br>
<del>
This class very much focuses on the semantics of the C language. In the author's opinion, it is much more efficient to program something when thinking at the level of the abstractions provided by the programming language. When you are teaching someone how to cook, it is not efficient to teach them all of the biochemistry involved in the chemical reactions when meat is cooking. That is not how to learn to cook, that is how to learn to be a biochemist. In C, understanding the voltage levels of the components in the computer, the bit patterns of the machine instructions, the binary details of every C operation, is necessary to build computers or to write C compilers. These are laudable careers, but focusing on this in order to learn C is not just distracting, it will make for poor C programmers who are not thinking in terms of the semantics of C itself. The code they write will be less portable because these details are different for different machines. Also, if the coder is thinking in terms of details below C semantics, it distracts from the big picture that is necessary to develop big programs. I have found that people who focus on these details are much slower at getting C itself, and have a weaker grasp of it once they get it, and are much worse programmers in the long run. However, why listen to my insights? Most people don't think this way, they think that people who understand all the details of something are better at it, therefore understanding the details of the C implementation would make someone a better software developer, even though this way of thinking is completely backwards from what I have actually seen. Programming languages are about abstraction above the physical computer, and thinking of them as just an abbreviation of the behaviour of the physical computer is missing the entire point, yet many people are so used to their old way of thinking that they would rather become worse programmers than try to understand the power of abstraction. Sometimes it seems that there is so much more effort for less result trying to teach programming to people who adamantly refuse to get this, however you can lead a horse to water, but you can't make them think.
</del>
