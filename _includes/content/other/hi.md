<!---
_includes/content/other/hi/index.md   hi, hello there! page content in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

My friends say probably no one has looked at this site except us.

Well?

Let me know if you like it! Let me know if you don't!

[Me](mailto:info@remove-me-from-address_no_spam_c-in-one-week.com "my site e-mail")

Let me know what you think sucks about it the most. I have a long  [list](/source/ "lesson source and what could be fixed and how to fix it") of things that need fixing. Maybe you thought of something that needs fixing even more, and if you let me know, maybe I will fix it.

Or you can fix it yourself! Just clone my site, and request I pull any changes back - [details how here](/source/ "source and how to work with it"). Or not; this is [open source stuff](/license/ "licensing for this site") except for student's comments and some of the diagrams in lesson 5; you can do whatever you want with the rest of it, change it around, use it as part of a proprietary paid class you want to teach, whatever, and you have my ok as long as I get a little credit per the license terms.

If you want to change it around, submit that pull request and I would be more than happy to work with anyone to fix this shit up. Because it needs it. But even if you don't, if you just have some ideas, or you just want to say Hi!, whatever, let me know!

Thanks,<br>
[Shaw](mailto:info@remove-me-from-address_no_spam_c-in-one-week.com "my site e-mail")

---

If you send an e-mail, and if it is ok to include it here according to [CC-BY](/license/ "site licensing"), let me know.
