<!---
_includes/content/other/ok/index.md   so you want to be a programmer page content in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

So you want to be a C programmer.

Maybe you don't know any programming.

These lessons are for you. They introduce a lot of C concepts. Most of these lessons build on the previous lesson. They won't make sense without the previous. If you do not take them one right after the other, one lesson each day or faster, the previous lesson is likely to get cold. This class is really designed to be completed in one week. After which, you will be a C programmer. With a full understanding of all the core concepts of C.

So you have to be dedicated. If you are not willing to dedicate a week to this, after work or whenever, it's likely not going to stick very well. There is very little homework and the lessons are designed to be taken one right after the other. Besides being dedicated, you also have to be fearless, just to be a C programmer at all; I talk about this in the last paragraph. However, you don't really have to know anything about programming, or even about what is going on inside your computer.

Maybe you think you understand what is going on inside your computer, lots of chips and things, with voltages going up and down at high speed. But, hmmm There has got to be something between that level of understanding and the level of understanding how to run the applications you run on your computer. The level in between those levels is the software running on the computer. Software is like a long instruction manual, telling the computer what to do all the time. How is that software organized? What is it in, what language? What do the expressions and statements in that language mean?

Well, much of that software is written in C. Most operating system software is written in C. A lot of user software is written in C. A lot of the software not written in C uses languages like Python, PHP, Perl. Most of the versions of these languages are actually programs written in C, C programs that read and understand instructions written in the other language, and run them, running in C. Other languages, such as C++, are used to make programs using the program-making back ends, code generators, that were originally developed for C. So in all these cases, if you know what is going on with C, you will know what your machine can do.

Actually understanding what C is saying, what I call the C semantics, the meaning of the C language, requires no understanding of chips and voltages rapidly going up and down. It requires no understanding of what is happening at the pins of these chips (pins are little wires going into chips), what binary values going into those pins represent particular instructions or locate particular things in the memory space of the computer. C is at a level above this. However, C is low enough that it is not spitting out a lot of activity for each thing you ask it to do, a lot of activity that would waste time. It is "just above the metal", so a C program can get full performance of the machine.

Each type of machine has different ways of representing things. If you think about this representation while writing a program it will only lead to writing programs that only work on that type of machine. Using C, which is above this, leads to writing programs that work on many types of machines. So if you upgrade your machine, say to the new Apple M1, or from a 32bit Intel to a 64bit Intel to whatever is next, if you write in C your code will still work. Also, focusing on C, the semantics of C, provides a common abstraction that still covers almost everything that can happen in the computer, and simplifies thinking about what is going on. Using a programming language abstraction makes it easier to have a full understanding of a complex program and is important for creating even more complex ones.

Besides coming close to the full performance of the machine, C is also a reasonably simple language. These few lessons have a reasonably complete coverage of all the concepts of C. Other languages, such as C++, are much more complicated.

So, why don't more people start learning programming by studying C?

Because you have to be fearless. The core constructs of C are close to what is going on inside the machine. These constructs do not involve any extra activity to protect you from your mistakes. Your program can easily crash if you make one mistake. Even worse, it is easy write a program that will appear to work, then when you are demo'ing your program to someone else, it crashes or doesn't get the right answer. Much more worse, what if it is your boss that is demo'ing the program when it messes up? No more job for you. These lessons talk about the perils, and ways to avoid them, yet you probably will get burned occasionally writing C programs. You have to be fearless, and careful, if you really are going to code in C. Are you up for it?
