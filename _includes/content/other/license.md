<!---
_includes/content/other/license/index.md   License page content in c-in-one-week
Copyright (c) 2021
-->

The lesson content on this site is being shared using the [Creative Commons Attribution 4.0 International (CC-BY 4.0)](http://creativecommons.org/licenses/by/4.0/ "CC-BY 4.0") license; per copyright messages in comments in the source. See the link at the bottom of every page displayed on the site for details on your rights under this license. This link explains your rights to share and modify this content. I think CC-BY gives you rights to do this, as long as the author is attributed, yet of course the legalese in the copyright messages as described by the link takes precedence over what I think. Basically, I hope you will use this content as you see fit, for good and not for evil. Yet it is not my place to judge such things, all I can do is hope.

Some of the diagrams in [lesson 5](/5-a-fifth/ "A Fifth") are based on a [diagram from Wikipedia](https://commons.wikimedia.org/wiki/File:Full_binary.svg), authored by Wikipedia user Tmigler. It is licensed per [Create Commons Attribution-ShareAlike 4.0 International (CC-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/), as it was on Wikipedia. I think the CC-SA license requires all derivative works to also be licensed under CC-SA, so these diagrams may not be modified without all distribution of them being made available under CC-SA.

The software I wrote to assist developing this site, which is stored in the same repository as the lesson content, is licensed under the GNU Public License version 2. See the [Source](/source/ "Source") page for a description of this software, and how to use it to further develop the site, including changing or adding to the lesson content.

For attribution, as required by CC-BY, my name is Shaw. I wrote this shit. I hope you like it. I used the nik 'Coffee' in the lessons.

Some of the comments after some of the lessons, and much of Questions For The Teacher, Related Yet Off Topic, and Test Programs, were written by others. This CC-BY 4.0 license is not intended by me to cover their writing.
