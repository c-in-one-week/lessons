<!---
_includes/content/other/resources/index.md   other learning resources related to c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

- [Kernighan & Richie: The C Programming Language, 2nd Edition](https://www.amazon.com/Programming-Language-2nd-Brian-Kernighan/dp/0131103628 "K&R")

Aka K&R, this book was written by the creators of C and defined the language for many years, until standards committees took over. Make sure to get the 2nd Edition, it was updated by the original authors with standardization changes to the language. This book was the way beginners learned C, back in the day.

This Amazon page says "There is a newer edition of this item" and links to a C++ book. Obviously that's not right, C++ is not C. Also, there is another book claiming to be C Programming Language 3rd edition, but it is really an inferior rewrite by other authors.
