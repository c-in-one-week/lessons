{% include startcomment.html -%}
<!---
Comments for lesson 4 go here.
Place the source for the comments after the following arrow line:
-->

{{ UserAlpha }}{{ "2021-6-5 10:12:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

A big thank you and two 👍  for putting so much effort into this! I hope there'll be a way for me to somehow repay you....

{{ UserCoffee }}{{ "2021-6-5 10:29:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

No prob. Just use it for good, not evil. That's all I ask.

