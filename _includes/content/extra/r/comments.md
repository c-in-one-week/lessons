{% include startcomment.html -%}
<!---
Related yet off topic comments and questions go here.
Place the source for the comments after the following arrow line:
-->

{{ UserZeta }}{{ "2021-6-2 3:51:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

[https://www.cprogramming.com/tutorial/c-tutorial.html?inl=nv](https://www.cprogramming.com/tutorial/c-tutorial.html?inl=nv)<br>
<br>
[https://www.youtube.com/watch?v=4Hit7Ya32lU](https://www.youtube.com/watch?v=4Hit7Ya32lU)

[https://web.stanford.edu/group/sisl/k12/optimization/MO-unit1-pdfs/1.8iterativeloops.pdf](https://web.stanford.edu/group/sisl/k12/optimization/MO-unit1-pdfs/1.8iterativeloops.pdf)<br>
^^ Stanford's 21 page explanation of Loops ^^

[https://www.codingeek.com/tutorials/c-programming/for-loop-in-c-programming-language-iteration-statements/](https://www.codingeek.com/tutorials/c-programming/for-loop-in-c-programming-language-iteration-statements/)

[https://www.cprogramming.com/tutorial/c-tutorial.html?inl=nv](https://www.cprogramming.com/tutorial/c-tutorial.html?inl=nv)

{{ UserCoffee }}{{ "2021-6-2 4:12:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Thanks for moving these links to this channel. Last night I was thinking about adding relevant references to a new channel, hopefully I will have a chance to do that tomorrow. The reason these particular links are not relevant is that this training is about C semantics as necessary for C programming. The C semantics of looping are pretty simple. The 'while' loop just repeats something while a condition is true. The 'for' loop allows abbreviation of 'while' loops. Studying the concept of 'iteration' is important for an understanding of constructs in some other languages used by those other languages to avoid some of the C hot poker constructs that I intend to show in later lessons. However, this concept is overkill for C, it just makes looping seem more complicated than it is, leads to overthinking things, and makes it seem like the different looping constructs, such as 'while' and 'for', are different somehow, which they aren't in C. We don't avoid hot pokers in C, we just try to be careful with them. I think part of the issue here is that I did a bad job with the Number Two homework assignments.

{% comment %} thenwait 164 {% endcomment %}{{ UserCoffee }}{{ "2021-6-2 6:56:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**I believe I did a bad job with the Number Two homework assignments:** for the first assignment I should have done the nested loop myself as an illustration in the lesson, using 'while' for both the outer loop and the inner loop, then just assigned replacing the inner 'while' with 'for' to show how it is really the same in C but abbreviates things. Expecting beginning programmers to **get** nested looping without an illustration was a bit much. I hope nested looping is clear to everyone now, it's just a loop in a loop, not complicated at all once you see it. When I did the second assignment, I did not explain the purpose of the loop counter. I realized that at the time, but I was tired and forgot to follow up. A did a great job of explaining it later in the Number Two channel, but I should have explained it in the assignment. Sorry for my fuck ups.

{% comment %} thenwait 1394 {% endcomment %}{{ UserAlpha }}{{ "2021-6-6 12:33:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Could you say that C (if done properly) is portable and therefore independent of the "CPU" or microprocessor while the lower level languages like Assembler and machine language are always specific to the "machine" you are on?

{{ UserCoffee }}{{ "2021-6-6 12:34:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Absolutely

{{ UserAlpha }}{{ "2021-6-6 12:34:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Cool

So in general C is closest you get to the cpu while staying portable?...neglecting the fact that there possibly 1000s of programming languages and "distance" to the cpu cannot be measured absolutely

{{ UserCoffee }}{{ "2021-6-6 1:14:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I would say so. Different people say different things.

{{ UserAlpha }}{{ "2021-6-6 1:47:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

yeah I'sure you could spend some time spoil some relationships discussing this...but i see my hypothesis as validated. Thx!
