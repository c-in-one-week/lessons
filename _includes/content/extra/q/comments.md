{% include startcomment.html -%}
<!---
Questions for the teacher go here.
Place the source for the questions after the following arrow line:
-->


{{ UserZeta }}{{ "2021-5-31 10:58:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

@Coffee  "using the pointer construct from yesterday to point to the local variable x. This innocent looking code will crash very badly, because when the function defining x returns, the space for its local variables no longer exists, so the pointer is now pointing to some other random thing. "
<br>
When you say 'crash very badly' do you mean, crash the code or the whole system?<br>
<br>
This of course assumes we are coding in the terminal.

{{ UserAlpha }}{{ "2021-5-31 11:01:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

and why does the pointer go rogue?...and what is a pointer in C exactly?

{{ UserCoffee }}{{ "2021-5-31 11:19:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Nothing you can do in a program can crash the whole system. Unless you really F-up. Generally, if you don't use sudo, and you don't try to run an external command which accidentally deletes all your files, a single program is only going to smash up itself while it is running, and when you fix the problem and re-run it, everything is ok. This is an important feature of program development- it is ok to have plenty of bugs in fresh code, you're not going to screw up anything else going on in the system, as long as you are prepared to squash them Before the code goes to alpha by having thorough module testing and integration testing. One thing- it is important to have a feel for what kinds of bugs won't be caught quickly, those are the mistakes not to make when typing the code. That is a good question to ask me on the last day of these lessons, once I have shown the various things happening in code so that my answer has some context.

{% comment %} thenwait 12 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 11:31:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

A `{ int x ; return &x ; }` the variable is local to the statement block. Once that statement block is done, no more variable. How that is done depends on the machine architecture, and C compilers are finding more and more complicated ways of taking advantage of that architecture to increase the performance of the program. All we know for sure, based on the semantics of C, is that the return value points at x, and if x isn't there anymore and you use the return value, especially if you write through the pointer to what used to be x, you will be changing the value of something, something which is not x because it does not exist anymore, and cause a mess in the program that may be very hard to track down. It is not good to think in terms of the way C used to implement these things, and then come up with tricks to manipulate that, or mental models based on particular implementations, and then have to continuously worry about changing those tricks or mental models for newer compiler technology. It is best to stick to the semantics of the C language, not the way that those semantics used to be implemented. I say this as someone who used to do exactly that, converting these kinds of pointers to those kinds of pointers and accessing them in weird ways, based on my knowledge of the exact implementation back when I first used C, and some of that code stopped working on newer compilers a long time ago.

{{ UserCoffee }}{{ "2021-5-31 12:58:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Had a good lunch. Ok, back to pointers. On Intel processors, things in RAM are in a particular segment of the ram as defined by the intel processor segment tables, which are set up for each program when it starts, and they are in a particular location in that segment. So pointers needed to have both a segment value and the location of the variable in the segment. You never needed to know about any of this in order to use properly written C. Also, it is no longer particularly relevant, with 64bits these days a whole program is in one big segment, so the segment is pretty much ignored these days. However, back in the day, if there were issues with the segment in a pointer, the program crashed. So doing things like printf("%lx\n", (long)&x) would likely mess up what segment value it printed, and using any code based on any kind of manipulation of pointers like I did back in the day, would crash. So ... a pointer is a pointer. Sorry for repeating this. It is just that we spent over an hour going over something that is pointless, when you guys should have been able to do the entire Lesson Two in an hour, if you weren't trying to out-think the language and the compiler writers. 5 final minutes repeating this here. Done

{{ UserZeta }}{{ "2021-6-1 10:05:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Is there a maximum of three expressions in a `For` loop?

{{ UserCoffee }}{{ "2021-6-2 12:45:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I follow the original standard, in which there is. Now, I am not sure, but since there are still compilers out there which follow the original standard, I consider other than 3 expressions non-portable. However- you can string as many expressions as you want to make one big expression using the comma operator, such as the following:

    for (d = 0.1, loopcounter = 0, printf("starting loop...\n"), fflush(stdout); ....

{{ UserAlpha }}{{ "2021-6-3 2:46:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

So just to be sure; the main function returns 0 when successful while sub-functions return 0 (or -1 or sth else but in general 0) when failing?

{{ UserCoffee }}{{ "2021-6-3 6:30:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Yup. Sorry about that. Error if non-zero exit statuses for commands, yet 0 in C is false in condition expressions and frequently used for errors. System calls are the ones that pretty much uniformly use -1 for error. For commands it’s consistent: 0 means success. For C functions, have to check the manual to know the error return.

{% comment %} thenwait 56 {% endcomment %}{{ UserCoffee }}{{ "2021-6-3 7:26:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Also, I didn’t put this in the main lesson because I’m trying to focus on what C has instead of what it does not have, but there is no uniform mechanism when an error occurs to know what the error is that caused it. For system calls, the cause of an error can generally by displayed to `stderr` using the `perror`, see `man perror` for details, and the references from there if you want to do your own custom processing using the global variables `perror` uses. Only call `perror` after there actually is an error, or it might print a message about something that happened a long time ago in your program that ended up not actually being a problem. For non-system call functions like `scanf`, you pretty much have to figure out in your own code if the function failed because of something you did, like calling the function wrong, or something the user did, like typing an invalid number, or because of a system problem such as lack of system resources or device failure. `Scanf` is not frequently used in production code, and for the functions that are used it is usually easier to tell if the problem is user error or a system error.

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-6-3 7:29:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

For user errors, it is up to the C programmer to figure it out and print an appropriate message, just `fprintf(stderr, “User error.\n”) ; exit(1) ;` is frowned upon these days. Back in the day, the poor lusers were lucky if they even got that much info. However, if it is a system error, then it has to be a problem that occurred during a system call, such as a failed low level read on a device or the terminal due to a mechanical problem, or the computer runs completely out of memory space to perform something, and so on. The function `ferror(stdout)` will return true or false if there was an error writing ‘stdout’ using the standard library functions, e.g. if the output was redirected to a file on some device that ran out of disk space; there are other means to detect errors in other circumstances, see the manual pages for the library functions you call. For any system error, the error had to occur in some low level system call, whether you called it directly, or you called a library function that called the system call, so you can just use `perror()`, check the manual page of ‘perror’ for usage.

{{ UserAlpha }}{{ "2021-6-3 11:46:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

"Functions are syntactically like variables in C" I can't get grasp on this sentence. Could you maybe describe the effects of this fact?

{{ UserCoffee }}{{ "2021-6-3 12:14:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Good question.

{{ UserAlpha }}{{ "2021-6-3 12:15:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I made the argument functions are more like constants than like variables

{{ UserZeta }}{{ "2021-6-3 12:15:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I think I grasp it. But i'm not sure if i'm right

{{ UserCoffee }}{{ "2021-6-3 12:29:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

By saying functions are syntactically variables, I meant in the language syntax, i.e. a file is a series of declarations, declarations are of variables, so in the C syntax, the way code is typed and not how it works or what it means, a function is a variable.

{{ UserAlpha }}{{ "2021-6-3 12:29:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

My point was that you cannot assign sth to a function just as cannot assign sth to a constant

{{ UserCoffee }}{{ "2021-6-3 12:30:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

The language semantics says how it works and what the code means, in C functions are not semantically much like variables. In other languages functions actually are semantically a lot like variables, just not in C.

So that was a very good question.

{{ UserAlpha }}{{ "2021-6-3 12:30:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

while a variable can be left and right of the single = sign

{{ UserCoffee }}{{ "2021-6-3 12:31:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Not all variables can be left of an `=` sign. Some of the ones which can't, besides function, are covered two lessons from now.

{{ UserAlpha }}{{ "2021-6-3 12:31:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

okay we will see then 🙂

{{ UserCoffee }}{{ "2021-6-3 12:33:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

My main reason for pointing out that functions are syntactically like variables, is that if you have the declarations `int g ;` and `int g() ;`, which declares the function 'g' without giving a defintion for it, the two names will collide. It's like `int x ;` and `int x ;`, only the one in the most inner set of '{' '}' will be used, and you best not put two declarations of the same variable name within the same inner set of '{' '}'.

Function declarations, like `int g() ;`, can go anywhere in C any other variable declaration can go. All functions must be declared before you use them, but the actual definition, with the statements that go in the function, can be used later in the source file, or the definition can even be in another source file.

{{ UserAlpha }}{{ "2021-6-3 12:35:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

so function and variable names must be unique

{{ UserCoffee }}{{ "2021-6-3 12:37:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

No, you guys already did a test with `{  int x ;    { int x ;`, so you can declare multiple times, as long as the declarations aren't all in the same inner '{' '}'. `{ int x ; int x` would be an error.

{{ UserZeta }}{{ "2021-6-3 12:40:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Is this statement true:

         for (n = 2 ; n <= 10 ; n++);
         Output of for function = x
         Therefore x = for
                  		
Therefore the computer treats X and For function as basically the same thing

{{ UserCoffee }}{{ "2021-6-3 12:42:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Nope. Statements are completely separate from functions. 'for' doesn't have a value in C, you can't declare it or redeclare it. 'for' and 'while' are just ways of telling the computer to loop. Simple, much simpler than some other languages where they do have values. These are called "reserved words" in C, and you can't use them for anything else.

Oops. C does have constant variables, they were added to the language, and almost nobody uses them and everyone still uses the original way of doing constants, it is very well understood and powerful, but I was mistaken. Sorry about my imperfrctions.

{{ UserCoffee }}{{ "2021-6-3 5:08:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

All variable names must be unique within the same inner ‘{‘ ‘}’, i.e. you can do `{ int x ; .... ; } ... { int x ; .... ; }` and even `{ int x ; .... { int x ; ... } ... }` but you can’t do `{ int x ; int x ; ..... ` The reason I am repeating this is because variables can be defined outside of any ‘{‘ ‘}’ at the top level of the source file as well as after ‘{‘. For example: `int x ; int main() { ..... ` This is not done so much anymore because using these variables wrong can cause your code not to be reentrant, something I will explain later. However, all functions in C, as opposed to other languages, are always defined at the top level. You can declare a function inside ‘{‘ ‘}’, eg `{ int x ; int f() ;` This is a definition of ‘x’ and supersedes any prior definition. But for a function such as ‘f()’, it is taken to merely  be a declaration of the function which must be defined elsewhere at the top level. Because of all this, because functions are always defined at the top level, all function definitions are at the same level, the top level, and all function names in C must be unique.

{{ UserZeta }}{{ "2021-6-3 5:29:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

When are there multiple kernels?

{{ UserCoffee }}{{ "2021-6-3 5:44:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Did you read the assignment?

I don’t know what context to answer your question in if it is based on the assignment material or something else.

{{ UserZeta }}{{ "2021-6-3 6:16:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

“The core operating system part is called the kernel. This part is always running, and there is only one if it in the computer.”<br>
<br>
Are there other devices that have more then one kernel? Maybe a board that essentially has a bunch of small computers on it to complete a much larger task. All being coordinated by one master kernel. Maybe we call that one an A.I..


{{ UserCoffee }}{{ "2021-6-4 5:36:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Ummm. The kernel mostly keeps track of system resources assigned to each particular process, e.g. what RAM they have. It hands out these resources as requested by a process, and reclaims them if the process indicates it no longer needs them or if it exits. It also does low level access to the hardware, such as low level reads and writes to disk drives and user input/output devices. In order to do this, it needs to keep track of all this, it needs to have some memory space for itself, besides the memory space for all the processes. If process A gives up some memory space to be reclaimed by the kernel, then process B requests more memory space, it might get what process A no longer needed, so all of this space to keep track of what is going on has to consider all the processes, everything that is running on the computer, all together, so there can be only one memory space for the kernel. The kernel is not a process, it is more like a set of low level functions, that are accessed using "system calls", which go into the kernel memory space typically using "software interrupts", as I talked about in the lesson. So in fact there is just one kernel memory space, and you are right.

{% comment %} thenwait 16 {% endcomment %}{{ UserCoffee }}{{ "2021-6-4 5:52:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

@Z As I just explained in the previous comment on this thread, it doesn't really make sense to have more than one kernel, when the kernel is the memory space used to organize resources for what is going on, since you want to allow these resources to be available for whatever needs them, e.g. process B getting memory space that process A had previously used. However, just in terms of having multiple pieces of independent hardware running con-currently, that is present in every multi-core microprocessor chip and every system with multiple microprocessor chips. Fun computer fact: the first multi-core chip I know of was actually for doing AI from a long time ago, the cores were very simple and slow, and it was a flop. Better to just write efficient code that ran on the best microprocessors available at the time.

I have been avoiding discussing multi-threaded code. Each process is separate from the other processes, it has its own memory space, its own current directory (as set by the `cd directory` command of the shell that directly or indirectly spawned the process), a lot of independent stuff. However, it is possible to have multiple running threads share the same process with the same memory space. This is a very-very hot poker topic: say one thread sets a variable to a value, then a tiny fraction of a microsecond later another thread sets the variable to some different value, and then the first thread uses it assuming it is still the earlier value it set, ooooops. If the variable is a pointer (pointer variables and expressions are today's lesson): Big Oooooops. And, it is very hard to debug this, because the exact scheduling isn't going to be the same each time you run the code to test it, so maybe you have to run it many times before this sequence happens and it crashes, and crashes badly. Soooo, you can do this, the kernel fully supports having multiple threads in the same process, but it is not something I will discuss further in these lessons because it is very hard to make it consistently reliable; you need to study threading models and that is a whole class in itself.

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-6-4 6:09:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Also, I keep saying every process has its own memory space. There are exceptions. If the same code is running in multiple processes at the same time, the kernel generally arranges to share the memory space of the code part of the processes. Also, processes can ask that part of their memory space be shared with other processes using the kernel shared memory requests. This is a good and high speed way of inter-process communication, as long as you are careful about what kind of data is stored in the shared memory space and avoid the problems discussed in the previous multi-threaded code paragraph.

{{ UserAlpha }}{{ "2021-6-4 6:09:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Im fine with discussing multi threading muuuuch later 😉

actually (and please don't feel pushed or criticised) I'm super keen on the part where we define a "real world question" (like I need to count the Xs in this text file) and then transform to a running program. But again I'm aware we're at the very beginning

{{ UserCoffee }}{{ "2021-6-4 6:13:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

You should be able to do that after tomorrow's lesson. In fact, I think that would be a great homework question! A reasonably efficient program to count the number of Xs in a text file can be written in C using a few simple lines.

{{ UserAlpha }}{{ "2021-6-4 6:14:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

....well its almost always "few simple lines"...for you 🙂 nor for us ...yet

{{ UserAlpha }}{{ "2021-6-4 11:59:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

When I declare the function after (or rather below) the main program I get an error/warning, saying its "implicit". Does it have to be before/above the main?

{{ UserCoffee }}{{ "2021-6-4 12:04:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Good question. You can leave the function definition where it is, and just add a declaration for it above main, which is written like `int f() ;`, where the function definition after main is `int f() { .....` “implicit declaration” messages are almost always a bad thing because then the compiler can’t properly check the types, maybe you defined it as returning ‘float’ in one source file but the compiler didn’t know that in another file and implicitly declared it as ‘int’.

{{ UserAlpha }}{{ "2021-6-4 12:05:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

thx

can we retrun more than one values from the function if needed?

{% comment %} thenwait 4 {{ UserAlpha }}{{ "2021-6-4 12:11:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

let me guess..this comes later?

{{ UserCoffee }}{{ "2021-6-4 12:12:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

No. How to do that is the next paragraph in the lesson I am writing for today. You are officially psychic.


{{ UserZeta }}{{ "2021-6-4 8:52:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

But back to topic. Regarding my favourite Hot-Poker the  Pointer: What is the difference between "a Pointer" and "a Pointer variable" ?

{{ UserCoffee }}{{ "2021-6-6 9:17:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

A pointer is the value of a pointer variable. The pointer variable stores the pointer, just like an ‘int’ variable stores an ‘int’.

&x is a pointer to x, a value.  And `int *some_int_pointer ;` declares a pointer variable.

{{ UserAlpha }}{{ "2021-6-6 9:27:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Okay so could there be a pointer to that pointer variable too? like &some_int_pointer ?

{{ UserCoffee }}{{ "2021-6-6 10:53:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

You betcha! That is actually a very useful thing in some situations, especially when you start setting up more involved structs and pointers to them like in today's lesson. I wasn't going to include that in the lesson because it is a more advanced topic. Maybe it would be good in the homework!

{{ UserAlpha }}{{ "2021-6-6 12:10:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

int *pointer_to_apointer = &some_int_pointer ? ...and so on ...forever???

{{ UserCoffee }}{{ "2021-6-6 12:13:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Of course. C and other modern languages do not set limits. If it is semantically meaningful, they are supposed to support it. If you can dream up a case where you want to do something like that, you can do it!
