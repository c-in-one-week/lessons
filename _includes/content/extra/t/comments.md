{% include startcomment.html -%}
<!---
Test Programs go here.
Place the source for the test programs after the following arrow line:
-->


{{ UserZeta }}{{ "2021-5-30 3:40:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Leap Year!**

    #include <stdio.h>

    int main() {
    int year ;
    int is4_digit ;
    
    //Here is where we are checking for what year it is
    // and giving the user and chance to enter the value.
    if (printf("what year is it? ") , scanf("%d", &year))
    
    //Here we check to see if the year is a 4 digit value.
    if (year > 999 && year < 10000) is4_digit = 1;

    // num % 2 computes the remainder when num is divided by 2
    if ( year % 4 == 0 )
    { 

      if ( year % 400 == 0 ) printf("It's a leap year!");
      else printf("Also not a loap year!");
    }
    else {  printf("It's not a leap year!") ; }

    return 0 ;


{{ UserCoffee }}{{ "2021-5-31 8:24:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Some of what <u>@Student Z</u> told me yesterday, where he was saying the IDE was restricting stuff, I realized about 2:30am may not actually be the IDE's fault. What he was describing sounded like the IDE is expecting C++ for code that is actually in C. Technically, C++ includes C, but some of the defaults are different, especially for the I/O libraries. If you rename this source file from "whatever-filename.cpp" to "whatever-filename.c", and change the #include line at the top to `#include <stdio.h>`, then the IDE should automatically realize you are using C and you may find that that fflush, scanf, and so on work a lot better.

{% comment %} thenwait 1388 {% endcomment %}{{ UserCoffee }}{{ "2021-6-1 7:32:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

fflush(stdout)? Says the teacher. Oh but teacher, it worked fine!!! Bad habits.......

{{ UserAlpha }}{{ "2021-6-5 3:28:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

    #include <stdio.h>
    int main(){
    // to make stuff more rable we clear the terminal (This function will work on ANSI terminals)
    printf("\e[1;1H\e[2J");

    //decalring our 2 protagonists
    int x = 5 ;
    int *pointer_to_x = NULL ;

    // initial situation
    printf("X-Value = %d \nX-Pointer = %d\n",x,pointer_to_x) ;

    // now we assign the poniter pointing to x, to the pointer
    pointer_to_x = &x;
    printf("after pointer assignment:\n") ;
    printf("X-Value = %d \nX-Pointer = %x\n",x,pointer_to_x) ;

    //now using pointers to reaasign X;

    *pointer_to_x = 10;
    printf("after Pointer reassignment:\n") ;
    printf("X-Value = %d \n", x) ;
    printf("X-Pointer = %x \n", pointer_to_x) ;

    //now we extract the pointer x directly from x (the variable)
    printf("X-Pointer extracted from x = %x \n", &x) ;

    //and finally we extract the value of x directly from the pointer
    printf("X-Value extracted from X-Pointer = %d \n", *pointer_to_x) ;
    }

