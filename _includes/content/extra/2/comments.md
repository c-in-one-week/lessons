{% include startcomment.html -%}
<!---
Comments for lesson 2 go here.
Place the source for the comments after the following arrow line:
-->

{{ UserZeta }}{{ "2021-6-1 1:39:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}


    #include <stdio.h>

    int main()
    {
        float x ;
        
            printf("enter x: ") , fflush(stdout), scanf("%f", &x) ; 
            while (x <= 20) 
              
                    { printf( "beginning of while loop \n" )   ;
                        for ( printf("x:") , x = scanf("%f", &x) ; x <= 20 ; x = (x * 2)  ) 
                        {
                            printf( "count x = %f\n" , x) ;
                        }
                    }
                
        printf(  "x = %f\n", x) ;
        return 0;
    }

This program doesn't do what I expect it to do and only raises questions.

{% comment %} thenwait 7 {% endcomment %}{{ UserZeta }}{{ "2021-6-1 1:48:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

The result from the terminal<br>
> enter x: 2<br>
> beginning of while loop <br>
> enter x:2<br>
> count x = 1.000000<br>
> count x = 2.000000<br>
> count x = 4.000000<br>
> count x = 8.000000<br>
> count x = 16.000000<br>
> x = 32.000000

{% comment %} thenwait 5 {{ UserZeta }}{{ "2021-6-1 1:53:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Why does it ignore the values for 'x' that I have entered? It seems to arbitrarily do the equation (x = x *2) where x = 1.

{{ UserCoffee }}{{ "2021-6-1 1:56:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Hint: start with copy/paste of the illustration code I put up for this lesson into the compile screen. Then just put the `for` inside of the ‘{‘ ‘}’ of the `while`.

{{ UserZeta }}{{ "2021-6-1 2:28:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}


    #include <stdio.h>

    int main()
    {
        float x ;
        float Total = 0 ;
        while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) {
        for ( float d = 0.1 ; d <= x ; d = d * 2 ) 
            {
                //printf("D = %f\n" ,d) ;
                Total = Total + d ;
            }
        printf("Total = %f\n" , Total) ;
    }
        return 0 ;
    }
`

{{ UserCoffee }}{{ "2021-6-1 7:43:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Looks good!

{% comment %} thenwait 11 {% endcomment %}{{ UserCoffee }}{{ "2021-6-1 7:54:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

A used the cool formatting for the printf %f. This is good, but I like the formatted one more pretty.

{% comment %} thenwait 12 {% endcomment %}{{ UserCoffee }}{{ "2021-6-1 8:06:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

One point: older C compilers using older C standards only allow declarations outside of any functions or right after ‘{‘. The newer Apple and Linux and Microsoft Visual C compilers will accept the newer form `for ( float d = 0.1; ....`, but especially in this case where there is a ‘{‘ close to the ‘for’, I would have written `while ( ...... ) {   <on to the next line>    float d ;    <on to the next line>   for ( d = 0.1 ; .... ` The construct ` for ( float d = 0.1 ; .... ` is not correct according to some C standards used by some older compilers that still exist, so some people would not consider it portable. Portable C code, that compiles and runs anywhere, is important. When I was younger one of the main compilers was called the Portable C Compiler, PCC, not just because it was good for portable code, but because the compiler itself was portable, the one compiler wrote machine instructions for many different computers using complicated tables for each computer saying how the instructions for each different computer worked.

{% comment %} thenwait 43 {% endcomment %}{{ UserCoffee }}{{ "2021-6-1 8:49:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

C has only three constructs. Declarations, statements, and expressions; with a few syntactic constructs such as comments thrown in. A C source file is just a series of declarations. All the code is statements inside of function declarations that define functions, such as inside `main`. This lesson Number Two gave the basic declaration syntax and it listed all of the statements. However, there are a number of expression operators we didn’t cover. Specifically for the next homework, there are more assignment operators than just ‘=‘. Many binary operators, such as ‘+’ and ‘-‘, have corresponding assignment operators: `a += b` is an assignment operation that adds b to a, so it is similar to `a = a + b`.  The value is the result, `c = a += b` assigns `a + b` to a and c. These assignment operators such as `a += b` are frequently more efficient than `a = a + b` because the machine usually has a hardware accumulator that works that way. There are two places in today’s assignment that could use assignment operators. Also, there are increment and decrement operators, `++x` is called preincrement x and adds one to x and is equivalent to `x += 1`, `—x` (two dashes before x) is called predecrement and subtracts one . `x++` is called postincrement x and is exactly like preincrement, except the value of the `x++` expression is the value before the increment. `x = 5 ; y = x++ ;` sets y to 5, even though x is set to 6. This is particularly useful, one might use x with a particular value inside a loop, the last time ‘x’ is used inside the loop body use `x++` to go on to the next bigger value automatically if that is needed for the next time around the loop. `x—` (two dashes after x) is called postdecrement and works the same as postincrement, except the value is decreased by one. The increment and decrement operators are frequently very efficient, many machines have special ways to execute them depending on the context.

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-6-1 8:52:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

**Homework Two for Number Two:** Change the previous homework code for this lesson to use assignment operators, and also add to this code an int to count the number of iterations of the ‘for’ loop by using an increment operator on the counter, and include the count in the output line. Let me know if you have any questions, and feel free to paste your code here.

{{ UserZeta }}{{ "2021-6-1 9:53:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

    #include <stdio.h>

    int main()
    {
        float x ;
        float Total = 0 ;
        int loopcounter = 0 ;
        while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) 
        {
        for ( float d = 0.1 ; d <= x ; d = d * 2 ) 
        {
            for ( loopcounter = 0  ; loopcounter <= d ; loopcounter++) ;
            //printf("D = %f\n" ,d) ;
            Total = Total + d ;
        }
        printf("Total = %f\n" , Total) ;
        printf ("Count= %d\n", loopcounter) ;
    }
        return 0 ;
    } //This still doesn't work`

{{ UserCoffee }}{{ "2021-6-2 12:35:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

    #include <stdio.h>
    
    int main()
    {
    float x ;
    float Total = 0 ;
    /** tim: no need to have loopcounter here, it is used inside only.
        It would still work with the definition out here,
        just a little harder to understand **/
    int loopcounter = 0 ;
    while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) 
    {  int loopcounter = 0 ;
       /** tim: probably a better place to declare loopcounter.
           If you don't declare it here, you at least have to set it to zero
           here or in the following 'for' initialization **/
        for ( float d = 0.1 ; d <= x ; /** d = d * 2  **/ `d *= 2`) 
        {  
            /* for ( loopcounter = 0  ; loopcounter <= d ; loopcounter++) ;
                 ** tim: not sure why this for loop is here? **/
            //printf("D = %f\n" ,d) ;
            /** Total = Total + d ; **/ `Total += d` ;
            loopcounter++ ; /** tim: this is all you needed to add besides
                                     the declaration and print,
                                     it just increments once each time
                                     around the loop **/
       }
        /* printf("Total = %f\n" , Total) ; */
        /** tim: print loopcounter **/
        printf("Total = %f Count = %d\n" , Total, loopcounter) ;
        printf ("Count= %d\n", loopcounter) ;
    }
    return 0 ;
    } //This still doesn't work

{% comment %} thenwait 6 {{ UserCoffee }}{{ "2021-6-2 12:41:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Does this work? I got confused by the inner loop that I am not sure why this is here, and put the loopcounter declaration in the wrong place at first. Hopefully I got it right the second time. Sorry, I'm not perfrect.

{{ UserZeta }}{{ "2021-6-2 1:02:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I tried something similar to this but it didn't work because I didn't place the loopcounter++ into the correct set of {}.


    #include <stdio.h>

    int main()
    {
    float x ;
    float Total = 0 ;
    int loopcounter = 0 ;
    while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) 
    {
         
        {
        for ( float D = 0.1 ; D <= x ; D = D * 2 ) 
            {
                //printf("X = %f\n" ,D) ;
                Total = Total + D ;
                loopcounter++ ;
            }
        }
        printf("Total = %f\n" , Total) ;
        printf("Count= %d\n" , loopcounter) ;
    }
    return 0 ;
    }

{{ UserCoffee }}{{ "2021-6-2 1:06:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

That’s very good for a first attempt. The problem is you want to count the number of times around the ‘for’, not the ‘while’, so move the loopcounter definition and initialization to 0 right before the ‘for’ and it would be perfect.

{% comment %} thenwait 14 {% endcomment %}{{ UserCoffee }}{{ "2021-6-2 1:20:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Actually, you also have to move the definition with initialization to 0 of Total to the same place if you want a separate total for each value of x. In the assignment I said to calculate the same function for each value of the for loop index, print out the total of all the calculated values. I personally would grade your answer as ‘correct, because I wrote the question ambiguously’, since I didn’t explicitly say the total of all calculated values for a particular x. That is usually implied when summing up something in an inner loop, if all the values for all x were requested typically the request would be for the grand total. But the question is ambiguous as written, have to admit it.

{{ UserZeta }}{{ "2021-6-2 1:22:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

The commented line will print out the totals for X at each iteration of the equation.

{{ UserZeta }}{{ "2021-6-2 1:29:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

    #include <stdio.h>

    int main()
    {
    float x ;
    float Total = 0 ;
    while (printf("enter x: ") , fflush(stdout), scanf("%f", &x)) 
    {
            int loopcounter = 0 ;
        {
        for ( float D = 0.1 ; D <= x ; D = D * 2 ) 
            {
                //printf("X = %f\n" ,D) ;
                Total = Total + D ;
                loopcounter++ ;
            }
        }
        printf("Total = %f\n" , Total) ;
        printf("Count= %d\n" , loopcounter) ;
    }
    return 0 ;
    }
