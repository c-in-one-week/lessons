{% include startcomment.html -%}
<!---
Comments for lesson 1 go here.
Place the source for the comments after the following arrow line:
-->
<br><br>
{{ UserZeta }}{{ "2021-5-30 12:22:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}


    #include <stdio.h>

    int main()
    {
        int x ;
        int y ;
        int t ;
        if (printf("enter x: ") , fflush(stdout), scanf("%d", &x)) {
            y=1000000 ;
            t= 42 ;
            printf("x = %d\n", x) ;
            x=x*y+t ;
        printf("x = %d\n", x) ;
        } else {
            printf("failed trying to read an integer.\n") ;
            return 1 ;
    }
        return 0 ;
    }

@Coffee

{{ UserCoffee }}{{ "2021-5-30 12:51:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Just curious- why did you use separate variables for y and t, instead of just one big equation with constants? I think it is good the way you did it, especially if the constants have meaning names from some formula in a text book or something, I was just wondering if that is why you did it that way.    Also, why did you change the value of x instead of creating a new variable for the result? Frequently it's not such good practice to change your input values, in case you later need to modify the function and you need the input value for something else, although some people always change variable values like that in order to save a little memory.

{{ UserZeta }}{{ "2021-5-30 12:52:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

It's more understandable for me in terms of math.

honestly I dint' get the parenthesis part with "," operator..

{{ UserCoffee }}{{ "2021-5-30 12:59:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

`a = b` is an expression in C. `f(x)` is also an expression. `a = b, f(x)` is a two level expression. But how will it be grouped? With `a = 2 * x + 4`, you know the * has higher precedence, so it will be grouped a = (2 * x) + 4. But what about `a = b, f(x)`. Will it be grouped `a = (b, f(x))`, where the result of the comma operator `b, f(x)` is f(x), so the value of f(x) is assigned to a. Or will it be grouped `(a = b), f(x)` where the assignment is directly of a = b, and f(x) is just called separately. Honestly, I am not sure, since I always use the parenthesis. I hate code that is that tricky to understand clearly. I'll check and get back to you in a second.

Will a be set to b or will a be set to f(x)? The mystery continues. For good code, I hate mysteries.

And the winner is: b! Comma has the lowest precedence of all expression operators, which I thought but I haven't actually read the manual on that since (well, a long time ago), so '=' binds tighter and it is grouped `(a = b), f(x)`, and a is assigned to b.

OOooooooooo

I looked up and now I am smiling!

{{ UserAlpha }}{{ "2021-5-30 1:05:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

so:

a = b, f(x) is the same as (a = b), f(x) ?

{{ UserCoffee }}{{ "2021-5-30 1:06:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Exactly the same. Adding parenthesis to an expression in C never changes it, as long as you add the parenthesis in the right place according to the precedence and associativity.

{{ UserAlpha }}{{ "2021-5-30 1:07:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

so in your 1st example you had the if statement applied to the 3 commands.

So the return value of the last statement is actually checked?

{{ UserZeta }}{{ "2021-5-30 1:07:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

So the parenthesizes just make the code easily to read and understand.

{{ UserCoffee }}{{ "2021-5-30 1:08:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Ummm technically a function call is an expression. C has expressions and statements. Commands are things like running the C compiler. But yes, the last expression in the comma separated list is what is checked by the if statement. That is what you want, since that is the scanf, which is the function getting the input, and you want to check if there was integer input.

Umm sometimes you need parenthesis, because the default precedence and associativity isn't what you want. Depends on the particular expression.

{{ UserAlpha }}{{ "2021-5-30 1:09:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

so what if the return value isnt a boolean?

that will crash right?

{{ UserCoffee }}{{ "2021-5-30 1:11:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Boolean is not a C type. The word doesn't really mean anything in C, C is simpler than that. All C conditionals `x` are equivalent to `(x) != 0`, so you can put anything in a conditional that can be compared to 0.

{{ UserAlpha }}{{ "2021-5-30 1:11:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

...didn't know that

{{ UserCoffee }}{{ "2021-5-30 1:11:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

C doesn't have a lot of types compared to some languages. int, float, char, pointer.

Arrays of those, pointers to those, structures containing those. Long and short of int and float. Unsigned int and char. That's about it.

{{ UserAlpha }}{{ "2021-5-30 1:12:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

so you could smething like if(x-9){} which would be true for x==9 and false for all other values of x?

{{ UserCoffee }}{{ "2021-5-30 1:13:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Oops. `Short float` isn't even a C type, just `long float`, which is also called `double` (exactly the same thing). Not so many types at all.

Yes you could, and I see that all the time, although generally not in so obvious a case as that, since the `==` construct is easier to read.

I don't think my mic is working on my linux partition. Sorry, I'm trying. It's lunch time anyway.
