{% include startcomment.html -%}
<!---
Comments for lesson 3 go here.
Place the source for the comments after the following arrow line:
-->

{{ UserZeta }}{{ "2021-6-4 8:06:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}


    #include <stdio.h>

    int my_big_task(int x)
    {
        int n ;
        if (! (printf("enter n: "), fflush(stdout), scanf("%d", &n)))
            return 0 ;
        printf("x = %d\nn = %d\nx * n = %d\n", x, n, x * n) ;
        return 1 ;
    }

    int main()
    {
        int x ;
     while (printf("enter x: ") , fflush(stdout), scanf("%d", &x)) 
     {

            if (! my_big_task(x)) 
        {
            fprintf(stderr, "Error: Invaild value for N \n") ;
            return 1 ;
        }
     }
         fprintf(stderr, "Error: Invaild value for X \n") ;
        return 0 ;
    }
