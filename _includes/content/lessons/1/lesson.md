<!---
_includes/content/lessons/1/lesson.md   content for lesson 1 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{{ UserCoffee }}{{ "2021-5-30 8:26:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I don't have time for this.<br>
<br>
<br>
Learn C in 7 days, just a little bit each day.<br>
<br>
The lesson names are not related to the content.<br>
<br>
1\. In the beginning<br>
2\. Number two<br>
3\. Three amigos<br>
4\. Four square<br>
5\. A fifth<br>
6\. Highway 66<br>
7\. 7th Heaven<br>
<br>
For fun and profit!<br>
<br>
<br>
I really don't have time for this.

<br>

{% comment %} thenwait 4 {% endcomment %}{{ UserCoffee }}{{ "2021-5-30 8:30:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

The book [K&R](/resources/ "K & R") is the bible for the C language. <span title="I modified a little because of changes in C">It starts with this program.</span>

    #include <stdio.h>
    
    int main()
    {
        printf("hello, world!\n") ;
        return 0 ;
    }

This uses '`printf`' to display 'hello, world!'. The rest of the program is explained in later lessons. Another simple program is one which adds two particular numbers together.

    #include <stdio.h>
    
    int main()
    {
        printf("two plus two = %d\n", 2 + 2) ;
        return 0 ;
    }

This shows how '`printf`' can print numbers, using '`%`' in the first argument to say where the numbers go in the output. The first '`printf`' argument is called the format argument since it controls the format of what is printed. A '`%d`' means the next argument is a number, and print it in decimal. A '`%x`' would print it in hex.

Another simple program that does exactly the same thing as the last one.

    #include <stdio.h>
    
    int main()
    {
        int x ;
        x = 2 + 2 ;
        printf("two plus two = %d\n", x) ;
        return 0 ;
    }

This program uses a variable, named 'x', to store the value of the numbers added together. A variable is something that can hold a value. There are several types of values. In this case the type is 'int', used for integers in C.

I suggest opening the C Code Academy page at [`https://www.onlinegdb.com/online_c_compiler`](https://www.onlinegdb.com/online_c_compiler "C Code Academy Online Compiler") and copy/pasting the code from the above examples into that page in order to try it out. (Thank you, <u>@Student Z</u>!)

If you make a typo writing these little programs, the computer will probably tell you what is wrong and which line the typo is on. Safe, with clear messages, easy to fix. After simple programs like this, they typically teach beginners basic code structure, such as how to repeat something multiple times. More safe stuff that will not cause a program crash. A crash typically causes the program to stop running after displaying some cryptic message such as 'Segmentation fault'. Scary.

<br>

They don't train beginners the scanf function, because it is too easy to crash your program if you use it wrong. However, back in the day they gave people hot pokers so they would learn which end to pick up; people were fearless back then. So, here goes....

    scanf(fmt, &arg1, &arg2, ...)

scanf is like printf, except instead of writing stuff out to the terminal, it reads stuff in from the terminal. Typically, you write a prompt first, such as printf("please enter your age: ") ;, then you call scanf to read the age.

The scanf fmt argument tells scanf what the types of the other arguments are, so it knows what to look for when it is reading stuff. If you make a mistake with this, your program will likely crash, because different types of arguments have different sizes, so scanf will be storing the data it reads using the wrong number of bytes, and possibly in the wrong location especially if there are multiple things being read, and everything will be smooshed up.

In C when a variable is used in a normal operation, its value is used, and the variable which is the source of that value is not remembered. However, C has 'pointers' to variables, which keep track of the variables themselves. Placing an '`&`' in front of a variable creates a pointer to that variable. Each of the non-fmt arguments to scanf is a pointer to a variable in your program. Scanf uses pointers so it knows which variables to store the values it reads.

First scanf example: read a number from the user at the terminal into the integer variable x.

At the top of the function after the curly bracket, declare the variable:

    int x ;

Then, to use scanf, just:

    scanf("%d", &x) ;

To write a prompt first:

    printf("enter a value for x: ") ; scanf("%d", &x) ;

To write a prompt first, and write the new value for x after you are done:

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-5-30 8:42:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

    printf("enter x: ") ; fflush(stdout) ; scanf("%d", &x) ;
    printf("x = %d\n", x) ;

Sometimes the prompt does not come out right away, it gets stored in internal buffers. To fix this, flush the internal output buffer by using the function `fflush(stdout)`.

{% comment %} thenwait 6 {{ UserCoffee }}{{ "2021-5-30 8:49:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

The scanf function returns the number of items it read. If you type an item of the wrong type, it can't read it, so it won't count it. I will now write a program that reads one integer from the user, and either writes a message that it couldn't read an integer, or writes out that integer.

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-5-30 8:52:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Here you go:

    #include <stdio.h>
    
    int main()
    {
        int x ;
        
        if (printf("enter x: ") , fflush(stdout), scanf("%d", &x) == 1) {
            printf("x = %d\n", x) ;
        } else {
            printf("failed trying to read an integer.\n") ;
            return 1 ;
        }
        return 0 ;
    }

Note: each source file that uses printf, scanf, fflush, stdout needs to include stdio.h at the top.

{% comment %} thenwait 4 {{ UserCoffee }}{{ "2021-5-30 8:56:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Note: the part inside the parenthesis after the 'if' is a conditional expression. It determines whether the statement after the closing parenthesis ')' will be done. A condition is true in C if it is not zero, so `if (x - 9)` is identical to `if (x - 9 != 0)`, which is identical to `if (x != 9)`. This is not a contest to confuse the innocent, so generally use the `if (x != 9)` form, yet in some circumstances, typically when checking things other than a number, the form without an explicit comparison operator is cleaner.

Note: putting several functions together in a list with commas will call them all in order, and has the value of the last function in the list. I did this with printf, fflush, and scanf, because the C `if` statement can not take multiple statements in the conditional part.

{% comment %} thenwait 5 {{ UserCoffee }}{{ "2021-5-30 9:01:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

Try typing several returns as input to this program, then a number. Or then a letter. Feel free to play with scanf, it is your friend if you respect its fmt argument. Always remember the '&' in front of the other arguments or scanf will bite you! Without the '&' the other arguments won't be pointers and scanf will definitely put the values in the wrong place, and you may have a very bad day.

Scanf is hardly ever used in real programs, because they have more complicated input parsing mechanisms. However, it is great for little test programs you might want to write to test out some new algorithm or something.

First **homework** assignment: write a program which reads an integer into a variable, and then prints out the variable value and also the variable value multiplied by a million plus 42. Feel free to copy and edit the above little program in this thread. Please paste your source code into this thread.

Also, if you have any questions, please do not hesitate to paste them into this thread.

Have a nice day!  😀
