<!---
_includes/content/lessons/5/lesson.md   content for lesson 5 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{{ UserCoffee }}{{ "2021-8-15 10:39:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% comment %} this was written in a text editor which do not mark exactly the date/times when the sections were written; I put these date times in as I proof-read the various sections {% endcomment %}

<a name="preamble"></a>

A Fifth

{{ UserCoffee }}{{ "2021-8-15 10:42:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Today's lesson directly builds on yesterday's lesson, Four Square. If you have any doubts about yesterday's lesson, if you haven't gone through it completely, it would be good to let me know before starting today's lesson. Yesterday's lesson had 9 compilable code examples. I compiled and ran them all before posting the lesson, so they should all work. Let me know if any of them did not work for you.

Much of this lesson was originally planned to go into Four Square, but I decided while writing it to make Four Square focus on the basics, so you would have a solid understanding of them before you went on. Four Square, pointers and 'struct', is the foundation of C. So even though Four Square was long, I am afraid this one may get even longer. Sorry.

The code examples in this lesson build from the first code example, which is quite simple, to later code examples, which get quite complicated. Each example adds new concepts to the previous example. More than for the previous lessons, it would likely be very confusing to start towards the end without a clear understanding of earlier examples.

There are 5 homeworks in this lesson. I suggest doing the first one, which is towards the middle of the lesson, before even reading the lesson material after it. Understanding what is in the first homework should make the material after it seem more like regular code, code that just assigns pointers and follows pointers to what they point to, instead of a mysterious way of performing magic on the computer.

{{ UserCoffee }}{{ "2021-8-15 10:47:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

<a name="lesson"></a>

**This Lesson**

Computers process information, data. Generally, a program doesn't just work on data for one thing, but a bunch of things. For example, you might want to find the tallest of a bunch of people, so you need heights for all the people. The homework of the last lesson showed a way to represent the height and birthday for one person. How do you deal with a whole bunch of heights for a whole bunch of people?

This lesson is about "*making a bunch of something*". The C language has two ways to do this. In the first way, a bunch can be put together using 'struct' and pointers. Each struct would have a pointer member, which points at another struct, which has a pointer member that points at another struct, and so on, to make a whole bunch of struct linked together by pointers. Another way is to make an array. In C, an array is just a bunch of the same type of variable, one after another in the computer memory space, so a whole bunch of data can fit in one array if the array is big enough. However, adding more data to an array is difficult once the memory space set aside for the array fills up. Also, it takes a lot of effort to add or remove data from an array except at the end, as we will talk about in the array section of this lesson, while it is easy to change a few pointers in some struct to add or remove data when it is stored in struct linked by pointers. And finally, in C, information is grouped into struct, which tend to be identified by pointers to them, so it is natural and easy to connect these struct by pointers, and to refer to the struct in code using pointers to them. For all these reasons, except some special cases such as those discussed in lesson 6, struct linked with pointers are often a more direct way of making a bunch of something than arrays. Therefore this lesson talks about making a bunch of data by linking struct together first, then there is a short section on arrays at the end.

Understanding how to make a bunch of data, and how to get around in the bunch using pointers, and modifying the pointers connecting the data, is a big part of C. Understand this lesson and you are pretty much 'there' as a C programmer, the rest is details.

For many problems, it is not necessary to store the whole bunch of data in the computer memory space to solve the problem at all. For example, to find the tallest of a bunch of people, a program could process their heights one at a time. It could set the height of the person being processed in one variable, and store the tallest height seen so far in another variable. If the height for the current person being processed is taller than the tallest height seen so far, it would store the new tallest height in the second variable. Presto, once all the people have been processed, the tallest height will be in the second variable, using just two variables without storing a whole bunch of heights. Usually, an algorithm like this that does not store a bunch of stuff in the computer memory space is more efficient. This lesson is not for this sort of algorithm. This lesson is for when a program needs to keep track of a bunch of things in the computer memory space.

{{ UserCoffee }}{{ "2021-8-15 10:54:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

<a name="lists"></a>

**Making A Bunch Of Struct Connected With Pointers**

A bunch of things can be stored by making a bunch of the same type of struct and linking them together using pointers in the struct. The struct would have a member which is a pointer to the same type of struct. The first struct in the bunch has a pointer in this pointer member to the second struct, the second struct has a pointer to the third, and so on, to make the whole linked bunch. Here is an example of a struct that contains a pointer to the same type of struct.

    struct element {
        struct element *next ;
        float somedata ;
    } ;

Here is an example of a four long bunch of data using linked struct. A sequential bunch of struct like this, linked together by pointers, is called a 'linked list'. Each single part of a sequential bunch is called an 'element'. This example sets up the pointer link members from each struct to the next. The pointer link member of the last element is set to NULL so you know you are at the end.

        struct element a ;
        struct element b ;
        struct element c ;
        struct element d ;
    
        a.next = &b ;
        b.next = &c ;
        c.next = &d ;
        d.next = NULL ;

You can get from each struct to the next by using the pointer link. For example, say you wanted the second struct, but at that location in your code you did not have reference to the variable 'b', just the variable 'a'. Using the pointer link '`a.next`' lets you get to the second struct. The whole four element list can be traversed this way.

<a name="figure1"></a>

<div class="lesson_diagram">
<img alt="Linked Named Struct by T Shaw, modified from Tmigler hosted at Wikimedia, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" src="/assets/images/structs.svg">
Figure 1 - Linked Named Struct
</div>

There is another way to set up the links exactly the same, but using pointer variables to the struct instead of the struct themselves. I will explain why I did this in the following paragraphs. See that this example actually does set up the links exactly the same as the previous example, that each assignment to the 'next' member that links the struct together sets exactly the same value.

        struct element a ;
        struct element b ;
        struct element c ;
        struct element d ;
        struct element *p1, *p2, *p3, *p4 ;
        
        p1 = &a ;
        p2 = &b ;
        p3 = &c ;
        p4 = &d ;
        p1->next = p2 ;
        p2->next = p3 ;
        p3->next = p4 ;
        p4->next = NULL ;

Let's check where this example sets '`p1->next = p2`'. Since '`p1`' is set to '`&a`' earlier, '`p1`' points to '`a`' and '`p1->next`' is the same as '`a.next`', remember from lesson 4 that `pointer->member` is the same as `struct.member` when the pointer points to the struct. Also in this example, '`p2`' is set to '`&b`'. Combining these, '`p1->next`' is the same as '`a.next`', and '`p2`' is the same as '`&b`', so '`p1->next = p2`' is the same as '`a.next = &b`'. You can check this where it sets '`p2->next`', '`p3->next`', and '`p4->next`', as well.

Why use pointers to the struct and the '`->`' operator on the pointers, instead of just accessing the struct members from each struct directly using the '`.`' operator? One reason is that in most programs, it is not known how big the bunch is when you start. Even if you did, it is frequently not easiest to make them all at once. The whole idea of linked lists is that you can easily add more data to the bunch as the program runs. So declaring struct variables one after another at the top, as in this example, is out.

<a name="allocation"></a>

Instead, you want to make each new struct variable as you need it. This is called allocation. In C, the typical way to allocate a new variable as the program is running is to use the 'malloc' function. This function creates a new variable, and returns a pointer to it. Whatever pointer variable you assign it to determines the type of the new variable, since C uses the type of a pointer to figure out what to do when that pointer points at something.

Here is a simple example of 'malloc', so simple it would never be used like this in a real program. It does not allocate a struct, I will show that a little later. It just allocates an 'int', one integer number.

        int *intptr ;
    
        intptr = malloc(sizeof(int)) ;
        *intptr = 7 ;
        printf("value of new variable is %d\n", *intptr) ;

This example allocates a brand new 'int' variable, and assigns intptr so that it points to it. It then assigns the value 7 to this variable through intptr. It then prints the value, 7, which was just assigned to the new variable. A variable which did not exist until 'malloc' was called.

The argument to the 'malloc' function is the size of the variable to be allocated. In the above example, an 'int' variable is being allocated so the 'sizeof' operator can be used on the 'int' type to get the size of the int variable. Besides using 'sizeof' on a type, the 'sizeof' operator can also be used on any variable, including one referred to by the pointer at '`*intptr`'. The following example does exactly the same as the previous, giving the same size argument to 'malloc' since '`*intptr`' is an 'int'. Using sizeof like this is better than the previous example because the size automatically adjusts if the type of intptr is changed in a later version of the code.

        int *intptr ;
    
        intptr = malloc(sizeof *intptr) ;
        *intptr = 7 ;
        printf("value of new variable is %d\n", *intptr) ;

Here is a compilable example of the above. Try it out! You have allocated your first variable.

    #include <stdio.h>
    #include <stdlib.h>

    int main()
    {
        int *intptr ;
    
        intptr = malloc(sizeof *intptr) ;
        *intptr = 7 ;
        printf("value of new variable is %d\n", *intptr) ;
        return 0 ;
    }

Let's do the previous four element linked list example, this time using 'malloc' to allocate each struct instead of declaring four individual struct.

        struct element *p1, *p2, *p3, *p4 ;
        
        p1 = malloc(sizeof *p1) ;
        p2 = malloc(sizeof *p2) ;
        p3 = malloc(sizeof *p3) ;
        p4 = malloc(sizeof *p4) ;
        p1->next = p2 ;
        p2->next = p3 ;
        p3->next = p4 ;
        p4->next = NULL ;

See how we don't create the 'struct' variables in the C source anymore, they are created while the program is running using 'malloc'? Here is a diagram of this four element linked list. Note that the individual struct do not have names since they are allocated with 'malloc', only the pointers to them are named.

<a name="figure2"></a>

<div class="lesson_diagram">
<img alt="Singly Linked List by T Shaw, modified from Tmigler hosted at Wikimedia, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" src="/assets/images/slist.svg">
Figure 2 - Linked List
</div>

You could add a fifth element to the linked list by just adding the four lines of code in the following example, and it would get allocated and put in the list whenever these lines are run.

        struct element *p5 ;
        p5 = malloc(sizeof *p5) ;
        p4->next = p5 ;
        p5->next = NULL ;

However, doing it this way is still limited by the number of pointer variables, 'p1', 'p2', ... declared in the source. Umm, not quite good enough. This can be fixed by using just two pointer variables, one that points at the first 'struct' element in the list and one that points at the last. Here is an example that sets up a list with one element using two variables like this. It allocates that element, then since there is no second element yet, it just sets the 'next' pointer member of the allocated 'struct' to be NULL.

        struct element *pfirst, *plast ;
        
        pfirst = plast = malloc(sizeof *pfirst) ;
        plast->next = NULL ;

Then, here is an example that adds another element, after the previous example has been run in the program to set up the list. It allocates the new element, setting the 'next' member of the previous last element to be the new element. Then it sets the plast variable to be the new last element. Finally, it sets the 'next' member of the new last element to be NULL.

        plast->next = malloc(sizeof *plast) ;
        plast = plast->next ;
        plast->next = NULL ;

This last example could be repeated as many times as you wanted, each time you wanted to add another element. You can always traverse the whole list by starting at pfirst, no matter how many elements you add. Understanding this example, how we can set up data structures at run time with an arbitrarily large number of elements, just using a few pointers and '`->`', is a key to understanding C.

Here is a compilable example of creating a list in a loop. Type as many values as you want, it will print them out each time and then add them up.

    #include <stdio.h>
    #include <stdlib.h>

    struct element {
        struct element *next ;
        float somedata ;
    } ;

    int main()
    {
        struct element *pfirst, *plast ;
        int neednewlist = 1 ;
        float newval ;

        while (printf("next value: "), fflush(stdout), scanf("%f", &newval) == 1) {
            if (neednewlist) {
                neednewlist = 0 ;
                pfirst = plast = malloc(sizeof *pfirst) ;
                plast->next = NULL ;
            } else {
                plast->next = malloc(sizeof *plast->next) ;
                plast = plast->next ;
                plast->next = NULL ;
            }

            plast->somedata = newval ;

            float total = 0.0 ;
            for (struct element *p = pfirst; p != NULL; p = p->next) {
                printf("%.2f", p->somedata) ;
                if (p->next != NULL) printf(" + ") ;
                total = total + p->somedata ;
            }
            printf(" = %.2f\n", total) ;
        }
    }

Now do you see why I used pointers and '`->next`' in that early example, instead of directly using the individual 'struct a', 'struct b', 'struct c', 'struct d', and '`.next`'? When using pointers and 'malloc' like this, there is no limit how long a list can be, until the computer runs out of memory. New elements can be added as needed, and in whatever part of the program code needs to add them.

If there is no more memory, 'malloc' will return 0 aka NULL. The return value of 'malloc' should always be checked, so the above examples should be changed to check it. Even if a computer has gigabytes of memory and you are only allocating a few struct, sometimes the amount of memory your program is allowed to allocate is much smaller than the total amount. Using a NULL return from 'malloc' without checking it will cause a confusing program crash. Also, you never know how much memory was already in use by other programs when your program was run. So check it.

If you are using an online C compiler to try out these examples, it likely severely limits the amount of memory you have available so that you do not impact their server or the other people using it. If you exceed your limits, the online C environment may just stop the whole program, however it may have malloc return NULL. So check it.

Using 'malloc' is called dynamic memory allocation, because you can allocate variables dynamically by calling the 'malloc' function. This has nothing to do with the use of the word dynamic in DRAM memory chips, where DRAM stands for Dynamic Random Access Memory and 'Dynamic' indicates the type of electric circuit used to store each bit value in the memory chips; nothing to do with software.

Variables declared in a function, like we have been using in previous lessons, only last until the end of the function. Once the function returns, accessing them will likely lead to all sorts of strange crashes or other bugs. The following is bad.

    int *do_not_do_this()
    {
        int x ;
        x = 7 ;
        return &x ;
    }

However, 'malloc' variables continue to be valid after the end of the function which called 'malloc'. A common C technique is to write a function which allocates a variable using 'malloc', checks to see if 'malloc' returned NULL, and then initializes the variable, as in the following example.

    struct element *newelement()
    {
        struct element *p ;

        if ((p = malloc(sizeof *p)) == NULL) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }
        p->next = NULL ;
        p->somedata = 0.0 ;
        return p ;
    }

In this example, the pointer 'p' is returned by the 'newelement' function, so that the variable it points to, the new variable created by 'malloc', can be used by whatever function called 'newelement' after 'newelement' returns. In fact, the new variable can be used by any function which has that pointer in the entire program, even after 'newelement' returns.

The memory space used for variables declared in a function is automatically available for re-use when that function returns. For the variables created by 'malloc', each call to 'malloc' uses up some memory space to make each new variable, space which is not automatically available for re-use when the function calling 'malloc' returns. The variable is still there, as the previous example shows. The function 'free' can be called on a pointer returned by 'malloc' when you are finished with it, so the space used by the new variable is available to be used again later. All the space is reclaimed anyway when a program exits, but it is good practice to free your 'malloc' variables.

If a program uses too much memory bad things happen. This can occur when the program runs for a long time and does not 'free' the variables created by 'malloc' when it is done with them. However, sometimes there is just not enough memory for a big problem. What usually happens when there is not enough memory is not a program or computer crash, instead the computer just becomes very slow. The memory space of a modern computer need not be entirely stored in the high speed RAM. The computer off loads memory space to the hard drive when it has not been used recently. Doing this is called 'virtual memory' and it is managed by the hardware and the kernel, and happens automatically without someone writing normal programs needing to know anything about it. When too much memory space is being actively used, then the system needs to off load memory to the hard drive that has been used more recently, and the computer gets slower and slower because it has to read more and more back in from the hard drive when it needs to access the off loaded recent stuff. These reads are called 'page faults', because virtual memory organizes the memory space into small chunks called pages. If your program uses 'malloc' a lot and your computer slows down, make sure the program uses 'free'. And buy more RAM. Who would guess this, that the main symptom of too much 'malloc' is a computer slowing down?

None of the previous paragraph matters anymore in most cases. Computers have gigabytes of memory. Page faults using SSD are faster than disk drives. A lot of programmers never call free. If somehow a program manages to allocate tons of memory, possibly due to a bug, it is reclaimed anyway when the program exits and the computer speeds back up again. However, if you want to write the big stuff, especially for a program that could run for a while, for example an interactive engineering program to design large systems, then you need to fully understand memory management.

The variables in the space returned by 'malloc' are not initialized. They may be all 0 in your test cases, then when running a real case suddenly be something else, and the program may not work. Do not assume the value of these variables are anything until you set them to something, the same as you have to set variables defined in a particular function to some value before you know they have a well defined value. Not initializing variables such as those allocated by 'malloc' is a common hot poker issue.

There is another function called 'calloc'. It is just the same as malloc, except it clears the memory to zero before it returns. Some people like 'calloc', since if they forget to initialize the memory, it will be initialized the same way every time and the program will crash the same way every time. Also, if you need the memory to be zeroed, then 'calloc' is frequently much more efficient than using 'malloc' and clearing it yourself. The 'calloc' function takes two arguments, first the number of elements, the second is the element size, but internally it is pretty much the same as 'malloc' with the number of elements and element size multiplied together. Up to your judgement which one to use.

Another hot poker alert: using 'free' on something not returned from 'malloc' or 'calloc', or using 'free' on the same thing more than once, will corrupt the free list. Hopefully this will cause an immediate crash, but it may crash some time later during an unrelated 'malloc' or other allocation activity, and be very difficult to figure out where the actual error is and debug it. Most development systems have special ways of debugging this. When running with the special debugging allocation library, the program slows down a lot, but it checks every free and other things to help find any problems. Useful to know how to activate this if you write a lot of code; allocation problems can be the hardest to debug.

{{ UserCoffee }}{{ "2021-8-16 12:21:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

So far we have been using singly linked lists, where each element has a single pointer, which points to the next element. A singly linked list can only be traversed in one direction, since each element points at the next element, but the next element does not point back at the previous. A doubly linked list is a list where each struct has pointers to the linked struct in both directions, allowing a doubly linked list to be traversed in either direction. Either side of a doubly linked list could be the end of the list traversal, so it is frequently called a 'dequeue', short for 'double ended queue', where queue is used because our British friends have their own silly words for things. Many list operations are simpler with doubly linked lists than singly linked lists. For example, you can remove an element with just a pointer to the element to be removed, since that element has pointers to both the previous and following elements, you can directly access those elements from the element being removed in order to remove it. I will explain removing an element in more detail in two paragraphs.

Typically, a doubly linked list has an empty list element whose forward link points at the first element, and whose backward link points at the last element. Any other data in the list element 'struct' is not used for the empty element. This empty element is called the list 'head'. The empty head is just a dummy element, it is not part of the list data elements. Pointers for the entire list typically point at the head, and can get from there to the first or last data element. If the list is empty, the head element points at itself. Using a head element greatly simplifies additions and deletions to lists, since then there is no requirement for a special case for a completely empty list with no elements, every list always has at least the head 'struct' to work with. Also, you can refer to the list as a whole by pointing at the head even while the list is changing, since the head just sits there even if other elements are added or removed. With all these advantages of doubly linked lists and an empty head element, they are frequently the go-to method for making a bunch of things unless there is a specific reason to use some other way. Here is a diagram showing a doubly linked list drawn as a circle, with the head element at the top connecting the one list endpoint to the other.

<a name="figure3"></a>

<div class="lesson_diagram">
<img alt="Doubly Linked List by T Shaw, modified from Tmigler hosted at Wikimedia, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" src="/assets/images/dlist.svg">
Figure 3 - Doubly Linked List (aka dequeue)
</div>

Consider removing an element from a doubly linked list. Before the removal, the 'next' pointer of the preceding element would point at the element, and the 'previous' pointer of the following element would also point at the element. Look at element 'p2' in the above doubly linked list diagram: the 'next' pointer of element 'p1' points at 'p2', and the 'previous' pointer of element 'p3' points at 'p2'. To remove element 'p2', just change the 'next' pointer of element 'p1' to point at 'p3', and the previous element of 'p3' to point at 'p1'. Then they both point around element 'p2', and 'p2' is no longer in the list. Removing 'p2' this way can be written as follows.

        p1->next = p3 ;
        p3->previous = p1 ;

However, in a real program you don't usually know exactly which element you want to remove until the program is running. Usually there is a pointer to the element to be removed, something like '`struct element *p`', and you want to remove that element, whether it is 'p1', 'p2', 'p3', or 'p4'. Suppose for example the 'p' pointer to be removed actually is 'p2' as in the preceding paragraph. How would you do the preceding example using just 'p'? Following from the diagram, 'p1' is '`p2->previous`', which would be '`p->previous`' in this case since 'p' actually is 'p2', and 'p3' is '`p2->next`' which would be '`p->next`', again since we are supposing 'p' actually is 'p2'. Therefore, '`p1->next`' is '`p2->previous->next`' which is '`p->previous->next`'. Also '`p3->previous`' is '`p2->next->previous'`, which is '`p->next->previous`'. So the two assignments above can be written using a pointer 'p' as follows, supposing 'p' actually is 'p2'.

        p->previous->next = p->next ;
        p->next->previous = p->previous ;

The point is, 'p' did not have to point at 'p2', it could just as easily point at 'p1', 'p3', or 'p4', and whatever it pointed at will be removed by these same two lines of code. If you look at the doubly linked list diagram and think real hard, you can see how these two lines remove any element by setting the next pointer of the previous element and the previous pointer of the next element to bypass that element. This logic works even if you are removing an element from a list with a million elements. Or the only element from a list with one non-head element. In fact, there may be no variables pointing at the other non-head elements besides 'p', so using just 'p' like this is typically the only option. Frequently it is not even known how many elements are in the list, they just get 'malloc'ed as needed, all you always have is a pointer to the head. Also, typically, removing the 'p' element would be part of some bigger operation that might be called many times, and those two lines of code could be used to remove many different elements from the list at different times.

The previous three paragraphs talked about removing an element from a doubly linked list. The point of those paragraphs was to work from the example of removing some particular element from the list, in this case 'p2', to the more useful case of removing any element pointed at by some pointer variable 'p' from the list. The operation of removing an element, such as 'p2', from a list is very simple. Element 'p2' is removed by setting the next pointer of 'p1' and the previous pointer of 'p3' to point around it. How would you figure out the code needed to change the way data is connected for a more complicated operation? A technique for developing this code is to draw two pictures of the links between the data, one before the change and one after, then use these pictures to figure out the code. Figure 4 and the six paragraphs after that show this technique for removing an element from a doubly linked list, a simple operation where this technique is not really needed. Homework 1 uses the technique explained in the following paragraphs for a more complicated operation.

<a name="figure4"></a>

<div class="lesson_diagram">
<img alt="Before and After List Element Removal by T Shaw, CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;" src="/assets/images/remove.svg">
Figure 4 - Before and After List Element Removal
</div>

This technique to figure out how to change the connections between data is based on two pictures, one showing the links before the change and one showing the links after. These pictures are typically hand drawn and just used while writing the code, they are not permanent documentation. They would just include the part of the data you are directly working with to perform the change. You can work from these pictures to figure out what needs to be changed, what the new values need to be, how to access what needs to be changed, and how to get the new values. Links between data are implemented by pointer members in each element 'struct' that point at other element 'struct'. Changes to these links are made by C statements with assignments to the pointer members that need to be changed.

This example is simple so you would not need to follow along with the pictures in the following paragraphs to understand it. However, you should follow along so you know what part of the pictures are used for what part of the code when you are doing more complicated operations, which is the whole point of this part of the lesson. The numbers in the pictures indicate <span style="color:red">what needs to be changed in red</span>, <span style="color:dodgerblue">how to access what needs to be changed in blue</span>, <span style="color:darkviolet">what the new values need to be in purple</span>, and <span style="color:green">how to get the new values in green</span>.

{% comment %} using darkviolet #9400D3 instead of purple #800080, purple shows up better on a white background, but darkviolet is a compromise which also should work in dark mode; also dodgerblue #1E90FF is a light mode / dark mode compromise for blue #0000FF {% endcomment %}

In this technique, the pictures only contain the elements directly involved with the change. In Figure 4 only the element being removed, and the elements before and after it with pointers to it, are involved, so these are the only ones drawn. The elements labeled "previous element" and "following element" and the element being removed are the same 'struct' before the removal and after, what changes are the links, the pointer members in the 'struct'. Before the removal there are four relevant links, as you can see by counting the number of 'next' and 'previous' pointer members in the before picture. After the removal the element being removed is no longer in the list so its pointer members do not matter, as you can see in the after picture. The code needs to change the relevant links of the remaining elements, the relevant element pointer members, in the after picture to the values they need to have. The relevant links are numbered in red in the after picture.

<span style="color:red">( 1 )</span> &nbsp; The previous element needs to have its 'next' pointer member changed.<br>
<span style="color:red">( 2 )</span> &nbsp; The following element needs to have its 'previous' pointer member changed.

Since there are two links that need to be changed there will be two C statements with assignments.

In this example, the way to access these 'struct' is through 'p', which might be a normal pointer variable or a pointer parameter to the function where the code is being written. The previous element needs to have its 'next' member changed, and the following element needs to have its 'previous' member changed. The way to access these members can be seen in the before picture, as indicated with the blue numbers. The 'next' member of the previous element is at <span style="color:dodgerblue">( 3 )</span> in the before picture. You can see that to get there, first you need to access the 'struct' for the previous element, so you start at 'p', which points to the element to be removed, and then use the 'previous' member of that struct to get a pointer to the previous element, that is '<span style="color:dodgerblue">`p->previous`</span>'. You then get to the 'next' member of the previous element using '<span style="color:dodgerblue">`p->previous->next`</span>'. Also, the following element needs to have its 'previous' member changed, which is at <span style="color:dodgerblue">( 4 )</span> in the before picture. From the before picture, you can see that to access the 'struct' for the following element, you start at 'p' and then use the 'next' member of that struct, that is '<span style="color:dodgerblue">`p->next`</span>'. You then get to the 'previous' member of the following element using '<span style="color:dodgerblue">`p->next->previous`</span>'. In summary, values need to be set for the following two links.

<span style="color:dodgerblue">( 3 ) &nbsp; `p->previous->next`</span> &nbsp;&nbsp;&nbsp; (the 'next' pointer member of the previous element)<br>
<span style="color:dodgerblue">( 4 ) &nbsp; `p->next->previous`</span> &nbsp;&nbsp;&nbsp; (the 'previous' pointer member of the following element)

Look at the after picture to figure out what the new values have to be. The previous element needs to have its 'next' member changed. From <span style="color:darkviolet">( 5 )</span> in the after picture, you can see that the new value is just <span style="color:darkviolet">a pointer to the following element</span>. Use the before picture, since that is how things are when you start, to figure out how to get that new value. From <span style="color:green">( 6 )</span> in the before picture, you can see that the following element is just the next element after the element being removed, where the element being removed is pointed at by 'p'. This means '<span style="color:green">`p->next`</span>' is a pointer to the following element, which is the value you need for the 'next' member of the previous element. Also, the following element needs to have its 'previous' member changed. From <span style="color:darkviolet">( 7 )</span> in the after picture, you can see that the new value is just <span style="color:darkviolet">a pointer to the previous element</span>. At <span style="color:green">( 8 )</span> in the before picture, you can see that the previous element is the previous element before the element being removed, so '<span style="color:green">`p->previous`</span>' gives you the new value you need for the 'previous' member of the following element. Here is a summary of the values for the assignments determined by this paragraph.

- The new value for <span style="color:red">(1) the 'next' pointer member of the previous element</span><br>
    Which is accessed through <span style="color:dodgerblue">(3) `p->previous->next`</span><br>
    Is: <span style="color:darkviolet">(5) a pointer to the following element</span><br>
    Which is: <span style="color:green">(6) `p->next`</span>

- The new value for <span style="color:red">(2) the 'previous' pointer member of the following element</span><br>
    Which is accessed through <span style="color:dodgerblue">(4) `p->next->previous`</span><br>
    Is: <span style="color:darkviolet">(7) a pointer to the previous element</span><br>
    Which is: <span style="color:green">(8) `p->previous`</span>

Putting all of this together. For the first C assignment, the previous element needs to have its 'next' member changed, which is accessed '<span style="color:dodgerblue">`p->previous->next`</span>'. The value for this, from the last paragraph, is '<span style="color:green">`p->next`</span>'. For the second C assignment, the following element needs to have its 'previous' member changed, which is accessed '<span style="color:dodgerblue">`p->next->previous`</span>'. The value for this, also from the last paragraph, is '<span style="color:green">`p->previous`</span>'. Here are these two assignments in C code.

        p->previous->next = p->next ;
        p->next->previous = p->previous ;

These are exactly the same two assignments as we figured out before for removing an element from a list. Clearly, this is a lot of work to figure out this simple case, which we were able to figure out more easily earlier. However, this same technique will be used in Homework 1, which is a somewhat more complicated case, and it can also be used for more complicated cases than that.

{{ UserCoffee }}{{ "2021-8-16 12:36:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

There are other diagrammatic aids for programming. Flowcharts, a way of drawing control flow such as 'while' loops, 'if' blocks, and so on, have been laughed at since before I was programming. They are a good way to communicate what is going on to non-programmers, but for programmers they are counter-productive and it is best to not even talk about them. Just looking at the code shows the same thing more easily if it is properly indented and you can read the language, this allows the part of your brain that 'does' code to become engaged and is the way to learn and do programming. However, some other diagrammatic aids are very useful. A block diagram shows the connections between major blocks of code. Which block calls which. If it is annotated with data shared between blocks, it is even better. A major block is a block of functions, it might be all the functions in a file, or even a few files, that perform some major operation. For example, suppose the dequeue customer-list is shared between major blocks "create new customer" and "update customer info" and "process customer request". A block diagram would show these blocks and the higher level blocks that call them, such as "perform customer action" which might call all three. The data can be added to the diagram, perhaps by drawing the traditional data icon which is a 3-D soup can (originally supposed to be a stack of mag tapes, if you know what a mag tape was) labeled with 'customer-list' and connected to those three blocks. This one picture can save some headache if someone changes customer-list to fix something in the new customer and update customer blocks without realizing it is also used in the customer request block and the changes need to be consistent there as well. This type of diagram is most useful when the software becomes complicated and there are a lot of blocks and a lot of separate data lists and so on. These sort of diagrams can be part of the permanent documentation for the software. They will not be discussed further in this class because we don't care about stuff like will anybody ever use our code or maintain it (Oh, did I type that out loud? Just kidding!)

{{ UserCoffee }}{{ "2021-8-16 12:42:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Now for a real example of doubly linked lists. It does not involve removing elements from a list. However, it does add elements to a list, the other basic list operation. This example does exactly the same thing as the previous example which used singly linked lists. However, this example is expanded in the next example to perform list operations, such as removing an element, that are not so easy with singly linked lists.

    #include <stdio.h>
    #include <stdlib.h>

    struct element {
        struct element *next ;
        struct element *previous ;
        float val ;
    } ;

    struct element *addelement(  /* add an element to a doubly linked list */
      struct element *list       /* pointer to head of existing list */
      /* if the list parameter is NULL, create an empty head for a new list */
    )
    {
        struct element *p ;

        if (! (p = malloc(sizeof *p))) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }

        if (list == NULL) { /* create a new empty list with empty head */
                /* the head of an empty list points at itself */
            p->previous = p->next = p ;
        } else {      /* add another element at the end of the list */
                /* element before the new last element is the old last */
            p->previous = list->previous ;
                /* head element goes after the new last element */
            p->next = list ;
                /* element after the old last element is new last element */
            list->previous->next = p ;
                /* new element goes at the end just before the head */
            list->previous = p ;
        }

        return p ;
    }

    int main()
    {
        struct element *list = addelement(NULL) ; /* create an empty list */
        float newval ;

        while (printf("next value: "), fflush(stdout), scanf("%f", &newval) == 1) {
            struct element *p = addelement(list) ; /* add new element to list */
            p->val = newval ;
            /* loop through the list and print the values in it */
            float total = 0.0 ;
            for (struct element *p = list->next; p != list; p = p->next) {
                printf("%.2f", p->val) ;
                if (p->next != list) printf(" + ") ;
                total += p->val ;
            }
            printf(" = %.2f\n", total) ;
        }

        /* free all elements in the linked list */
        struct element *plast = list->previous ;
        for (struct element *p = list->next; p != list; p = p->next) {
            free(p->previous) ; /* do not free p since still need p->next */
        }
        free(plast) ; /* loop does not free last element list->previous */
    }

Note how the loop over the list elements works.

        for (struct element *p = list->next; p != list; p = p->next) {

The pointer 'list' points at the empty head. To get to the first real element, use '`list->next`'. To get from each element 'p' to the next, use '`p->next`'. The loop keeps going as long as 'p' has not looped all the way around to the empty lead, '`p != list`'. This same construct will loop over the list in the other direction when the '`->previous`' pointers are used instead of the '`->next`' pointers.

        for (struct element *p = list->previous; p != list; p = p->previous) {

These two constructs work even for an empty list with no non-head elements, since in that case the 'next' and 'previous' pointers of the head point back at the head itself, so the first value of 'p' is the head and the loop is immediately finished.

Here is a little cash register example using a doubly linked list. Normally, you would type a quantity and a price. It will print all the old values entered if you type a quantity of 0. If the quantity is negative, it will allow you to retype the entry that number of items back from the current item. If you are retyping a previous entry and you enter a quantity of 0, the previous entry will be removed from the list.

These examples use a float number for prices. Floating point numbers round to some number of places depending on the type of computer. For many calculations the inputs are not exact or the formulas are not exact, so rounding during the calculation is ok. People care that money is calculated exactly right to the penny, so in real life programs involving money another format is used instead of float, such as fixed point with an increased number of bits. C has no built in support for these other formats, but there are libraries for performing calculations in all sorts of formats.

    #include <stdio.h>
    #include <stdlib.h>

    struct element {
        struct element *next ;
        struct element *previous ;
        int qty ;
        float price ;
    } ;

    struct element *addelement(  /* add an element to a doubly linked list */
      struct element *list       /* pointer to list head, NULL for new list */
    )
    {
        struct element *p ;

        if (! (p = malloc(sizeof *p))) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }

        if (! list) {
            p->previous = p->next = p ;
        } else {
            p->previous = list->previous ;
            p->next = list ;
            list->previous->next = p ;
            list->previous = p ;
        }

        return p ;
    }

    void printlist(struct element *list)
    {
        int cnt = 0 ;
        for (struct element *p = list->next; p != list; p = p->next) ++cnt ;
        for (struct element *p = list->next; p != list; p = p->next) {
            printf("[-%d] quantity=%d price=%.2f cost=%.2f\n",
                                cnt, p->qty, p->price, p->qty * p->price) ;
            --cnt ;
        }
        printf("type the negative number to edit that item\n") ;
    }

    struct element *finditem(struct element *list, int n)
    {
        struct element *p ;

        /* n is negative. change it to be the number of elements to go back. */
        n = -n ;

        for (p = list->previous; n > 1; --n, p = p->previous) {
                if (p == list) {
                    printf("going back before first item\n") ;
                    return NULL ;
                }
        }
        if (p == list) {
            printf("going back before first item\n") ;
            return NULL ;
        }

        return p ;
    }

    float changeitem(struct element *list, int n)
    {
        float change ;
        float newval ;
        struct element *p ;
        p = finditem(list, n) ;
        if (p == NULL) return 0.0 ;
        printf("old quantity and price: %d %.2f\n", p->qty, p->price) ;
        if (printf("new quantity: "), fflush(stdout), scanf("%d", &n) != 1) {
            printf("invalid quantity\n") ;
            return 0.0 ;
        }
        if (n < 0) {
            printf("negative quantity not allowed\n") ;
            return 0.0 ;
        }
        if (n == 0) {
            /* for quantity 0, remove the item from the list */
            change = - p->qty * p->price ;
            p->previous->next = p->next ;
            p->next->previous = p->previous ;
            free(p) ;
            printlist(list) ;
            return change ;
        }
        if (printf("new price: "), fflush(stdout), scanf("%f", &newval) != 1) {
            printf("invalid price\n") ;
            return 0.0 ;
        }
        change = n * newval - p->qty * p->price ;
        p->qty = n ;
        p->price = newval ;
        printlist(list) ;
        return change ;
    }


    int main()
    {
        struct element *list = addelement(NULL) ; /* create an empty list */
        int n ;
        float total = 0 ;

        while (printf("quantity: "),
               fflush(stdout), scanf("%d", &n) == 1) {
            if (n == 0) {
                printlist(list) ;
            } else if (n < 0) {
                total += changeitem(list, n) ;
            } else {
                float newval ;
                if (printf("price: "), fflush(stdout), scanf("%f", &newval) == 1) {
                    struct element *p = addelement(list) ;
                    p->qty = n ;
                    p->price = newval ;
                    printf("cost for %d items at price %.2f = %.2f\n",
                                                 n, newval, n * newval) ;
                    total += n * newval ;
                }
            }
            printf("total = %.2f\n", total) ;
            printf("(type 0 to list items) ") ;
        }

        /* free all elements in the linked list */
        struct element *plast = list->previous ;
        for (struct element *p = list->next; p != list; p = p->next) {
            free(p->previous) ; /* do not free p since still need p->next */
        }
        free(plast) ; /* loop does not free last element list->previous */
    }

This example shows going forward and backwards in a doubly linked list. It also shows an element being removed from a doubly linked list as discussed above.

<a name="homework1"></a>

**Homework 1**

In the above example, if an already entered item is selected to be modified by entering a negative quantity in the main loop, and then the new quantity value read by changeitem is negative, changeitem prints an error message that a negative quantity is not allowed. The homework is to change the above example program so that instead of printing an error in this case, it moves the item to be changed which was passed to changitem to a new location in the list, using the negative quantity that caused the error to select the new location, and then print the list. The new location to move the item will be after some other item which is that negative number from the end of the list. A way of implementing this is talked about in the next paragraph.

This homework can be done by modifying changeitem so that instead of printing an error message, it uses the negative quantity to select another item using finditem by passing the negative number to finditem, which will return the item that number back from the end of the list. Remember to always check when a function such as finditem returns a value indicating an error. Then, you need to write a new function that will move one item to be after another in a doubly linked list, however it might be best to write the code that goes in the new function after the rest of this, so just write the declaration for the new function first. Then, in changeitem call this new function to move the item being modified after the other item selected by the negative new quantity using finditem, and then call printlist to show the result. The function to move one item to be after another can be most easily written by first removing from the list the item being moved using the code to remove an item we have already discussed. After this, the function needs new code to insert the item which was just removed to go after the destination item passed to it. Figuring out this new code is the meat of this homework assignment; I put a hint for what I do to figure out this sort of thing in the next three paragraphs. Note that this will not work properly to move an item after itself because the item would no longer be in the list once it is removed, so check for both items being the same first.

The diagrammatic aid shown in [Figure 4](/5-a-fifth/#figure4 "Figure 4 - Before and After List Element Removal"), drawing before and after pictures of the data and links being modified, is particularly useful for this homework. These pictures would be drawn while writing the function that actually modifies the list to help figure out how to insert one item after another. Make sure to follow and understand the example of this for removing an element from a list as shown in Figure 4 and the paragraphs after it before doing this homework. Do not draw whole circular lists like Figure 3, that would just add to the confusion, the point of a doubly linked list is that each element is connected the same as all the others so you can think in terms of the elements you are working with and ignore their location in the whole list. The ability to think locally, just in terms of the local parts of the whole thing that may be needed for the problem at hand, is an important part of programming, and I hope this homework helps train you in this. First, draw a ‘before’ picture of before the insertion, containing the element after which you will do the insertion and a pointer to it labeled with the name of the function parameter for this pointer, maybe one element before this element, one element after it, and all four of the pointer members connecting these three elements. Draw the item you are moving off to the side in this picture and a pointer to it labeled with the name of the function parameter for the item being moved, since this is before the insertion there will not be anything meaningful in the pointer members of the removed element off to the side so do not draw any pointers for them. Then, draw an ‘after’ picture with all four elements and their pointer members connecting them, the three that were connected in the first picture and the inserted one in the appropriate place, with the two labeled pointer variables for the function parameters drawn to point to the same two elements at their locations in the after picture. The after picture would have six pointer members. I draw these sort of pictures in pencil with a big eraser at hand. Comparing the before picture to the after picture shows you what needs to be changed, and following from the named pointer variables to the changed pointer members of the changed elements shows you how to get from your input parameters to the data you need to change. This paragraph may not make much sense when you first read it, but if you draw the before picture and the after picture as described, then re-read this paragraph, it likely will. These drawings may not be really necessary for this homework because a doubly linked list is relatively simple, but they are good to try here since once you understand this diagrammatic technique, you can use it to build many types of data structures. The next two paragraphs have more hints for this homework.

<a name="picslink"></a>

Have you finished the two pictures described in the previous paragraph? If you have not finished them yet, you should try. Give it a shot. A person does not usually learn nearly as much by just reading and looking at pictures as they do by creatively making and drawing things. The point of this class is to learn how to do this stuff, not just see how it was done, but actually how to do it yourself. Once you have finished drawing the pictures, or you have really tried and carefully reviewed Figure 4 and are completely stuck, [here](/5-a-fifth/homework1/ "Before and After Pictures for Inserting an Item after Another") is a version of these pictures drawn by Student A and Student Z.

From the before and after pictures, you can figure out what you need to do; what code you need to write in the new function to insert the moving item after the destination once it has been removed from the list. Looking at the after picture, there are four pointer members in the element 'struct' that are pointing at different other element 'struct' than they were in the before picture, so that means four pointer members of the element 'struct' must be changed. You can use the before picture to find out how to get to the element 'struct' pointer members that need to be changed starting at the pointer variables which are passed as function parameters. The change is done with four C statements assigning values for the four pointer members that need to be changed. The order of these assignment statements matters. When something is assigned to, it overwrites the previous value, so it is a problem if you needed the previous value. Two of the pointer members that need to be changed, which you can see in your after picture, are in the element which is moving. Since this element has been removed from the list, it is not part of the list anymore and its pointer members before the insertion are irrelevant. So set these first. Then, there are still two more pointer members that need to be set. One of them is needed to get to the element 'struct' of the other, so do not overwrite this one until after you have used it to set the other one. Each of these four C assignments sets a pointer member which you get to by starting with a parameter to the function and then using the arrow operator to access a struct member, possibly multiple arrow operators in a row, until you get to the member being set. It sets the pointer member to a value which you also get to by starting with one of the parameters to the function, and then using the arrow operator if necessary to access a pointer member, to get a pointer with the value you need. You can determine which element 'struct' pointer members need to be set and what values they need to have by looking at your after picture, and how to get to them and to the values you need to set them to from the parameters to the function by looking at your before picture. All of this is explained in gory detail in the paragraphs after Figure 4. Once you write these four C assignments, the homework code is finished, so now you can compile and debug it. Hopefully it works the first time!

{{ UserCoffee }}{{ "2021-8-16 12:53:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Generally, you will not be writing code that removes elements from lists or inserts them before or after other elements in a list all the time. Usually, you will write a function once, perhaps once per project, that does each of these operations, and then just call the function when you need it. Or perhaps these operations are already available in some library you are using. However, now that you have completed this homework, you can write these functions if necessary. Also, you can use these same techniques for more complicated data structure operations as needed, which may not already be in a library. Even more important, now you know how these operations work, you can see that they just involve assigning pointers and following pointers to what they point to, instead of some mysterious operations that are performed by magic on the computer.

{{ UserCoffee }}{{ "2021-8-16 1:37:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

<a name="trees"></a>

The next example is a lot more complicated than previous examples. Our cash register always multiplies two numbers together and adds this into a total, where each pair of numbers is taken from an element in a list. That example shows lists, in which one element is processed after another in sequence, or sometimes in reverse for a doubly linked list. Lists are good when an operation is performed on each element one after another, in our case adding them all together. For more arbitrary operations, a different way of linking the structs is to have links from each struct to several other struct instead of just linking them together in a sequence. One metaphor for this is a tree. The root of the tree corresponds to the starting struct. The structs which that one links to are called branches of the tree. Finally, the struct which do not point at any other struct are called the leafs of the tree.

The 'struct' in a tree are called 'nodes'. The branch nodes pointed at by one node are called 'daughter' or 'child' nodes of that node, and the node which points at the child nodes is called their 'parent' node.

This example uses a tree to store a calculation. Here is a diagram of a tree for an addition operation, `5 + 3`. The root node is the one labeled '`+`' at the top for the addition operation itself, and the child nodes are the ones with the numbers being added at the bottom. In this case the child nodes of the root, the numbers, are also leaf nodes because they do not have child nodes themselves. When people draw trees, they usually draw them upside down with the root at the top, since the root is the starting node, and people read from the top down.

<a name="figure5"></a>

<div class="lesson_diagram">
<img alt="Addition Tree by T Shaw, modified from Tmigler hosted at Wikimedia, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" src="/assets/images/addtree.svg">
Figure 5 - Tree For Performing A Simple Addition Calculation
</div>

Here is code which calculates the value of the tree.

    #define ISNUMBER    1
    #define ISMULTIPLY  2
    #define ISADD       3

    struct node {
        int type ; /* ISNUMBER for leaf, ISMULTIPLY, or ISADD */
        float number ;
        struct node *child1 ;
        struct node *child2 ;
    } ;

    float evaluate(struct node *n)
    {
        if (n->type == ISNUMBER)
            return n->number ;

        if (n->type == ISMULTIPLY)
            return evaluate(n->child1) * evaluate(n->child2) ;

        if (n->type == ISADD)
            return evaluate(n->child1) + evaluate(n->child2) ;

        fprintf(stderr, "invalid node type: %d\n", n->type) ;
        exit(1) ;
    }

This code works by doing the calculation one node at a time per each call of the evaluate function. If a node is just a number, then the value of that node is just the number. If the node is an operation, then the value of the node is the values of the child nodes, with that operation performed on them. This means that while calculating the value of a node, it needs to calculate the value of the child nodes. To perform this, when the evaluate function is working on a non-leaf node, it calls itself on the child nodes. When a function calls itself, it is called 'recursion', and the function is called a 'recursive' function.

Try following this code by looking at the diagram of the addition calculation tree. Imagine the evaluate function in the code being called on the root node, the '`+`' node at the top of the diagram. First, it calls itself recursively on child1, which is the node with 5 just below the root node. This is a leaf, so the recursive call just returns with the value of the node, 5. Then the evaluate of the '`+`' node calls itself recursively on child2. This is another leaf, the 3, so this recursive call also returns with the value of the node, 3. Once the top level evaluate of the '`+`' has finished calling itself recursively on the child nodes, it performs its operation on them, adding 5 and 3, and returns the final value, which is 8.

A more complicated example would be to calculate `2 * (5 + 3) + 7`. In this case, the root node says to add the value of its child nodes. The first child says to multiply 2 times the result of `5 + 3`, where `5 + 3` is represented by another node saying to add 5 and 3. The second child node of the root is just the number 7. This description of the operation of the evaluate function is repeated in more detail three paragraphs below. Here is a diagram of the entire tree for this more complicated calculation example.

<a name="figure6"></a>

<div class="lesson_diagram">
<img alt="Calculation Tree by T Shaw, modified from Tmigler hosted at Wikimedia, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;" src="/assets/images/calctree.svg">
Figure 6 - Tree For Performing A More Complicated Calculation
</div>

This shows how a tree can be an arbitrarily complicated expression in terms of multiplication and addition, not just a linear sequence such as adding a list of numbers together.

The very same evaluate function for our simple '5 + 3' example will evaluate this entire tree, or any arbitrary multiplication and addition tree.

For a more detailed description of how this tree is evaluated, try following the evaluate function while looking at the diagram of the calculation tree. Start with the evaluate function being called on the root node, the '`+`' node at the top of the diagram. First, it calls itself recursively on child1, which is the '`*`' node just below the root node. This recursive call of evaluate starts by calling itself recursively on child1 of the '`*`' node, which is just the number 2. This is a leaf, so the recursive node just returns with the value 2. Then the evaluate of the '`*`' node calls itself recursively on child2 of the '`*`' node, which is the '`+`' node of 5 and 3. The recursive call on the '`+`' node of 5 and 3 calls itself recursively on the 5 node, which is a leaf so it just returns 5, and the 3 node, which is a leaf so it just return 3, and then adds them together because it is a '`+`' node, and gets the value 8 for 5 added to 3. Then it returns the 8 to evaluate called on the '`*`' node, which already has the value 2 for the child1 node, so the '`*`' node multiplies them together and gets 2 times 8, which is 16, and returns that to the evaluate called on '`+`' root node. The evaluate function of the root node then calls itself on the child2 node, which is 7, and then adds that to the 16 returned when evaluate was called on the child1 node, and then returns the final result, 23.

Trees are used for many things in programming. Recursion is a huge thing in software development. It is used for operating on many types of trees, and many other things as well.

For the full example, the cash register example will be turned into a calculator. The elements of the linked list will not be automatically added together to get a total. Instead, when you run it you can say what operation you want to perform on the previous elements, instead of just adding them. For this example, you can multiply them together or add them together. Say you want to multiply three numbers. You can multiply the first two numbers. Then, you can multiply the result by the third number. This function is not possible with the cash register example, since it only multiplies two numbers at a time, it can not do arbitrary multiplication and addition calculations.

Obviously, this example is only the core of a calculator. It is not very user-friendly. In lesson 6, we will be extending this with a better interface. Yet this core is the hard part, get this and you get the hard stuff in this whole class.

    #include <stdio.h>
    #include <stdlib.h>

    /********************************************
    *
    *          calculation tree operations
    *
    ********************************************/

    #define ISNUMBER    1
    #define ISMULTIPLY  2
    #define ISADD       3

    struct node {
        int type ; /* ISNUMBER for leaf, ISMULTIPLY, or ISADD */
        float number ;
        struct node *child1 ;
        struct node *child2 ;
    } ;

    float evaluate(struct node *n)
    {
        if (n->type == ISNUMBER)
            return n->number ;

        if (n->type == ISMULTIPLY)
            return evaluate(n->child1) * evaluate(n->child2) ;

        if (n->type == ISADD)
            return evaluate(n->child1) + evaluate(n->child2) ;

        fprintf(stderr, "invalid node type: %d\n", n->type) ;
        return 0.0 ;
    }

    struct node *makenode(int type)
    {
        struct node *n ;

        if (! (n = malloc(sizeof *n))) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }

        n->type = type ;
        return n ;
    }

    struct node *makenumber(float number)
    {
        struct node *n = makenode(ISNUMBER) ;
        n->number = number ;
        return n ;
    }
    
    struct node *makeoperation(int type, struct node *child1, struct node *child2)
    {
        struct node *n = makenode(type) ;
        n->child1 = child1 ;
        n->child2 = child2 ;
        return n ;
    }
    
    void treeprint(struct node *n)
    {
        if (! n) {
            printf("NULL tree pointer") ;
            return ;
        }
        if (n->type == ISNUMBER) {
            printf("%.2f", n->number) ;
            return ;
        }
        if (n->type == ISMULTIPLY) printf("MULTIPLY(") ;
        else if (n->type == ISADD) printf("ADD(") ;
        else printf("invalid_node_type(") ;
        treeprint(n->child1) ;
        printf(", ") ;
        treeprint(n->child2) ;
        printf(")") ;
    }
    
    /********************************************
    *
    *        calculator that uses the tree
    *
    ********************************************/

    struct element {
        struct element *next ;
        struct element *previous ;
        struct node *val ;
    } ;

    struct element *addelement(  /* add an element to a doubly linked list */
      struct element *list,      /* pointer to list head, NULL for new list */
      struct node *newval        /* calculation tree to put in list */
    )
    {
        struct element *p ;

        if (! (p = malloc(sizeof *p))) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }

        if (! list) {    /* make new empty list */
            p->previous = p->next = p ;
            p->val = NULL ;
        } else {         /* add another element at end of existing list */
            p->previous = list->previous ;
            p->next = list ;
            list->previous->next = p ;
            list->previous = p ;
            p->val = newval ;
        }

        return p ;
    }

    void printlist(struct element *list)
    {
        int cnt = 0 ;
        for (struct element *p = list->next; p != list; p = p->next) ++cnt ;
        for (struct element *p = list->next; p != list; p = p->next) {
            printf("[%d] ", cnt) ;
            treeprint(p->val) ;
            printf(" = %.2f\n", evaluate(p->val)) ;
            --cnt ;
        }
    }

    struct element *finditem(struct element *list, int n)
    {
        struct element *p ;
        
        if (n <= 0) {
            printf("need positive number to go back\n") ;
            return NULL ;
        }
        
        /* go backwards through the doubly linked list */
        for (p = list->previous; n > 1; --n, p = p->previous) {
            if (p == list) {
                /* got back to empty element at head of list */
                printf("going back before first item\n") ;
                return NULL ;
            }
        }
        
        if (p == list) {
            /* got back to empty element at head of list */
            printf("going back before first item\n") ;
            return NULL ;
        }
        
        return p ;
    }

    int main()
    {
        struct element *list = addelement(NULL, NULL) ; /* create empty list */
        int n ;
        float total = 0 ;

        while (
               printf(
          "type 0 for print, 1 for a number, 2 for multiply, 3 for add: "),
               fflush(stdout),
               scanf("%d", &n) == 1) {
            if (n == 0) {
                printlist(list) ;
            } else if (n == 1) {
                float newval ;
                if (printf("number: "), fflush(stdout), scanf("%f", &newval) == 1) {
                    addelement(list, makenumber(newval)) ;
                }
            } else if (n == 2 || n == 3) {
                int c1, c2 ;
                int type ;
                if (n == 2) type = ISMULTIPLY ;
                else type = ISADD ;
                printf("type two numbers saying how far to go back: ") ;
                fflush(stdout) ;
                if (scanf("%d", &c1) != 1) c1 = -1 ;
                while (c1 == 0) {
                    printlist(list) ;
                    if (scanf("%d", &c1) != 1) c1 = -1 ;
                }
                if (scanf("%d", &c2) != 1) c2 = -1 ;
                while (c2 == 0) {
                    printlist(list) ;
                    if (scanf("%d", &c2) != 1) c2 = -1 ;
                }
                if (c1 > 0 && c2 > 0) {
                    struct element *v1 = finditem(list, c1) ;
                    struct element *v2 = finditem(list, c2) ;
                    if (v1 && v2) {
                        struct node *n = makeoperation(type, v1->val, v2->val) ;
                        addelement(list, n) ;
                        printf("the value is: %.2f\n", evaluate(n)) ;
                    }
                }
            } else {
                printf("must type something between 0 and 3") ;
            }
        }
    }

<br>

<a name="homework2"></a>

**Homework 2**

Run the tree calculator example, and use it to calculate `5 + 3` as shown in the first tree diagram. After that, calculate `2 * (5 + 3) + 7` as shown in the more complicated diagram.

There is another homework at the end of this lesson to improve the calculator. I put it at the end of the lesson because it is time to get on with arrays.

{{ UserCoffee }}{{ "2021-8-16 1:57:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

<a name="arrays"></a>

**Arrays**

The other way to store a bunch of something is to use an array. An array is a bunch of the same type of variables, one after another. Each individual variable in the array is called an array element. An array can be created the same as a normal variable, with the number of elements in '`[`' '`]`' added after the name.

        int x[10] ;

In C, arrays are intimately associated with pointers. Huh??? you say.

Each element of an array does not have a name associated with it. Instead, you access it though a pointer to that element. The name of the overall array gives a pointer to the first element. So if the overall array is declared '`int x[10] ;`', then 'x' gives a pointer to the first element of the array.

        int x[10] ;
        *x = 7 ;
        printf("first element is %d\n", *x) ;

Remember from the previous lesson, using '`*`' allows accessing a value through a pointer. In this example, the first element is set to 7 and then printed.

Here is another example of the same thing, where the pointer nature is emphasized.

        int x[10] ;
        int *my_int_pointer = NULL ;
        my_int_pointer = x ;
        *my_int_pointer = 7 ;
        printf("value at my_int_pointer is %d\n", *my_int_pointer) ;
        printf("first element is %d\n", *x) ;

Obviously, for an array you need to access elements besides just the first one. To access an element after the first, add a value to the pointer to the first element. A pointer to any element in an array can be moved to point to any other element in the array by adding the number of elements to move. Here is an example which accesses the fifth element, which is four past the first.

        int x[10] ;
        int *my_int_pointer = x ;
        my_int_pointer = my_int_pointer + 4 ;
        *my_int_pointer = 7 ;
        printf("value at my_int_pointer is %d\n", *my_int_pointer) ;

Another way to access an array element is to use '`[`' '`]`' after a pointer that points into an array. For any pointer 'p', '`p[i]`' is defined in C as exactly the same as '`*(p+i)`'. In the array '`int x[10]`', 'x' gives a pointer to the first element of the array. So '`*x`' is the value at that pointer which is the first element, and could also be written '`*(x+0)`' since adding '`0`' to something doesn't change it, which would be '`x[0]`' by the definition in C for '`p[i]`'. Also, '`x[4]`' is exactly the same as '`*(x + 4)`', which accesses the array element four past the first element, the fifth element. The ten elements in the array are `*x`, `*(x+1)`, `*(x+2)`, ..., `*(x+9)`; which could also be written `x[0]`, `x[1]`, `x[2]`, ..., `x[9]`.

Here is an example which writes a value using the pointer to the fifth element like the previous example, and then accesses that value using '`x[4]`'.

        int x[10] ;
        int *my_int_pointer = x ;
        my_int_pointer = my_int_pointer + 4 ;
        *my_int_pointer = 7 ;
        printf("value of the fifth element is %d\n", x[4]) ;

The following diagram illustrates this. On the right of each array element are multiple ways of referring to that same element, e.g. '`*p`' is the same as '`*(x + 4)`'.

<a name="figure7"></a>

<div class="lesson_diagram">
<img alt="Pointer Into Array by T Shaw, CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;" src="/assets/images/array.svg">
Figure 7 - Array With A Pointer To The 5th Element
</div>

If you have a pointer to any element in the array, then just add '1' to the pointer, and you now have a pointer to the next element. If you subtract '1' from the pointer, you go back to the previous element.

In C, outside of allocation and declarations, arrays are not relevant. There are no built-in "array things" in memory like there are in other languages. What is relevant is the pointers into them. As I said above, an array is just a sequence of multiple of the same type of variable in memory. As I also said above, in C, adding or subtracting an integer to a pointer that is pointing at one variable in a sequence in memory points at another variable in that sequence as far away as the integer you add. That's it. Other languages typically build their "array things" internally using these primitive C concepts, as I have said in previous lessons many languages use C semantics at their core, but the "array thing" is superstructure on top of what is available in C. The C language is closer to the metal, so it gives the programmer more control and it may run faster if the lower level concepts can be combined in some particular way for a particular program that does not need all of the superstructure. To illustrate how C is all about pointers, in the next example `my_int_pointer[4]` and `my_int_pointer[3]` are used, yet in this case both places are really referring to the same variable, whose value was set to `7` using `*(my_int_pointer + 4) = 7`.

        int x[10] ;
        int *my_int_pointer = x ;
        *(my_int_pointer + 4) = 7 ;
        printf("fifth element: %d\n", my_int_pointer[4]) ;
        my_int_pointer = my_int_pointer + 1 ;
        printf("that same value: %d\n", my_int_pointer[3]) ;

Here is an example that accesses multiple elements of an array. It adds five float numbers together. The first element in the array is pointed to by valarray. The last element is pointed to by valarray+4 since that is the fifth element. Increasing the pointer by one goes from each element to the next. Feel free to try out this example.

    #include <stdio.h>

    int main()
    {
        float valarray[5] ;
        float total ;
        valarray[0] = 0.5 ;
        valarray[1] = 23.0 ;
        valarray[2] = 12.13 ;
        valarray[3] = 36.47 ;
        valarray[4] = 7 ;
        for (float *p = valarray; p <= valarray + 4; )
            total += *p++ ;
        printf("total is %.2f\n", total) ;
    
        return 0 ;
    }

The C syntax in the following three paragraphs should have been in lesson 2, they don't really fit here. Maybe someday I'll move them there, along with some other stuff. With examples and exercises.

The '`p++`' expression uses the '`++`' operator, called the *post-increment operator*. The '`++`' operator is a very convenient way to add one to something and is used a lot in C. This operator uses the value of the variable first, then adds one to it. So '`*p++`' gets the value pointed at by the pointer 'p', then adds one to 'p'. As you can see from the previous example, this *post-increment operator* is very useful for incrementing a pointer to prepare for the next time around a loop. There is also a *pre-increment operator*, which adds one to a variable before using the value, which is not used so much. Also, there are *post-decrement* and *pre-decrement* operators, which subtract one instead of adding one.

The expression '`total += ...`' adds a value to the total variable and stores the result back in total. This is called an assignment operator. Besides '`+=`', assignment operators can use '`-=`', '`*=`', '`/=`', and so on.

The pointer 'p' is declared in the above example inside the 'for' statement. Variables declared inside 'for' statements may be used up to the end of the statement, in this case the end of the body of the 'for' loop. It is a good idea to declare variables as locally to the code that needs them as possible, so that someone reading the code can more easily see where they are being used.

Here is an interactive version of the above example. It adds five numbers, but it interactively lets you set the numbers to some value.

    #include <stdio.h>

    int main()
    {
        float valarray[5] ;
        for (float *p = valarray; p <= valarray + 4; ) *p++ = 0.0 ;
        int i = 0 ;
        float newval = 0.0 ;
        do {
            float total ;
            if (i < 0 || i > 4) {
                printf("element is out of bounds\n") ;
            } else {
                valarray[i] = newval ;
            }
            for (i = 0; i <= 4; ++i)
                printf("valarray[%d] = %.2f\n", i, valarray[i]) ;
            total = 0.0 ;
            for (float *p = valarray; p <= valarray + 4; )
                total += *p++ ;
            printf("total is %.2f\n", total) ;
        } while (printf("type which element to change and new value: "),
                      fflush(stdout),
                      scanf("%d %f", &i, &newval) > 0) ;
    
        return 0 ;
    }

This loop uses a 'do - while' statement to perform the loop body, including printing out the current list of values, before it does the condition requesting a new value.

Because a 'do - while' has the condition at the loop bottom, and so 'scanf' is not called until the loop bottom just before the second time though the loop, the variables that would be set by 'scanf' need to be set before the loop is entered to have a value the first time through the loop. They are initially set to some value which will have no effect, here 'newval' is set to 0.0 which has no effect because all the values are already 0.0. This is ugly, it is not immediately obvious why the code has to set 'i' and 'newval' like this until you read all the way down to the 'scanf'. Most teachers would grade down for doing this. But it works, so whatevers. An example in lesson 6 shows a cleaner way of doing this.

Hot poker alert. In this example, the element to set, '`i`', is checked to make sure it is not out of bounds. The array is declared of size 5, so any access not within elements 0 through 4 is out of bounds. You must have code to check this, the C language will not check it for you. If you access an array element out of bounds, the program may immediately crash, or crash later, or just give the wrong answer. In any case, you will end up having a bad day so always make sure array accesses are within bounds. Repeating this because it is probably the hot poker that people get burned on the most: C has no bounds checking, so always be careful when accessing an array.

From the 'four-square' lesson, we learned that NULL is just 0. How is this set up? How do people normally do constants in C?

    #define NULL 0

The '#' character should be the first character on a line of C source to be completely portable. The '#define' line is not really part of C semantics, this line itself does not tell the compiler to make some code which performs some operation or directly effect some operation to be performed. It just sets up a simple text substitution, whenever you type NULL, the compiler sees 0. You can '#define' any word to be any text at all. We will talk about this more in lesson 6.

Why do I bring up '#define' now? Because it is a bad idea to put an array size all over your code. What if you want to change the size from 5 to 8, if 5 turns out not to be big enough? If you use '#define', then you can change the array size at the place where it is defined, and not have to worry whether every place the size is used has been changed properly. Here is the previous example using '#define'.

    #include <stdio.h>

    #define VALSIZE 8

    int main()
    {
        int i = 0 ;
        float newval = 0.0 ;
        float valarray[VALSIZE] ;
        for (float *p = valarray; p <= valarray + VALSIZE - 1; )
            *p++ = 0.0 ;
        do {
            float total ;
            if (i < 0 || i > VALSIZE - 1) {
                printf("element is out of bounds\n") ;
            } else {
                valarray[i] = newval ;
            }
            for (i = 0; i <= VALSIZE - 1; ++i)
                printf("valarray[%d] = %.2f\n", i, valarray[i]) ;
            total = 0.0 ;
            for (float *p = valarray; p <= valarray + VALSIZE - 1; )
                total += *p++ ;
            printf("total is %.2f\n", total) ;
        } while (printf("type which element to change and new value: "),
                      fflush(stdout),
                      scanf("%d %f", &i, &newval) > 0) ;
    
        return 0 ;
    }

This example is the same as the previous, except the array is bigger to show how easy that is using '`#define`'.

The 'malloc' function used to allocate each 'struct' in a bunch of linked struct is also used to allocate an array. It allocates an entire array at once. Just multiply the size of each element by the number of elements to get the size argument to use for 'malloc' to allocate an array with that many elements.

The following example allocates an array of 10 integers.

        my_int_pointer = malloc(10 * sizeof(int)) ;

In this example, `my_int_pointer` is set to point to the first element of the array, just like in the case where it is set to an array declared in the function.

Here is the previous example, using a separate function to allocate the array using 'malloc'. It allows you to set the array size to whatever you want when you are running the program.

    #include <stdio.h>
    #include <stdlib.h>
    
    float *makearray(int num_elements)
    {
        float *p = malloc(num_elements * sizeof *p) ;
        if (p == NULL) {
            fprintf(stderr, "Out of memory.\n") ;
            exit(3) ;
        }
        for (float *p1 = p; p1 < p + num_elements; )
            *p1++ = 0.0 ;
        return p ;
    }
    
    int main()
    {
        float *valarray ;
        int i = 0 ;
        int n ;
        float newval = 0.0 ;

        if (printf("array size: "), fflush(stdout), scanf("%d", &n) != 1)
            return 1 ;
        
        if (n <= 0) {
            printf("array size must be positive\n") ;
            return 1 ;
        }
        
        valarray = makearray(n) ;

        do {
            float total ;
            if (i < 0 || i > n - 1) {
                printf("element is out of bounds\n") ;
            } else {
                valarray[i] = newval ;
            }
            for (i = 0; i <= n - 1; ++i)
                printf("valarray[%d] = %.2f\n", i, valarray[i]) ;
            total = 0.0 ;
            for (float *p = valarray; p <= valarray + n - 1; )
                total += *p++ ;
            printf("total is %.2f\n", total) ;
        } while (printf("type which element to change and new value: "),
                      fflush(stdout),
                      scanf("%d %f", &i, &newval) > 0) ;
        
        free(valarray) ;
        
        return 0 ;
    }

There is a function like 'malloc' called 'realloc' specifically for arrays. It is used to make arrays longer that have already been dynamically allocated using 'malloc' or 'calloc', e.g. you can start out with the array being 10 elements long, then if you fill that up make it 20 long, then if you fill that up make it 40, and so on. We will not be using 'realloc' in these lessons, it is not used as much as the others, but comes in very handy if you are not sure how long to make an array when you first allocate it. The return value from 'realloc' should be freed when you are finished with the array. Note, 'realloc' is a very hot poker item if you have pointers at or into any of the space that has been dynamically allocated, because sometimes it has to move the allocated space to make it bigger, and a pointer at or into it then points at the wrong variable. Crash. Oops. Hopefully it will crash immediately, and not fail later in front of an audience of millions of your peers. Be careful if you use 'realloc'!

In summary, an array is a bunch of data in a single big block with all the data in the bunch one after another. This has it's advantages, like the ability to get from one element to another element when the other element is a known number of elements away, also it is an efficiently compact way of storing bunches of data especially when the individual elements are small such as single numbers. However, there are many disadvantages to arrays. Adding a new element in the middle, for example, is slow because the elements above the new element all need to be moved up. Typically in C, linked struct are used instead of arrays unless there is some particular reason to use an array, such as the elements need to be in a table where they are accessed by a numeric index. In C, things are frequently accessed by pointers, accessing an element in a table by an index does not occur as much.

<a name="homework3"></a>

**Homework 3**

In the first section about linked lists, we created a little cash register where each transaction had a quantity and a price. The following struct can be used to store this info.

    struct element {
        int qty ;
        float price ;
    } ;

Redo the previous array example to use an array of this struct instead of an array of float. You will need to change it to allocate these struct instead of floats. You will need to initialize the array of struct where it currently initializes the array of float. You will need to 'scanf' three values where it now reads two: which element, a new quantity, and a new price. You will need to multiply the quantity times the price.

In C, if there are multiple pieces of info about something, they are grouped into a struct. In olden days before structures like this, people would use parallel arrays, e.g. in this homework there would have been a 'qty' array of type 'int' and a 'price' array of type 'float', instead of putting them together into a single array of the 'element' type which contains both. That tends to create code which is much harder to maintain, since then if you add or modify the info about something, you have to keep track of changes to the use of multiple arrays, and it also tends to run slower because it is indexing for each array instead of only once, so parallel arrays are rarely used anymore.

<a name="homework4"></a>

**Homework 4**

Redo the previous homework. Instead of allocating an array of '`struct element`' using 'malloc', allocate an array of *pointers* to '`struct element`' using 'calloc'. Hint: a pointer to a struct is declared using a '`*`', and an array returned from malloc or calloc is a pointer which is also declared using a '`*`', so an array of pointers to some struct uses a pointer to a pointer. Since each of "being a pointer" and "using an array of them" needs a '`*`' in the declaration, there are two '`*`' in declaring the variable for the calloc return value, such as '`struct element **p`'. Some more hints: using calloc means the array will be all NULL pointers and there should be no code to initialize the values inside the makearray function. Also, when a new value is set into valarray each time around the main loop, check if the pointer at that location in valarray is NULL, and if it is NULL use malloc to allocate a new struct before using it. Finally, when looping to calculate the total, skip NULL entries in valarray.

{{ UserCoffee }}{{ "2021-8-16 2:57:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% comment %} I just made up this date/time because I did not mark when I got to this point and there needs to be a context change {% endcomment %}

We have now covered the core mechanisms for making a bunch of stuff. There is a lot of theory regarding this. For example, there are books on just the many possible ways of using recursive functions to process a tree of nodes. All of this is included in the theory of data structures, a core part of computer science.

Bunches of stuff are made out of linked 'struct' or arrays. This lesson has examples of making linked 'struct' using a singly linked list, using a doubly linked list, and using a tree. This lesson has examples of making arrays of numbers and homework with arrays of 'struct'. There are many ways of combining all of this. A common way of making a bunch of stuff is called a 'hash table'. It looks like a new way, but it is really usually made by combining arrays and linked lists, so understanding them is necessary first. There will be an example of a hash table in lesson 6.

<a name="homework5"></a>

**Homework 5**

In the linked list half of this lesson, there was an example which calculated the value of a calculation tree entered by the person running the example. Modify the tree calculator example code to perform division, subtraction, and negation as well as the multiplication and addition it already performed. Run it to calculate the value of `-4 + (7 - 3) / 2 + 2 * 5`.

{{ UserCoffee }}{{ "2021-9-4 10:44:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

<a name="memorytour"></a>

We have been using 'malloc' a lot. We have used it to allocate individual struct to make lists and trees, and to allocate multiple variables one after another to form arrays. Here is an overview of what is in the memory space, including the 'malloc' variables. This is not C specific, what is in the memory space overall is the same for different programming languages.

Your computer is organized into many processes, one for each program being run, in addition to the kernel which provides low level functions to those processes, as we discussed in lesson 3. Let's take a tour of the memory space of one process. This memory space is divided into several parts. There is the space for the program instructions, the binary codes that tell the computer exactly what to do when running a program, which is shared by all the processes running the same program. Also, I mentioned in lesson Number Two that you can define variables outside of any function. There is a separate part of the space for these types of global variables. They are not used very much anymore, because there are huge code maintenance issues if someone changes the way such a variable is used in one function, and it becomes inconsistent with another function using that variable. Better to just make a struct with the values needed by more than one function, and pass a pointer to the struct between the functions, so you can directly see who has access to what, and you can change it around with a new struct for particular functions if necessary. In addition, there is another part of the space for variables declared in functions. This space is called the stack, because when one function calls another which calls another and so on, the space for the variables for each new function called is stacked on top of the space for the variables in the previously called functions. The stack space also records which function called each function, so when a function returns it knows where to return. When a function returns, the space for that function's variables is automatically unstacked, that is the space is removed from the stack and those variables no longer have any memory space assigned for them so they must not be used anymore, and the space is available for other function calls. Finally, there is a separate part of the space for variables allocated with 'malloc'. This space is outside the stack, so when a function returns, the 'malloc' variables allocated by that function are still there. This is important for creating variables in one function with 'malloc' and using them in others, as you have seen in the examples and homework. The 'malloc' space is sometimes called the heap, because 'malloc' variables are just thrown in a heap, not in a stack which can be easily unstacked when functions return like the stack variables. There are other parts to the memory space of a process used in special cases, for example for library functions and for shared variables between processes and so on, but the above four spaces are the main ones: instruction, global variables, stack, heap.
