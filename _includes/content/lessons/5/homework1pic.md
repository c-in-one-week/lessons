<!---
_includes/content/lessons/5/homework1pic.md  homework 1 picture description for lesson 5 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{{ UserCoffee }}{{ "2021-8-16 12:42:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% comment %} this was written in a text editor which do not mark exactly the date/times when the sections were written; I put these date times in as I proof-read the various sections {% endcomment %}

Here is a diagram with the before and after pictures for Lesson 5, Homework 1. This diagram was drawn by Student A and Student Z.

The homework is to move one element after another. A hint is to perform this by first removing the element to be moved from its current position, then inserting it after the other element. These pictures are used for the second part, for determining how to insert one element into a doubly linked list after another one.

These sort of pictures are a general tool for modifying connections between 'struct'. They would typically be drawn while writing the function that actually modifies the connections, in this case the pointers linking a doubly linked list, to help figure out what to do. They are generally not intended as permanent documentation. I would typically draw pictures like these in pencil, with a big eraser at hand.

The ‘before’ picture is before the insertion, containing the element after which you will do the insertion and a pointer to it labeled with the name of the function parameter for this pointer, maybe one element before this element, one element after it, and all four of the pointer members connecting these three elements, possibly more pointer members if that helps clarify the list structure. Draw the item you are moving off to the side in this picture and a pointer to it labeled with the name of the function parameter for the item being moved, since this is before the insertion there will not be anything meaningful in the pointer members of the removed element off to the side so do not draw any pointers for them.

The ‘after’ picture is after the insertion, with all four elements and their pointer members connecting them, the three that were connected in the first picture and the inserted one in the appropriate place, with the two labeled pointer variables for the function parameters drawn to point to the same two elements at their locations in the after picture. The after picture would have at least the six pointer members between these four elements.

Comparing the before picture to the after picture shows you what 'struct' pointer members need to be changed, and following from the named function parameter pointers to the changed pointer members of the changed elements shows you how to get from your input parameters to what you need to change.

<div class="lesson_diagram">
<img alt="Lesson 5 Homework 1 - List Insertion by Student A and Student Z, CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;" src="/assets/images/insertafter.svg">
Before and After Drawings for Insertion into a Doubly Linked List
</div>

The next paragraph in Lesson 5 after the one with the link to this page has details on how to use these pictures to write the code for the homework assignment.
