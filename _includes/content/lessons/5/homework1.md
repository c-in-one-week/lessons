<!---
_includes/content/lessons/5/homework1.md  homework 1 picture cover for lesson 5 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

This page is to give you one last chance to draw the pictures for homework 1 yourself before showing them to you. This class is not about learning stuff. It's not about gaining abstract knowledge. It's about turning you into a C programmer. Reading words, reviewing code examples, looking at pictures: those are all good things to learn about programming. But to be a programmer, you need to actually do it yourself. Put together the connections between the data, drawn on a piece of paper, yourself. Make the pictures yourself. I didn't go to all the effort of making this class so that you can tell people "I learned about C". I did this so you can be a C programmer. So draw the pictures before you look! Give it a shot.

If you have tried and tried but are not able to draw the pictures, then listen to Master Yoda. "There is no try, there is only do." So click that back button, and draw them.

If you have actually drawn the pictures, yet they don't convey the information needed for the homework and you just do not see what else to add, or if you have drawn them but they just don't look right for some reason and you want to see how someone else would draw them, then I have an idea. Go up in the browser address bar, add the three letters 'pic' after the word homework1 and before the slash, and see what happens.
