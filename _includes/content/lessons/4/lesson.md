<!---
_includes/content/lessons/4/lesson.md   content for lesson 4 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{{ UserCoffee }}{{ "2021-6-5 12:32:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Four Square

{% comment %} thenwait 12 {% endcomment %}{{ UserCoffee }}{{ "2021-6-5 12:44:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

This is the core lesson. It is lesson number four of seven, the middle pillar, if you will. It only talks about two things, in the following two sections.<br>
Understand these two things, and you will have a solid understanding of C. Not all of C, just two core concepts, two fundamental concepts. And I don't mean understand them by over thinking them, don't get distracted by bits and bytes and binary representations and voltage levels; just understand them in the context of C semantics, as is presented in this lesson. These over thinking types of things are different on every type of machine anyway. Once you get what C is, the semantics of C, then would be a better time to over think things if that is what you want.

There may not be as much text in this lesson as previous lessons, but it is all meat. I expect it would not be easy to complete this lesson in one day. Perhaps, you will take this statement as a challenge, and prove me wrong!<br>
<br>
Here we go.<br>
<br>

{% comment %} thenwait 13 {% endcomment %}{{ UserCoffee }}{{ "2021-6-5 12:58:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Data Structure**

So far we have been dealing with numbers. Our variables are numbers. Numbers are cool. You can square them, which I already said I almost always code as '`x * x`', out of habit as much as anything. You can call functions with them as arguments, you can return them from functions. You could create huge programs of many many functions, all just using numbers. It gets tedious to keep it organized.

Eventually you want to keep track of things, organize things, more easily. The 'if', 'while', 'for' statements, and function calls, allow you to structure your code. Organize it so it is easy and simple to see what is going on: some block of code is only performed if a condition is true. Some block of code is repeated in a loop. Some block of code in a function is called from another block of code where the function is called.

What about data? How do you structure the data to keep it organized?

    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;

This defines a data structure in C. It does not declare any variables of that data structure, it just says that when a variable is defined using that structure, what the contents of that variable will be. Like int or float, 'struct structurename', where 'structurename' is the name after 'struct', is a new type which can be used directly to declare variables. For example, to declare a variable of type 'struct simple_date', use:

        struct simple_date x ;

The parts inside the struct are called members. To access them, use the variable name followed by '`.`' followed by the member name. For example, to access the '`year`' member of '`x`', use '`x.year`'. This is equivalent to there being another variable, inside of the '`struct simple_date x`' variable. For example, '`x.year = 5 ;`' is the same as setting any '`int`' variable to 5. Except that this '`int`' is one of the three members inside '`x`'.

Here's a whole program using 'struct simple_date'. Feel free to copy it into your compiler window and check it out.

    #include <stdio.h>
    
    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    int main()
    {
    
        struct simple_date bday, tday ;
    
        tday.year = 2021 ;
        tday.month = 6 ;
        tday.day = 4 ;
    
        bday.year = 1987 ;
        bday.month = 3 ;
        bday.day = 2 ;
    
        printf("You are %d years, %d months, and %d days old\n",
            tday.year - bday.year, tday.month - bday.month, tday.day - bday.day) ;
    
        return 0 ;
    }

A 'struct' type is usually defined outside of any function. Also, usually, a 'struct' declaration comes first to define the 'struct' type, then that 'struct' type is later used to define variables of that 'struct' type, as in the above examples. However, this is not necessary, you can define a 'struct' wherever you can declare a variable, and you can also put the 'struct' definition together with defining variables of that 'struct'.

    {
        struct some_struct {
            int member1 ;
            int member2 ;
        } a_variable_of_some_struct ;
        ...

You can even do this without a 'struct' name at all.

    {
        struct {
            int member1 ;
            int member2 ;
        } a_variable_of_unnamed_struct ;
        ...

However, this is rarely done. Most programs are organized with named 'struct' types defined at the top outside of any functions, and the variables of that type defined later. This helps organize the source files, as will be illustrated in a later lesson.

Do not confuse 'struct' type definitions with variable definitions. `struct x { int a, b, c ; } ;` does not actually define any variables, just a new 'struct' type with three members. And `struct x { int a, b, c ; } y ;` does define the variable 'y' using the 'struct x' type.

{% comment %} thenwait 23 {% endcomment %}{{ UserCoffee }}{{ "2021-6-5 1:26:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Pointers**

Leaving Data Structure aside for a while, let's talk about pointers.

Our friendly little construct '`int x ; scanf("%d", &x) ;`' uses a pointer to the 'x' variable. Forget about 'struct' for a moment, we are now back to talking about 'int'. Why does 'scanf' need a pointer to 'x'? Because 'scanf' doesn't care about the value 'x' had before scanf was called, instead it needs access to the 'x' variable so it can store a new value into the variable.

Can you yourself write a function that takes a pointer as a parameter, just like scanf? Definitely!

There is a new syntax for this. When declaring a pointer variable which would point to something of some type, put a '`*`' between the type name and the variable name. For example '`int * my_int_pointer ;`'. In fact, people usually omit the space after the '`*`', so they write '`int *my_int_pointer ;`'. How do you get to the variable a pointer variable points at? It's easy! In any expression, if you want to get to the variable a pointer variable points at, just put '`*`' in front of the variable, like '`*my_int_pointer`'. This is why people omit the space after the '`*`' in the declaration, it looks consistent that way.

{% comment %} thenwait 5 {{ UserCoffee }}{{ "2021-6-5 1:32:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

For example:

        int *my_int_pointer = ... initializer goes here ... ;
        ... a bunch of code goes here ...
        *my_int_pointer = 5 ;

will set whatever variable my_int_pointer was pointing at to the value 5. You can also use the value of the variable being pointed at, of course, with the same '`*`' syntax:

        printf("the value of the variable being pointed at is: %d\n", *my_int_pointer) ;

Note that I wrote an initializer (the '`... initializer goes here ...`' part) when I declared '`my_int_pointer`'. This initializer is not required by the C language, but it is a very good idea unless you like getting burned by hot pokers. If you write to the variable pointed at by some uninitialized pointer, so the pointer does not have a value yet, it will write the value to some unpredictable variable in the program and, oh it could corrupt things and you might not even know until the wrong value comes out a long time later. Maybe you will not know until your customers complain about the wrong value, and your company fires you. Oh well. A good initializer choice is frequently simply zero:

        int *my_int_pointer = 0 ;

The 0 pointer is also called NULL. It is guaranteed not to point at any variable. In most operating systems, writing to the 0 pointer causes an immediate program crash, so you know you made a mistake, and the debugger can tell you what line of source code. In many operating systems, reading from the 0 pointer causes an immediate program crash as well, also helpful for finding your mistakes.
So typically a pointer declaration looks just like:

        int *my_int_pointer = NULL ;

Then, before you use a pointer, you can test against NULL to see if the pointer is good:

        if (my_int_pointer == NULL)
            ... print error condition ...

This is one case where I almost never use the '==' sign. Since an 'if' condition tests against 0 anyway, and since NULL is zero, you can just write:

        if (! my_int_pointer) {
            fprintf(stderr, "software error: pointer still set to NULL.\n") ;
            exit(4) ;
        }

Putting this all together, here is sample code you can copy to the compile window and run. In this code, the "`stdlib.h`" include file is needed to declare various system library functions such as 'exit', like "`stdio.h`" declares various input / output library functions. The 'exit' function ends the program, the same as returning from 'main', the argument to 'exit' is the exit status which is the same as the value returned by 'main'.

    #include <stdio.h>
    #include <stdlib.h>
    
    int main()
    {
        int *my_int_pointer = NULL ;
        int x = 5 ;
    
        printf("originally x is: %d\n", x) ;
    
        /* remember from scanf: &x creates a pointer to the x variable */
        my_int_pointer = &x ;
    
        if (! my_int_pointer) {
            /* This test is not really necessary in this case- the code just
               set my_int_pointer to &x in the previous line, it is going to be set.
               But if there is any code in between, possibly with 'if' statements
               and so on, it is a good idea to check that my_int_pointer is set */
            fprintf(stderr, "software error: pointer still set to NULL.\n") ;
            exit(4) ;
        }
    
        *my_int_pointer = 6 ;
    
        printf("x becomes: %d\n", x) ;
    
        return 0 ;
    }

In the 'scanf' case in the earlier lessons, a pointer was passed to a function. Here is a complete sample program showing how to define your own function for doing that. Free free to copy it to your compiler and run it.

    #include <stdio.h>
    #include <stdlib.h>
    
    void my_pointer_function(int *my_int_pointer)
    {
        if (! my_int_pointer) {
            fprintf(stderr, "software error: my_pointer_function received NULL.\n") ;
            exit(4) ;
        }
        *my_int_pointer = 6 ;
    }
    
    int main()
    {
        int x ;
    
        my_pointer_function(&x) ;
    
        printf("value set for x is: %d\n", x) ;
    
        return 0 ;
    }

{% comment %} thenwait 5 {{ UserCoffee }}{{ "2021-6-5 1:40:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

<br>In my opinion, it generally will make your programs worse to think about what is going on here at the machine instruction level. At the C semantics level, a pointer, indicating the variable 'x', was passed to '`my_pointer_function`'. On different hardware and different systems, this is compiled to different machine instructions. Trying to out-think the compiler leads to code that will not work on other hardware or systems that do not work the same. There is very rarely a reason to write code that doesn't work on all systems, code that is not based on portable C semantics. Portable code makes life so much easier when you say "Cool! I didn't realize Apple (or whoever) would come out with the new M1 chip! Ummmm, will my code still work on it?" Remember when PC's went from 32 bit to 64 bit? The hardware changed. Changing the microprocessor mode to 64 bits killed a lot of non-portable code, so many programs still had to run in 32 bit mode and could not take advantage of the extra memory capability and so on that 64 bits provides.

In the above sample, the value 6 is not returned by the function. Instead it is set into the variable 'x' through the pointer. In fact, no value is returned by the function. This is indicated using a function return type of 'void'.

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-6-5 1:43:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

One particularly cool thing about this construct, is that you can pass as many pointers as you want. This allows functions to give multiple values to whatever called them. Here is another example you could copy to your compiler and run.

    #include <stdio.h>
    #include <stdlib.h>
    
    void my_pointer_function(int *my_first_int_pointer, int *my_second_int_pointer)
    {
        if (! my_first_int_pointer) {
            fprintf(stderr, "software error: my_pointer_function received NULL in arg 1.\n") ;
            exit(4) ;
        }
        if (! my_second_int_pointer) {
            fprintf(stderr, "software error: my_pointer_function received NULL in arg 2.\n") ;
            exit(4) ;
        }
        *my_first_int_pointer = 5 ;
        *my_second_int_pointer = 6 ;
    }
    
    int main()
    {
        int x, y ;
    
        my_pointer_function(&x, &y) ;
    
        printf("value set for x is: %d, for y is: %d\n", x, y) ;
    
        return 0 ;
    }

<br>

{% comment %} thenwait 31 {% endcomment %}{{ UserCoffee }}{{ "2021-6-5 2:14:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Putting This Together**

So, can you make pointers to structs? Absolutely! For example:

    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    ... more declarations and function code goes here ...
    
        struct simple_date *date_ptr = NULL ;
        struct simple_date bday ;
    
        date_ptr = &bday ;

Again, using '`&`' in front of a variable gets a pointer to that variable. So in this example, we get a pointer to the 'bday' variable, and assign that to the date_ptr variable. The 'bday' variable is declared to be of type '`struct simple_date`', the 'date_ptr' variable is declared to be of type '`struct simple_date *`', i.e. a pointer to a '`struct simple_date`', which matches what the '`&`' gets when applied to 'bday'.

So, how do we use pointers to structs? We could use the '`*`' operator, just like for an 'int' pointer. So, in order to set the bday.year member of bday to 2021, we could use

        date_ptr = &bday ;
        (*date_ptr).year = 2021 ;

However, there is a shortcut for this that means exactly the same thing. No magic, just a simple shortcut. Instead of '`(* pointer).member`', you can type '`pointer -> member`', where the arrow '`->`' gets the member value of a struct from a pointer to that struct. The spaces around the arrow are usually omitted, so it is written '`pointer->member`', and said "pointer arrow member". To write the previous example this way:

        date_ptr = &bday ;
        date_ptr->year = 2021 ;

This does not seem very useful. Why use '`date_ptr`', instead of just referencing the 'year' member of 'bday' directly? The answer is that pointers can be reassigned, they are not just stuck to always point at the same variable.

        struct simple_date date1, date2 ;
        struct simple_date *date_ptr = NULL ;
    
        date_ptr = &date1 ;
        .... Code that uses date_ptr for processing date1
        date_ptr = &date2 ;
        .... Code that uses date_ptr for processing date2

So why not just use 'date1' and 'date2' in the code that processes them, instead of using a pointer at all? You can pass the pointer as an argument to a function, so the same code can work on 'date1' and 'date2', depending on the pointer being passed. Here is the complete example.

    #include <stdio.h>
    #include <stdlib.h>
    
    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    void date_print(struct simple_date *date_ptr)
    {
        printf("year = %d month = %d day = %d\n",
            date_ptr->year, date_ptr->month, date_ptr->day) ;
    }
    
    int main()
    {
        struct simple_date tday, bday ;
    
        tday.year = 2021 ;
        tday.month = 6 ;
        tday.day = 4 ;
    
        bday.year = 1987 ;
        bday.month = 3 ;
        bday.day = 2 ;
    
        date_print(&tday) ;
        date_print(&bday) ;
    }

Technically, you could avoid using pointers in this case and just pass around the struct itself. But that is very inefficient as then all the data in the struct has to be copied each time it is passed. Also, there are things you can only do with pointers, as we will learn in the next paragraph. People almost always pass pointers to structs, not the structs themselves, except for a few simple special cases of small structs I will cover in a later section.

What if you want to change a member in a struct variable from a called function? If you pass a pointer to that struct, no problem! This is one of the main things you can only do by passing a pointer. Here is a complete example.

    #include <stdio.h>
    #include <stdlib.h>
    
    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    void setyearnow(struct simple_date *date_ptr)
    {
        if (! date_ptr) {
            fprintf(stderr, "software error: NULL argument to setyearnow\n") ;
            exit(4) ;
        }
        date_ptr->year = 2021 ;
    }
    
    int main()
    {
        struct simple_date tday ;
    
        setyearnow(&tday) ;
    
        printf("the year was set to %d\n", tday.year) ;
    
        return 0 ;
    }

Let's add a little bit more. The original sample code in this lesson had a problem.

        printf("You are %d years, %d months, and %d days old\n",
            tday.year - bday.year, tday.month - bday.month, tday.day - bday.day) ;

This will only work if today's month is greater than your birthday month, and today's day is greater than your birthday day. Otherwise, it prints out silly negative numbers.

Here is a function that sets the year, month, and day of a struct simple_date by subtracting two other dates, fixing the silly negative numbers. Note, function declarations are frequently put on multiple lines to make them more readable; in C you can put or omit line breaks in any code anywhere a space could be put.

    void date_subtract(
        struct simple_date *d_result,
        struct simple_date *d1,
        struct simple_date *d2
        )
    {
        if (! d_result || ! d1 || ! d2) {
            /* the '||' operator in condition expressions means 'or', one or the other */
            /* there is also an '&&' operator which means 'and', both one and the other */
            /* I will add a 'references' channel with a link to a page of the operators */
            fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
            exit(4) ;
        }
        d_result->year = d2->year - d1->year ;
        d_result->month = d2->month - d1->month ;
        d_result->day = d2->day - d1->day ;
        if (d_result->day < 0) {    /* fix it if the day is negative */
            -- d_result->month ;    /* use the decrement operator on the month */
            d_result->day += 31 ;    /* get rid of silly looking negative number */
        }
        if (d_result->month < 0) {    /* fix it if the month is negative */
            -- d_result->year ;
            d_result->month += 12 ;
        }
    }

This code is not right because all months do not have 31 days, and I am not going to fix it. There are C library functions that perform these kinds of calculations and they know how many days are in each month; functions which are discussed in a later lesson. It is not worth adjusting this simple function to be completely accurate.

Putting all this together.

    #include <stdio.h>
    #include <stdlib.h>
    
    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    void date_print(struct simple_date *date_ptr)
    {
        if (! date_ptr) {
            fprintf(stderr, "software error: NULL argument to date_print\n") ;
            exit(4) ;
        }
    
        printf("years = %d months = %d days = %d\n",
            date_ptr->year, date_ptr->month, date_ptr->day) ;
    }
    
    void date_scan(struct simple_date *date_ptr)
    {
        if (! date_ptr) {
            fprintf(stderr, "software error: NULL argument to date_scan\n") ;
            exit(4) ;
        }
    
        if (printf("enter year: "), fflush(stdout), scanf("%d", & date_ptr->year) != 1) {
            fprintf(stderr, "invalid year entry\n") ;
            exit(1) ;
        }
        if (printf("enter month: "), fflush(stdout), scanf("%d", &date_ptr->month) != 1) {
            fprintf(stderr, "invalid month entry\n") ;
            exit(1) ;
        }
        if (printf("enter day: "), fflush(stdout), scanf("%d", &date_ptr->day) != 1) {
            fprintf(stderr, "invalid day entry\n") ;
            exit(1) ;
        }
    }
    
    void date_subtract(
        struct simple_date *d_result,
        struct simple_date *d1,
        struct simple_date *d2
        )
    {
        if (! d_result || ! d1 || ! d2) {
            fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
            exit(4) ;
        }
        d_result->year = d2->year - d1->year ;
        d_result->month = d2->month - d1->month ;
        d_result->day = d2->day - d1->day ;
        if (d_result->day < 0) {    /* fix negative days */
            -- d_result->month ;    /* use the decrement operator on the month */
            d_result->day += 31 ;    /* get rid of silly looking negative number */
        }
        if (d_result->month < 0) {    /* fix negative months */
            -- d_result->year ;
            d_result->month += 12 ;
        }
    }
    
    int main()
    {
        struct simple_date bday, tday ;
        struct simple_date aday ;
    
        printf("Todays date:\n") ;
        date_scan(&tday) ;
    
        printf("Birthday date:\n") ;
        date_scan(&bday) ;
    
        date_subtract(&aday, &bday, &tday) ;
    
        printf("Your age:\n") ;
        date_print(&aday) ;
    
        return 0 ;
    }

Note that in most of the places in this code, 'struct' variables are actually operated on using pointers to them, not directly. That is normal for C code. In C, the '`->`' operator is used a lot.

A struct member can be a pointer. For example, '`struct some_struct { int *x_ptr ; } ;`' declares a structure which has a member which is a pointer to an integer. You can use this member in any way you can use a pointer to an 'int'. For example:

    struct some_struct {
        int *x_ptr ;
    } ;
    
    int main()
    {
        struct some_struct my_struct ;
        int x ;
    
        my_struct.x_ptr = &x ;
        ....

Then when you pass my_struct to something, that thing has access to the 'x' variable, or whatever variable you had set the pointer to. Here's a complete example.

    #include <stdio.h>
    #include <stdlib.h>
    
    struct some_struct {
        int *x_ptr ;
    } ;
    
    void f(struct some_struct *some_pointer)
    {
        * some_pointer->x_ptr = 6 ;
    }
    
    int main()
    {
        struct some_struct my_struct ;
        int x ;
    
        x = 5 ;
        my_struct.x_ptr = &x ;
    
        printf("original value for x: %d\n", x) ;
    
        f(&my_struct) ;
        
        printf("final value for x: %d\n", x) ;
    
        return 0 ;
    }

This form, of having a member of a struct being a pointer to an 'int' or pointer to some other numeric type, is rarely used. What is used frequently is when a member of a 'struct' is a pointer to another 'struct'. This really helps organize data a lot, which we will see more of in the next lesson. For example, lets make a 'struct' that contains members which are pointers to two simple_date structs.

    struct twodates {
        struct simple_date *date1 ;
        struct simple_date *date2 ;
    } ;
    ...
        struct simple_date d1, d2 ;
        struct twodates my_twodates ;
        my_twodates.date1 = &d1 ;
        my_twodates.date2 = &d2 ;
        ...

Let's rewrite the previous sample code using this structure, just for fun. Here is the last sample code.

    #include <stdio.h>
    #include <stdlib.h>
    
    struct simple_date {
        int year ;
        int month ;
        int day ;
    } ;
    
    struct twodates {
        struct simple_date *date1 ;
        struct simple_date *date2 ;
    } ;
    
    void date_print(struct simple_date *date_ptr)
    {
        if (! date_ptr) {
            fprintf(stderr, "software error: NULL argument to date_print\n") ;
            exit(4) ;
        }
    
        printf("years = %d months = %d days = %d\n",
            date_ptr->year, date_ptr->month, date_ptr->day) ;
    }
    
    void date_scan(struct simple_date *date_ptr)
    {
        if (! date_ptr) {
            fprintf(stderr, "software error: NULL argument to date_scan\n") ;
            exit(4) ;
        }
    
        if (printf("enter year: "), fflush(stdout), scanf("%d", & date_ptr->year) != 1) {
            fprintf(stderr, "invalid year entry\n") ;
            exit(1) ;
        }
        if (printf("enter month: "), fflush(stdout), scanf("%d", &date_ptr->month) != 1) {
            fprintf(stderr, "invalid month entry\n") ;
            exit(1) ;
        }
        if (printf("enter day: "), fflush(stdout), scanf("%d", &date_ptr->day) != 1) {
            fprintf(stderr, "invalid day entry\n") ;
            exit(1) ;
        }
    }
    
    void twodates_scan(struct twodates *twodates_ptr)
    {
        printf("Enter first date (day1):\n") ;
        date_scan(twodates_ptr->date1) ;
    
        printf("Enter second date (day2):\n") ;
        date_scan(twodates_ptr->date2) ;
    }
    
    void twodates_subtract(
        struct simple_date *d_result,
        struct twodates *twodates_ptr
        )
    {
        struct simple_date *d1 = NULL ;
        struct simple_date *d2 = NULL ;
    
        if (! d_result || ! twodates_ptr) {
            fprintf(stderr, "software error: NULL argument to date_subtract\n") ;
            exit(4) ;
        }
    
        d1 = twodates_ptr->date1 ;
        d2 = twodates_ptr->date2 ;
        if (! d1 || ! d2) {
            fprintf(stderr, "software error: NULL pointer in argument to date_subtract\n") ;
            exit(4) ;
        }
    
        d_result->year = d2->year - d1->year ;
        d_result->month = d2->month - d1->month ;
        d_result->day = d2->day - d1->day ;
        if (d_result->day < 0) {    /* fix negative days */
            -- d_result->month ;    /* use the decrement operator on the month */
            d_result->day += 31 ;    /* get rid of silly looking negative number */
        }
        if (d_result->month < 0) {    /* fix negative months */
            -- d_result->year ;
            d_result->month += 12 ;
        }
    }
    
    int main()
    {
        struct simple_date day1, day2 ;
        struct simple_date dur ;
        struct twodates bothdates ;
    
        bothdates.date1 = &day1 ;
        bothdates.date2 = &day2 ;
    
        twodates_scan(&bothdates) ;
    
        twodates_subtract(&dur, &bothdates) ;
    
        printf("Change from day1 to day2\n") ;
        date_print(&dur) ;
    
        return 0 ;
    }

And that's it for Four Square. Making 'struct' variables. Pointer variables, including pointers to 'struct variables', in functions and inside 'struct' variables. Get this, and you will be a solid C programmer.<br>
<br>

{% comment %} thenwait 18 {% endcomment %}{{ UserCoffee }}{{ "2021-6-5 2:50:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Homework:**

In the last sample code of this lesson, add a '`height`' member to '`struct simple_date`', of type '`float`'. Change the '`date_print`' and '`date_scan`' code to also print and scan the height. Change the '`twodates_subtract`' code to deal with the height as well. By doing this, you add to the code an ability to calculate how much you grew between the two dates. Note that you did not have to change the '`main`' code at all, or much of the other code, in order to add this functionality. Using 'struct' and pointers to 'struct' allows code to be developed and evolve without having to change every little thing as you put in more stuff, and keeps everything better organized.

Please let me know if you have any questions, and feel free to paste homework results into this thread.
