<!---
_includes/content/lessons/2/lesson.md   content for lesson 2 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{{ UserCoffee }}{{ "2021-5-30 9:56:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Breakfast. Scrambled eggs, almost runny. Lots of Tapatio. Yummm<br>
I suggest you finish the beginning assignment before paying any attention to this one.

{% comment %} thenwait 4 {{ UserCoffee }}{{ "2021-5-30 10:00:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

In the very first lesson we started with scanf. Oh my god! Scanf is such a harsh place to start, so unforgiving! Yesterday I thought long and hard, for whole minutes, regarding where to start. The training should be interactive. So it should take input. There are many ways to take input. However, scanf is quick and easy, as long as you respect it. This is C, if you mess up, you will get burned. That is a big part of these lessons: why normal people should not use C. Hot pokers can be good teachers, yet somehow normal people don't see that.

{% comment %} thenwait 4 {{ UserCoffee }}{{ "2021-5-30 10:04:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

In the very first lesson we used pointers, created with those '&' in front of the non-fmt scanf arguments. Pointers! A deep C mystery! In the first lesson! Well, I think the whole world should understand pointers. They're just a way to point at something, so you can write to it in another function or share it in other ways. Not so complicated after all.

{% comment %} thenwait 9 {% endcomment %}{{ UserCoffee }}{{ "2021-5-30 10:13:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Also, in the very first lesson we discussed the ',' operator. Separating a list of functions with the ',' operator calls them all, and has the value of the last function. The ',' operator: one of those obscure C operators, in the very first lesson? Oh my god! However, is it really that complicated to call several functions in order? Yet the comma operator can get complicated: is `a = b, c, d` the same as `(a = b), c, d`, which sets a to b, then calls c and d; or `a = (b, c, d)`, which calls b then c then d and sets a to the result. The truth is, I don't remember for sure, because I would never write `a = b, c, d`, I would always use the parenthesis.  This is another good lesson for C: it is possible to write code that requires obscure knowledge to understand clearly, but why? Usually, just include some extra parenthesis, and the meaning becomes clear.

{% comment %} thenwait 15 {% endcomment %}{{ UserCoffee }}{{ "2021-5-30 10:28:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

And finally, in the first lesson we discussed the fflush function to be used between writing a prompt and reading some input. It is used because the prompt might be buffered in the output buffer, and without fflush the prompt might not actually be written to the user until later, and so the program waits for input from the user without the prompt being written yet and it becomes very confusing what is happening. In fact, using fflush is almost never necessary, because stdio figures out when to flush automatically. And it almost always works. Except when it doesn't. So some snot nosed kid might point out that calling fflush unnecessarily is wasting CPU resources. It actually can tax the CPU a lot if calling fflush causes various buffers to be misaligned. The kid might say to run the program without fflush, then add the fflush if it doesn't work because the prompt does not come out before the input is being read. A seasoned programmer would say "if it doesn't work ... on what system? with what I/O library? with the input and output sources using what device type?" We try to write code that always works. Simple when possible, yet bulletproof. So, always call fflush between writing a prompt and reading the input, and nobody gets hurt. Even if your code is being used much later on some operating system that hasn't even been invented yet that does the output buffering differently.

{% comment %} thenwait 4 {{ UserCoffee }}{{ "2021-5-30 10:32:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

I already know what I want to include in lesson number two. First, hopefully everyone will complete the beginning lesson. Then, after completing the beginning lesson, having fun with scanf, and posting their code, we can go on to this lesson. And I expect that is not happening until after lunch time! Probably not today's lunch, either.

{% comment %} thenwait 1313 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 8:25:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Number Two

{% comment %} thenwait 28 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 8:53:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

This lesson is called Number Two because it is the second lesson, not because of anything about C. However, if you consider C to be Number One, then Number Two might be C++. C code works in a C++ compiler, mostly, since C is a subset of C++, mostly. Many consider C++ to be a better language for novices because it has new constructs that allow avoiding some of the hot poker C constructs. For example, in C you can write:

    { int x ; return &x ; }

using the pointer construct from yesterday to point to the local variable x. This innocent looking code will crash very badly, because when the function defining x returns, the space for its local variables no longer exists, so the pointer is now pointing to some other random thing. Depending on what is done with the pointer, it may take a long time to debug something like this. C is a hot poker language, be careful or you will get burned. People can use C++ with C++ library objects and avoid this type of situation entirely. Also, C++ has constructs to allow organizing large coding projects, for instance defining new object types to encapsulate functionality, many consider C++ a better language for advanced programming projects. So why learn C at all?

{% comment %} thenwait 4 {{ UserCoffee }}{{ "2021-5-31 8:57:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

1\. The hot poker C constructs are used extensively by seasoned programmers when defining objects, especially advanced objects like the ones in the C++ library. You do not need to know these constructs to use the C++ library, but if you want to extend this library or create your own, you need to have a full understanding of these hot poker C constructs. If you want to be a master programmer, you need to eat, breathe, and dream them. Also, they are not that complicated, I think they are simpler than some of the safer C++ constructs. The C ones just need a little care when being used.

2\. C is a simple language. A good place to start, if you want to go all the way programming wise. That simplicity is the core of this lesson.

{% comment %} thenwait 85 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 10:22:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Here is a more-or-less complete definition of C in just a few pages. You might want to read these pages a few times.<br>
<br>
A C source file consists of a series of declarations. These declarations can be of variables, functions, or types. Mixed in are other syntactic things, such as C comments. A C variable declaration is of the form `type variable, variable, ... ;`, it is possibly to put in an initial value as `variable = value` instead of just `variable`. A function declaration is of the form:

    function_name ( parameters ) ;

for a function that is defined elsewhere, and

    function_name ( parameters )
    {
        statement
        statement
        ...
    }

to define a function, which will be covered in the next lesson. Declarations of types define new types, which will be covered in a later lesson.

There are relatively few C statements. We have already seen the 'if' statement:

        if ( expression ) statement
        else statement

Anywhere there can be a statement, there can be a list of statements surrounded by '{' and '}'.<br>
Right after any '{' there can be a list of declarations before the list of statements. The variables declared here are only valid within the '{' '}' block of statements. We have seen variable declarations after the opening '{' for a function, yet they can be after any '{', which is a good way of saying that a particular variable is only used in the particular list of statements contained in the '{' '}' list. The following is an example of this.

        if (x != 1) {
            int x3 = x * x * x ;
            printf("x=%d  x cubed = %d  x to the sixth = %d", x, x3, x3 * x3) ;
        }

Other statements are:

        while ( expression ) statement

which repeats the statement while the expression is true.

        do statement while ( expression ) ;

which does the statement first without checking the expression until it is done the first time.

        return expression ;

which returns a value from the current function.

One of the most used statements is just an expression, called an expression statement:

        expression ;

which does the expression and that's all.

In C, assignments are actually expressions, so you can type:

        a = b = c ;

which is equivalent to `a = (b = c) ;`, i.e. the value of c is assigned to b, and then the result of that assignment expression, which is still the value c, is assigned to a. Once you think of C assignments as just expressions this becomes a very simple concept.<br>
Also, the comma operator discussed in the beginning lesson is used in expressions, so

        f(x) , g(x)

is an expression consisting of calling f, then calling g, and the value of the expression is g(x).<br>
Expressions are things that have values, as shown in the following expression statement using the assignment and comma operators:

        a = ( f(x), g(x) ) ;

However, the value can be ignored, as shown in the following expression statement using just the comma operator: `f(x) , g(x) ;`

The final statement that is frequently used is the 'for' statement, which is written:

        for ( expr1 ; expr2 ; expr3 ) statement

The semantics of the 'for' statement are that expr1 is done first, only once. Then, while expr2 is true, the statement after the parenthesis is repeatedly done, and each time after that statement expr3 is done. For example:

        for (n = 1, total = 0; n <= 10; n = n + 1) total = total + n * n ;

This finds the sum of the squares of 1 through 10, and is equivalent to

        n = 1, total = 0 ;
        while (n <= 10) {
            total = total + n * n ;
            n = n + 1 ;
        }

but the 'for' loop construct is generally a lot more concise and easier to read.

        ;

Just semicolon is a null statement that does nothing. It is occasionally useful as the body statement of a 'for' where the meat is in the expressions.<br>
There are a various statements for non-local control transfer: 'goto', 'switch', 'break', and 'continue'. None of these is very complicated, and they are not used as often as the ones mentioned above. They can be studied in a later lesson.<br>
Note that every C statement ends with a C statement inside of it, such as the statement inside of the 'if' statement, or explicitly ends with a ';', such as the 'return' statement. Thus, the single ';' always ends a statement, including a statement containing a statement inside of it. The exception to this is when a statement contains a list of statements surrounded by '{' '}' inside of it, in which case the statement ends with '}' and not ';'.

There are many expression operators in C, such as comma, assignment, '+', '-'. Going over them all would require another lesson.<br>
<br>
There are relatively few basic C data types. There are 'int', 'char', 'float'. For 'int' there is 'long int' with more bits, and 'short int' with less bits. Just 'long' or 'short' implies 'long int' or 'short int'. If more or less bits are not available for the hardware being used, then 'long' or 'short' may still be used so that code still compiles, and they are the same as 'int'. A 'char' variable is one byte. The 'float' type comes with a 'long float', which has more bits if more bits are available in the hardware. A 'long float' is also called a 'double', these are synonyms for the same thing. The 'float' type is for "floating point", aka base 2 decimal fractions. They also internally have exponents, so one float variable can store 3.57 * 10^42, which is the number of elementary particles in the universe *(prove I'm wrong, eh)*. In C code, this value would be written `3.57e42`. Not so complicated.<br>
<br>
So there you have it. You have learned C by the end of Number Two. You can now take the rest of the week off. May I suggest a nice beach?

{% comment %} thenwait 8 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 10:37:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

Here is a program to illustrate some of this:

{% comment %} thenwait 3 {{ UserCoffee }}{{ "2021-5-31 10:40:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

    #include <stdio.h>
    
    int main()
    {
        float x ;
        
        while (printf("enter x: ") , fflush(stdout), scanf("%f", &x) == 1) {
            double y =   x * x * x / 42 + 7.0 / 11 ;
            printf("x = %f y = %f\n", x, y) ;
        }
        return 0 ;
    }

Look familiar? C is like that. Simple concepts go a long way.

{% comment %} thenwait 7 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 10:47:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

First of all, `while` is one of the repeating statements, 'looping' as it's called. `scanf` returns the number of items read, if the next input is not a number, it will return 0, and the 'while' will end since the condition is no longer non-zero. So this program will loop, reading a floating point number each time, and calculate x^3/42 + 7/11 each time around, until you type something besides a number, such as 'q'.

{% comment %} thenwait 5 {{ UserCoffee }}{{ "2021-5-31 10:52:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}{% endcomment %}

I used `x * x * x` instead of using some function to calculate x to the power of 3 because multiply is done directly by the hardware so it might be a little quicker. These days, compilers are pretty smart so it might do something really tricky when you call the function to calculate powers, like directly inline floating point accelerator code, but for a square or a cube I would still probably use this construct out of habit. It makes the code less readable to do it this way, but it is likely slightly faster, also it doesn't depend on having access to a power function so building the overall program might be slightly easier to maintain using `x * x * x`. Using a power function might cause more consistency in handling overflow and other special cases. Up to the coder's judgement which way to do this.

{{ UserZeta }}{{ "2021-5-31 10:55:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

I want all the hot pokers!

{{ UserCoffee }}{{ "2021-5-31 10:58:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

The `scanf("%f", &x)` is using the scanf fmt `"%f"` to indicate the type of the x argument, a `float`. This must match or you will be burned by a hot poker, as mentioned in the beginning lesson. I believe `"%lf"` is used for `double` since it is also called `long float`. In C, calculations are not performed using `float`, they are always calculated using `double`, or sometimes using even more bits than that if that is best with the hardware. So I almost always declare my variable types using `double`, why waste the extra bits that have already been calculated? Except! If a pointer to the variable is being taken, such as in a scanf argument, the type must match, such as `float` versus `double`, so changing the declaration of 'x' in this code without changing the scanf fmt would be a bad thing.

{% comment %} thenwait 10 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 11:08:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

In C, there is only one division operator. If both operands are integer, it will use integer division, and lose any fractional part in the answer. However, if either operand is a float or double, it will use a floating point division, without truncation. So in this code example I divided by 42 and 11, but in both cases the actual division was done in floating point because the other operand was floating point. This is standard in C- the same operator can handle multiple data types, and the operation is performed using the more general data type, where the operands of the less general data type are promoted to the more general. Note that `7 / 11` would be zero, because in that case both operands are integer, so the division is done using integer division and the fractional part is lost, which is why I wrote `7.0 / 11`. Many people would consider that bad code, they want to see `(double)7 / (double)11`, yet that seems unnecessarily verbose to me.

{% comment %} thenwait 7 {% endcomment %}{{ UserCoffee }}{{ "2021-5-31 11:15:00 AM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}

**Homework:** put a loop inside a loop. Inside the 'while' loop, put a 'for' loop. This 'for' loop should start at 0.1, and go up to not exceed x, where the 'for' loop index goes up by a factor of two every time, i.e. 0.1, 0.2, 0.4, 0.8, 1.6, ... Calculate the same function as in the program above for each value of the 'for' loop index, but instead of printing it out, find the total of all the calculated values. Then, print out the total when the 'for' loop is done. This is called nested looping, and happens all the time. The code inside the inside loop is executed many times more often than the other code, so optimizing it has a big impact on program run times, and it helps to notice nested loops and speed them up to make the overall program go faster. The inside loop is called the ‘inner loop’, so watch your inner loops for faster code.

*This homework assignment is a bridge too far for beginning programmers who have never seen looping before. When I have some time, I will do the nested loop in the assignment using 'while', explain nested looping, then simplify the homework to just convert this to the equivalent nested loop using 'while' and 'for'. Sorry I haven't gotten to it it.*

Please post here to let me know if you have any questions, and feel free to post your homework code here when you have it.
