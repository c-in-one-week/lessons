<!---
_includes/content/lessons/7/lesson.md   content for lesson 7 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

And for the 7th day, God said we rest. So why is there a lesson today at all?

What's next?

Next you could learn how to write GUI code instead of just using printf/scanf/fgets etc. The basic paradigms are different for every system's GUI - Windows, Mac, Android, iPhone, Linux. This takes a huge amount of effort to learn the details, and it is something that might never be relevant for you depending on which system you need to use. I would suggest getting good at C first, then good at C++, then use a good C++ library for a GUI framework which already has all the line-by-line GUI code encapsulated for you. If you are good at C++, you will be able to customize that library as you need for your particular case, using subclassing and templated type definitions in terms of the library templates, and you will have better - faster - stronger code with a minimum of effort. All you need is C, then C++.

Many people just copy some mountain of code with limited understanding, and hack little bits of it to get what they want, and that's it. Then when something unexpectedly doesn't work, about all they can do is try to figure how to explain it was all someone else's fault. With this class, you are already beyond that. For GUI development, a sufficiently documented C++ GUI library for some particular system can be used with full understanding of the GUI system in terms of the library objects, and very little effort or new code, as long as you are good at C++. There are many GUI Development Tools for generating GUI apps with hardly any programming at all. That's pretty much what Microsoft VB6 and its successors, including most of .net, really are. They don't compare in capability to using a strong C++ GUI library, such as Microsoft C++.net and WTL or the older ATL. Some development tools are multi-system, e.g. iPhone and Andoid, or Windows and Mac. These are frequently the easy way out for novices that provides less capability. Once you get good at C++, then something like WTL is actually easier and more powerful for the specific system it targets.

Then there's networking. I thought it was so complicated I didn't even try for way too long. Then I had to, and I found out it is not that complicated after all. Basically it's just: create a socket, then read/write to it. After the socket is created, before using it for reading or writing, you have to give it an IP address, a port, and the communication type. These are given to the socket using bind() or connect(), depending on whether your are listening for connections or creating them. This is all documented with examples in https://man7.org/linux/man-pages/man3/getaddrinfo.3.html You should be able to follow everything in the example in that link by now. You could try compiling and running it if you want, I didn't read it that carefully so I do not know what it does or depends on.

So instead, I suggest you complete the final project. Which is ...

What do you want to do?

Would you like to write an interactive calculator that runs on the terminal for calculating with complex numbers? That was one of my first programs on my little hp calculator. Or how about vectors? Think of something you can do with only C coding, that doesn't require your program to interact with other programs or networking, until you are solid at writing the code itself.

My first compiled program calculated the amount of fuel for a rocket, with the fuel efficiency and payload of the moon rockets, to go to the nearest star in one lifetime. I was young, before I had any type of calculator. I didn't know the rocket equation, which can be used to calculate this directly. When I saw the results, with all those digits for the amount of fuel needed, I became very depressed. I wish I hadn't been, I should not have put aside my dream so easily.

So many things you could write. How about a casino that runs in terminal: no graphics because it's just terminal, somewhat simpler to implement that way, but you could have it ask what card game you wanted to play, then it could beat you and take all your money. You could have a library for basic card functions such as maintaining a deck and getting a card and shuffling and so on. You could have a library for high level interactions, such as dealing and displaying a hand. You could have a library to maintain the players, with pointers to the hand for each player, and other player info, it might emulate their behaviour for players other than the player running the program, including the house of course. You could have a library built on top of these libraries for each game: black jack, 5 card draw, 7 card stud, etc. Big project!

Learning something requires practice. Many ideas, even ones that seem simple, turn into big projects. A significant programming project takes much more time that this entire C class. Really, what is needed is a bunch of little homework assignment style projects to get good at the language, before you forget everything you just learned, that do not take too much time.

So, go for it. Whatever big or little projects appeal to you.

Dream it! Write it! You can do it!<br>
Done
