---
layout: lesson
title: 4. Four Square
---

<!---
4-four-square/index.md   lesson 4 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/4/lesson.md -%}

{% include content/extra/4/comments.md %}
