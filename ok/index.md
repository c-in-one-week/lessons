---
layout: nonlesson
title: Ok
pageTitle: C In One Week - Ok, So You Want To Be A C Programmer
---

<!---
ok/index.md   so you want to be a programmer page in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include content/other/ok.md -%}
