#!/bin/sh

# git-pre-push-hook git pre-push script for c-in-one-week.com

# builds production c-in-one-week.com site, then pushes it for deployment

# A hook script to verify and build what is about to be pushed.  Called by "git
# push" after it has checked the remote status, but before anything has been
# pushed.  If this script exits with a non-zero status nothing will be pushed.
#
# This hook is called with the following parameters:
#
# $1 -- Name of the remote to which the push is being done
# $2 -- URL to which the push is being done
#
# Information about the commits which are being pushed is supplied as lines to
# the standard input in the form:
#
#   <local ref> <local sha1> <remote ref> <remote sha1>
#
# This script is used for c-in-one-week.com, which is currently set up to do
# production deployment builds locally instead of on the server.
#
# The production c-in-one-week.com site for deployment is built by this hook
# into _build_deploy/_deploy folder, which is then added and committed and
# pushed to the site-pages repo which uses gitlab CI/CD to deploy the web site.
#
# If the remote URL is not the actual site, e.g. for a fork, push with no build

# git-pre-push-hook   called by pre-push to build and deploy production site
# Copyright (C) 2021  T Shaw
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program (COPYING-GPL-v2-txt);
# if not, write to the Free Software # Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


set -e

ME='git push'
URL='git@gitlab.com:c-in-one-week/lessons.git'

TOOLDIR='_tools_deploy'

if [ "$2" != "$URL" ]; then
  echo "Pushing to fork: '$2', not actual site: '$URL'. Not rebuilding site."
  exit 0
fi
if [ "$1" != origin ]; then
  echo "WARNING: $ME: expecting to push back to 'origin' remote, not '$1'."
  exit 0
fi
if [ ! -d "$TOOLDIR" ]; then
  echo "$ME: pre-push hook not called in top level lessons folder."
  echo "$ME: current directory '`pwd`' does not contain '$TOOLDIR'."
  exit 1
fi
if [ ! -x "$TOOLDIR/build_production" ]; then
  echo "$ME: 'build_production' not found in '`pwd`/$TOOLDIR'."
  exit 1
fi

git fetch

if git --no-optional-locks status --porcelain -b -uno |
			grep '^##.*ahead' >/dev/null
then :
else
  echo "Nothing committed to push"
  exit 0
fi

if git --no-optional-locks status --porcelain -b -uno |
			grep '^##.*behind' >/dev/null
then
  echo "Remote repo has new changes:"
  git --no-optional-locks diff --compact-summary HEAD...origin
  echo 'Must pull new changes before push.'
  exit 1
fi

MSG="`git log --reverse --format='%h - %s' origin..HEAD`"

# fetch local main commits into _build_deploy
(   set -e
    cd _build_deploy
    echo -n 'Recent commits made by current user: '
    git pull )

# fetch existing site into _build_deploy/site-pages
# if there are unpushed edits in site-pages, this will likely have merge issues
(   set -e
    cd _build_deploy/site-pages
    # avoid merge problems
    rm -rf _deploy
    echo -n 'Recent site page changes already deployed: '
    git pull )

# check that deployment process files in repo match init files every push
./_tools_deploy/deploy_files.sh _build_deploy

# build _build_deploy into site-pages/_deploy
(   set -e
    cd _build_deploy
    "$TOOLDIR/build_production" site-pages/_deploy )

cd _build_deploy/site-pages

# use git to see if any changes; exit 0 if none
if git diff --quiet ; then
  echo "No site page changes to be deployed."
  echo "Pushing new repo content."
  exit 0
fi

# deploy changed files
echo
echo "Web site files changed. Changes being pushed."
git diff --name-only
git add .
if [ `echo "$MSG" | wc -l` -eq 1 ] ; then
  MSG="commit: $MSG"
else
  MSG="commits:
$MSG"
fi
git commit -m "associated lessons.git $MSG"
git push

echo "Finished deploying changed site pages."
echo "Pushing new repo content."

exit 0
