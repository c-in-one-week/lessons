#!/bin/sh

# deploy_files.sh [ -m ] [ -v ] build_directory
#
# create and check build directory for deployment process files
# and set the git pre-push hook to deploy the site when the sources are pushed
# deployment process files come from the same directory as this script
#
# the -m flag causes this script to build the deployment directory and set hook
# the -v flag causes this script to be more verbose
#
# deployment works by pushing the site-pages repo put in the build directory
# the site-pages repo has a _deploy subdirectory with web site content
# when site-pages is pushed, a gitlab CI/CD script places the site content into
# the public subdirectory, which is where gitlab CI/CD expects site content
# deployment to public from _deploy is where hash codes are added to bypass the
# browser cache and reload the new pages when site pages change
#
# deployment process files in the site-pages repo include:
# .gitignore		an anachronism from a different directory structure
# .gitlab-ci.yml	control configuration for gitlab CI/CD
# public_deploy.sh	called by .gitlab-ci.yml to deploy to public directory
# README.md		README for the deployed site pages repo
#
# site content is built into the _deploy subdirectory in the site-pages repo
#
# the git pre-push hook is named pre-push in .git/hooks of the development repo
#
# there are three levels of repo directory:
#  directory that development is being performed in, such as 'lessons'.
#  directory that deployed pages are build in, such as 'lessons/_build_deploy'.
#  directory for deployed pages, such as 'lessons/_build_deploy/site-pages'.
# the _build_deploy directory is just a clone of .., used to pull any source
# commits to be deployed, without disturbing the development directory itself
#
# this script will not exit non-zero unless something drastically goes wrong,
# like it is being called incorrectly. if initialization files for deployment
# process files do not exist, it will print a warning that automatic
# deployment on git push will not work, and it will not set the git pre-push
# hook, but it still will exit 0. However, it uses 'set -e', so if any
# command fails, such as a git clone, it will fail out.
#
# this script never removes or changes any existing deployment process files,
# including the git pre-push hook, so the warning that automatic deployment
# on git push will not work may be incorrect if pre-push is already installed
#
# This script works if the site-pages repo is completely empty, so long as it
# exists, so if the site-pages repo gets too full of old commits of all the
# deployed site pages ever, just delete the repo completely, then create a new
# one called site-pages. The web site may need to be re-configured on Gitlab
# after doing that. After that, if any clone if the development repo does
# a push, the push will complain about missing files in site-pages, but still
# push new content to the new site-pages repo. The push will say to run
# the './install_dev' script to install the missing deployment process files.
# New content pushed before './install_dev' is used will not actually be
# deployed, because the new repo will not have a .gitlab-ci.yml file.
# Once './install_dev' is run in any development clone after a new site-pages,
# then that clone will push all the deployment process files, and they will
# automatically be pulled by all other development clones after that.
#
# it is ok to run this script multiple times
# this script was written under Debian

# deploy_files.sh   check for files used by deployment process
# Copyright (C) 2021  T Shaw
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


set -e

ME="`basename "$0"`"
TOOLS_DIR="`dirname "$0"`"

MFLAG=false
VFLAG=false
while [ $# -ge 1 ]
do
  case "$1" in
    -m ) MFLAG=true ;;
    -v ) VFLAG=true ;;
    -* ) echo "usage: $0 [ -m ] [ -v ] build-directory" ; exit 1 ;;
    * ) if [ -n "$BUILD_DIR" ]; then
          echo "$ME: more than one build directory argument given"
          exit 1
        fi
        BUILD_DIR="$1" ;;
  esac
  shift
done

if [ -z "$BUILD_DIR" ]; then
  echo "$ME: build-directory argument missing"
  exit 1
fi

if ! [ -d .git ]; then
  echo "$ME: should be run in a git directory of sources for deployed pages"
  exit 1
fi

REPO_DIR="$BUILD_DIR/site-pages"

if [ ! -x "$TOOLS_DIR/pre-push" -o \
     ! -f "$TOOLS_DIR/ci-copy.gitlab-ci.yml" -o \
     ! -f "$TOOLS_DIR/public_deploy.sh" ]
then
  if [ ! -f .git/hooks/pre-push -o ! -f "$REPO_DIR/.gitlab-ci.yml" ]; then
    # perhaps installed .gitlab-ci.yml in repo does not need public_deploy.sh
    if $MFLAG ; then
      if $VFLAG ; then echo ; fi
      echo "Warning: 'pre-push', 'ci-copy.gitlab-ci.yml',\
 or 'public_deploy.sh' not found."
      echo "Warning: can not enable auto deployment of site pages by git push."
    fi
  fi
  exit 0
fi

if $VFLAG ; then
  echo
  echo "Enable auto deployment by git push using local build then copy up."
fi

# build directory and repo structure if '-m' flag given and not already there
if [ ! -d "$BUILD_DIR" ]; then
  if ! $MFLAG ; then
    echo "$ME: '$BUILD_DIR' does not exist."
    echo "$ME: git push will not automatically build and deploy site pages."
    echo "$ME: use ./install_dev to create build directories."
    exit 0
  fi
  (   set -e
      mkdir "$BUILD_DIR"
      cd "$BUILD_DIR"
      git clone .. . )
fi
if [ ! -d "$REPO_DIR" ]; then
  if ! $MFLAG ; then
    echo "$ME: '$REPO_DIR' does not exist."
    echo "$ME: git push will not automatically build and deploy site pages."
    echo "$ME: use ./install_dev to create build directories."
    exit 0
  fi
  (   set -e
      cd "$BUILD_DIR"
      git clone git@gitlab.com:c-in-one-week/site-pages.git --depth 1 ||
        true )
  if [ ! -d "$REPO_DIR" ]; then
    echo "$ME: failed cloning site-pages.git into '$REPO_DIR'."
    echo "$ME: perhaps there is no access or account with Gitlab."
    echo "$ME: git push will not automatically build and deploy site pages."
    exit 0
  fi
fi

# do not install files in site-pages if they already exist but are different,
# so will init if empty new site-pages repo, yet use old stuff if already there
if [ ! -f "$REPO_DIR/.gitlab-ci.yml" ]; then
  cp "$TOOLS_DIR/ci-copy.gitlab-ci.yml" \
		"$REPO_DIR/.gitlab-ci.yml" -v
elif ! cmp -s "$TOOLS_DIR/ci-copy.gitlab-ci.yml" \
		"$REPO_DIR/.gitlab-ci.yml" ; then
  echo "Warning: deployment .gitlab-ci.yml does not match its init file."
fi
if [ ! -f "$REPO_DIR/public_deploy.sh" ]; then
  cp "$TOOLS_DIR/public_deploy.sh" "$REPO_DIR" -v
elif ! cmp -s "$TOOLS_DIR/public_deploy.sh" \
              "$REPO_DIR/public_deploy.sh" ; then
  echo "Warning: deployment 'public_deploy.sh' does not match its init file."
fi

# not the end of the world if site-pages
# is not initialized with .gitignore if it is missing in current;
# site-pages has a _deploy subdirectory that must be pushed,
# but if somehow one ends up in the development directory, it must be ignored
if [ ! -f "$REPO_DIR/.gitignore" ]; then
  if [ -f .gitignore ]; then
    echo "creating '$REPO_DIR/.gitignore'"
    grep -F -x -v _deploy < .gitignore > "$REPO_DIR/.gitignore"
  elif $VFLAG ; then
    echo "Missing '.gitignore' in '$REPO_DIR'."
  fi
elif [ -f .gitignore ]; then
  if grep -F -x -v _deploy < .gitignore | cmp -s - "$REPO_DIR/.gitignore"
  then :
  else
    echo "Warning: deployment '$REPO_DIR/.gitignore' does not match."
  fi
fi
if $VFLAG ; then
  if [ ! -f .gitignore ]; then
    echo "Warning: no top level '.gitignore' file."
  elif ! grep -F -x -v _deploy < .gitignore > /dev/null; then
    echo "Warning: top level '.gitignore' should ignore '_deploy'."
  fi
fi

# not end of the world if README.md is not initialized if missing in TOOLS_DIR
if [ -f "$TOOLS_DIR/init-site-README.md" ]; then
  if [ ! -f "$REPO_DIR/README.md" ]; then
    cp "$TOOLS_DIR/init-site-README.md" "$REPO_DIR/README.md" -v
  elif ! cmp -s "$TOOLS_DIR/init-site-README.md" "$REPO_DIR/README.md" ; then
    echo "Warning: deployment repo README.md does not match its init file."
  fi
else
  echo "Warning: '$TOOLS_DIR' has no 'init-site-README.md'."
  if [ ! -f "$REPO_DIR/README.md" ]; then
    echo "Warning: '$REPO_DIR' has no 'README.md'."
  fi
fi

# do pre-push last so if anything above fails, can still push normally
if [ -f "$TOOLS_DIR/pre-push" -a -f .git/hooks/pre-push ]; then
  if ! cmp -s "$TOOLS_DIR/pre-push" .git/hooks/pre-push; then
    if $MFLAG ; then
      cp "$TOOLS_DIR/pre-push" .git/hooks -v
    else
      echo "Warning: '.git/hooks/pre-push' does not match its init file."
    fi
  fi
else
  if [ ! -f "$TOOLS_DIR/pre-push" ]; then
    echo "Warning: '$TOOLS_DIR' has no git pre-push hook file."
  fi
  if [ ! -f .git/hooks/pre-push ]; then
    if $MFLAG ; then
      cp "$TOOLS_DIR/pre-push" .git/hooks -v
    else
      echo "$ME: no git pre-push hook installed so no automatic deployment."
      echo "$ME: use ./install_dev to install it."
    fi
  fi
fi
