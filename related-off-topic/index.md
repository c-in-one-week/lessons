---
layout: lesson
title: Related Yet Off Topic Comments and Questions
pageTitle: C In One Week - Related Yet Off Topic
---

<!---
related-off-topic/index.md   related yet off topic in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

## {{ page.title }}

{% include lessonhead.html %}

{% include content/extra/r/comments.md %}
