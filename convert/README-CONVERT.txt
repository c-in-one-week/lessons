Much of the content of C In One Week comes from a Discord chat section
as discussed in CONTENT BACKGROUND below. This document says how to convert
the Discord section content to content for use in the web site.


HAND EDITING CONTENT AFTER CONVERSION

Note that after content conversion has been performed, the content still must
be edited by hand. Obviously, any personal or inappropriate content must be
removed. In order to make sure none of this type of content is ever committed
to the web site, the .gitignore file is set to ignore any file name starting
with 'private', so it is highly suggested you place 'private' at the start of
the output name of any CSV file exported from Discord, and keep it at the start
of the name until the content has been edited to remove personal content and is
being pasted into the appropriate content file for the website. Note that if
something bad is committed, then fixed before being pushed, then committed
again, then finally pushed, the unfixed bad content will get pushed by the push
after the second commit just like if it had been pushed before the bad content
was fixed, since git remembers everything committed. If something bad gets
committed, the best way to deal is to delete the whole local repo entirely.
Copy just the files you were working on out of the site clone, rm -rf the
entire clone of the site, re-clone, then copy back in the files you copied out. 
If you accidentally push something bad without doing this, let me know right
away, and maybe I can fix it, only the owner (me) has privilege to rewrite the
git history on gitlab, which does not completely work anyway.

Also, there are some differences between Discord chat formatting and the
formatting used in the web site. They both use backticks for code within
a regular sentence. However, the web site will not properly use backticks
to format code outside of a regular sentence, e.g. if it is a whole line or
more of continuous code. Instead, remove the backticks, and indent the entire
code block by 4 spaces. Do not use tabs, some of the build software for the
web site is documented to not work properly with tabs. Also, web links have
a different format. For the web site, use:
  [text to show on web page](https://etc.com "Comment when hovering over it")
which can be intermingled with other text.

Line breaks in the content outside of indented code blocks are ignored, except
empty lines are used to separate paragraphs. One line directly after another
will be put together into a big paragraph. To force a line break in the middle
of a paragraph, use '<br>'. The '<br>' are automatically added by this
conversion process for line breaks in the original Discord content, so normally
the person doing the conversion does not need to worry about this unless
they want to add or remove a line break in the content for some reason.

Other html tags besides '<br>' are recognized, such as '<u>underline me</u>'.
If you get too complicated with the html tags, the entire paragraph will only
use the embedded html tags, and not do automatic formatting, and weird things
happen. If it looks ok in the build_dev development site, then the formatting
is working.

Check the _FOLDERS.txt file for the content source file locations in the
folder tree in this repo where the edited content is to be pasted.


CONVERSION SUMMARY

1. obtain Discord chat content, typically as '.csv' files
2. cvtchat (supported on Windows) to convert from '.csv' files to '.md' files
3. hand edit files to remove personal content and fix formatting differences
4. copy and paste edited content to appropriate file in _includes/content
5. use build_dev to review and edit file in _includes/content to fix any issues


CREATING CHAT '.csv' FILES FROM DISCORD

There are various ways to extract the text from a Discord chat session.
Student A has kindly provided the Discord chat text, and worked on formatting
to make it appropriate for these lessons. This repository is set up so that
any file name beginning with 'private' will be ignored by Git. The raw
markdown files have 'private' inserted at the beginning of the file name,
to make sure that raw files that might contain personal info in the Discord
chat content are less likely to be committed as site content. Any '.csv' files
will also be ignored by Git.


CONVERTING CHAT '.csv' FILES TO MARKDOWN '.md' FILES

The build_convert script in this folder builds the 'cvtchat' application. This
application creates a markdown '.md' file from a '.csv' file containing chat
content. It will use the same file name, except that 'private-' will be placed
at the beginning if it is not already there. If the output file already exists,
it will print an error message and not overwrite it. This program can process
multiple files at once. If no file name is given, it will process stdin and
write to stdout. By default it prints various status messages. The '-f' flag
forces this program to overwrite any existing output files. The '-q' flag
causes this program to not write the status messages.

The 'cvtchat' application may be run on Windows as a console mode application,
i.e. not GUI. Put 'cvtchat.exe' on Windows, right click on a '.csv' file, then
select 'cvtchat.exe' program as the application to Open the file, and it will
create the '.md' file. When built for Windows, the user presses Enter
before the application exits to allow reading any messages, since the window
created to run a console mode application is closed when the application exits.
The '-q' flag will disable reading this line as well as status messages, so
error messages may not be readable if the '-q' flag is used on Windows.
If the output file already exists, the Windows version will prompt to overwrite
it, even if '-q' is used, unless the '-f' is used to force it to overwrite it.
The 'cvtchat.exe' application file for Windows is in the 'cvtchat.zip' Windows
compressed folder in this folder.

When a chat paragraph is followed by a chat paragraph from a different person,
a line is always written in between them by the cvtchat application
giving the new person's name and the time, in a way documented in
the source to this tool. If the same person says successive paragraphs, then
if the next paragraph is less than or equal to the number of skip minutes
from the first, no such line is written. If it is more than the number of
skip minutes but less than the number of show minutes, an author and time
line will be written, but it will be commented out. If it is more than
the number of show minutes, then the author and time line will be written,
even though the author is the same as the previous chat paragraph. The skip
minutes and show minutes values may be set using -skip min and -show min. It
will add a blank line between each chat paragraph for a markdown paragraph.
If there are line breaks inside of a chat paragraph, the '<br>' markdown
sequence will be inserted before them to create a line break in the markdown
output.

Cvtchat also performs some conversions on the content.

1. It removes <br> from inside of '`' code blocks. cvtchat inserts <br>
   before every '\n' in the chat so that line breaks will be present in the
   rendered page, but inside code blocks they would be present anyway
   and the added <br> are printed literally, so they are removed.
2. It changes names to use Greek letters, except for Coffee.
3. It subtracts 9 from the hour in the date/time stamps. This converts
   from German time zone to Pacific daylight savings time. Sorry, Student A.

The '-c' flag will prevent cvtchat from performing these conversions.
The '-houroffset nhours' can be used to set the hour offset amount.


CONTENT BACKGROUND

Much of the lesson text was originally created in the files that are now in
the 'original' subdirectory.

This training was originally presented to Student A and Student Z in a section
of a Discord chat called 'tims-c-training'. The text was created in the files
being maintained in this repo, and then copy and pasted one paragraph at a time
into the chat. This was done for lessons 1-4. Lessons 5 and 7 were also
originally created the same way, but the content was copied to the appropriate
file in the _includes/content/lessons folder first and initially presented
in this web site, not in the Discord chat. The content for lesson 6 was
originally created in _includes/content/lessons/6/lesson.md and initially
presented in this web site.

As the content for lessons 1-4 was copy and pasted into the chat, it was being
edited. So the content in _includes/content/lessons of these chapters was taken
from the chat, not directly from the files now in the 'original' subdirectory.
Also, the original comments at the ends of the lessons, Questions For Teacher
material, Related But Off Topic material, and Test Programs material were
taken from the chat. All of this material needed to be removed from the chat
and converted as necessary for presentation on the website. This conversion
is what these convert tools do.

The Discord chat content was extracted by student A. Thank you very much,
Student A.
