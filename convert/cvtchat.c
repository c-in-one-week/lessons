/******************************************************************************

	cvtchat [ -q ] [ -f ] [ -c ] [ -houroffset nhours ] files

Convert Discord Chat in CSV to Markdown

For each input file, an output file with the same name as the input
file is created. The input file must end with ".csv" (in either case),
the output file will end with ".md". If the file name does not begin
with the string "private-", this string will be put at the beginning.
The output file must not already exist unless the -f flag is used.
However, on Windows, if the -f flag is not used but the output file exists,
an interactive prompt asks the user if the output file is to be overwritten.

The -q flag causes it to operate quietly, without displaying status.
On Windows, the program will display that it is completed and wait for
the user to press the ENTER button at the end. The -q flag also
suppresses this, the program just exits at the end.

The -c flag causes it to just convert CSV to markdown files. Without the -c
flag, it will also convert names to greek letters (except Coffee),
add an offset to the time including prefixing the file with the offset,
change "achim" or "zack", ignoring case, to "UserAlpha" or "UserZeta",
and remove <br> within backtick code blocks. The '-c' behaviour is consistent
with the earlier Excel version. The default houroffset is -9, which
changes Central European Time, e.g. Germany, to Pacific Time.


Revision History:
2021/6/25  - Tim S - initial version
2021/7/3   - Tim S - added name/hour/backtick fixes

******************************************************************************/


/******************************************************************************

cvtchat   convert chat from CSV to markdown
Copyright (C) 2021  T Shaw

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (COPYING-GPL-v2-txt);
if not, write to the Free Software # Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

******************************************************************************/


/*
	fields in CSV file
*/
#define CSV_AUTHORID	0
#define CSV_AUTHOR	1
#define CSV_DATE	2
#define CSV_CONTENT	3
#define CSV_ATTACHMENTS	4
#define CSV_REACTIONS	5


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <unistd.h>
#include <libgen.h>	/* including libgen changes basename/dirname version */


#ifdef _WIN64
#define ONWINDOWS 1
#endif
#ifdef _WIN32
#define ONWINDOWS 1
#endif
#ifdef _WIN16
#define ONWINDOWS 1
#endif


struct infile {
	FILE *fp ;
	int curline ;
	char *fname ;
	int qflag ;
} ;

struct cvtinfo {
	/* same user made previous comment <= skipmin, no user info saved */
	int skipmin ;
	/* same user made previous comment >= showmin, user info shown */
	int showmin ;
	int fflag ;
	int qflag ;
	int cvtnames ; /* convert names to greek letters */
	int cvthours ; /* number of hours to add to time for timezone adjust */
	int cvtbackticks ; /* only add <br> outside backtick regions */
	char **flist ;
	int nfiles ;
} ;

struct outinfo {
	FILE *fpout ;
	int contentcnt ;
	int bytecnt ;
	int linecnt ;
	int linemax ;
	time_t prevtime ;
	char prevauthor[32] ;
} ;

void doline(struct outinfo *of, struct infile *f, struct cvtinfo *ci,
	    int nfields, char **fields) ;


/*****************************************************************************
*
*	miscelaneous low level routines
*
******************************************************************************/

void cvtexit(int qflag, int status)
{
#ifdef ONWINDOWS
	if (! qflag) {
		fprintf(stderr, "Press Enter to quit: ") ;
		fflush(stderr) ;
		getc(stdin) ;
	}
#endif
	exit(status) ;
}

void err(struct infile *f, char *msg)
{
	if (! f) fprintf(stderr, "cvtchat: error- %s\n", msg) ;
	else if (! f->fname) fprintf(stderr, "cvtchat: error on line %d- %s\n",
					f->curline, msg) ;
	else fprintf(stderr, "(%s, %d) error- %s\n", f->fname,f->curline,msg) ;
	cvtexit(f->qflag, 1) ;
}

void msg(struct infile *f, char *msg)
{
	if (! f) fprintf(stderr, "cvtchat: %s\n", msg) ;
	else if (! f->fname) fprintf(stderr, "cvtchat: (%d)- %s\n",
					f->curline, msg) ;
	else fprintf(stderr, "(%s, %d) %s\n", f->fname, f->curline, msg) ;
}

void nomem(int qflag)
{
	fprintf(stderr, "cvtchat: out of memory\n") ;
	cvtexit(qflag, 3) ;
}

void bug(struct infile *f, char *msg)
{
	if (! f) fprintf(stderr, "cvtchat: software error- %s\n", msg) ;
	else if (! f->fname) {
		fprintf(stderr, "cvtchat: software error on line %d- %s\n",
				f->curline, msg) ;
	} else {
		fprintf(stderr,
		  "(%s, %d) software error- %s\n", f->fname, f->curline, msg) ;
	}

	cvtexit(f->qflag, 4) ;
}


/*****************************************************************************
*
*	parse a CSV file
*
******************************************************************************/

struct csvline {
	char *txt ;
	int curtxt ;
	int maxtxt ;
	int curpos ;
	int *fields ;
	char **ptrfields ;
	int curfield ;
	int maxfield ;
	int qflag ;
	int chkbacktick ;
	int cvtnames ;
} ;

struct csvline *newcsv(int qflag, int chkbacktick, int cvtnames)
{
	struct csvline *c ;

	if (! (c = (struct csvline *)malloc(sizeof(struct csvline))))
		nomem(qflag) ;

	c->curtxt = c->curfield = 0 ;

	c->maxtxt = 128 ;
	if (! (c->txt = (char *)malloc(c->maxtxt * sizeof(char))))
		nomem(qflag) ;
	
	c->maxfield = 4 ;
	if (! (c->fields = (int *)malloc(c->maxfield * sizeof(int *))))
		nomem(qflag) ;
	if (! (c->ptrfields = (char **)malloc(c->maxfield * sizeof(char *))))
		nomem(qflag) ;
	
	c->qflag = qflag ;
	c->chkbacktick = chkbacktick ;
	c->cvtnames = cvtnames ;
	
	return c ;
}

void alloctxt(struct csvline *c, int morechar)
{
	if (c->curtxt + morechar < c->maxtxt)
		return ;
	
	while (c->maxtxt <= c->curtxt + morechar)
		c->maxtxt *= 2 ;
	
	if (! (c->txt = (char *)realloc((void *)c->txt,
					c->maxtxt * sizeof(char)))) {
		nomem(c->qflag) ;
	}
}

int readuptonewline(struct csvline *c, struct infile *f)
{
	++f->curline ;

	do {
		alloctxt(c, 32) ; /* do loop if not enough room */
		if (! fgets(c->txt+c->curtxt, c->maxtxt-c->curtxt, f->fp)) {
			if (ferror(f->fp)) {
				fprintf(stderr,
					"cvtchat: read error on line %d\n",
					f->curline) ;
				cvtexit(c->qflag, 2) ;
			}
			return 0 ;
		}
		c->curtxt += strlen(c->txt + c->curtxt) ;
		if (c->curtxt <= 0) bug(f, "nothing read in line") ;
		if (c->curtxt >= 2 && c->txt[c->curtxt-2] == '\r'
				   && c->txt[c->curtxt-1] == '\n') {
			c->txt[c->curtxt-2] = '\n' ;
			c->txt[--c->curtxt] = '\0' ;
		}
			
	} while (c->txt[c->curtxt - 1] != '\n') ;

	return 1 ;
}

void addstring(struct csvline *c, char *s)
{
	int len = strlen(s) ;

	alloctxt(c, len) ;
	strcpy(c->txt + c->curtxt, s) ;
	c->curtxt += len ;
}

void addfield(struct csvline *c)
{
	if (c->curfield >= c->maxfield) {
		c->maxfield *= 2 ;
		if (! (c->fields = (int *)realloc((void *)c->fields,
					   c->maxfield * sizeof(int *)))) {
			nomem(c->qflag) ;
		}
		if (! (c->ptrfields = (char **)realloc((void *)c->ptrfields,
					   c->maxfield * sizeof(char *)))) {
			nomem(c->qflag) ;
		}
	}
	c->fields[c->curfield++] = c->curpos ;
}

void getcsvnonqtfield(struct csvline *c)
{
	char *s = c->txt + c->curpos ;
	char *s0 = s ;

	while (*s && *s != '\n' && *s != ',')
		++s ;
	
	while (--s >= s0 && isspace(*s))
		;
	
	c->curpos = s - c->txt + 1 ;
}

void getcsvqtfield(struct csvline *c, struct infile *f)
{
	char ch, *s ;
	int backtick ;

	backtick = 0 ;
	for (s = c->txt + c->curpos; ; ) {
		if ((ch = *s++) == '"') {
			char *t ;
			if (*s != '"') break ;
			/* squish double double-quotes up to end of line */
			t = s++ ;
			while (*t++ = ch = *s++) {
				if (ch == '\n') break ;
				if (ch == '`') backtick = ! backtick ;
				if (ch == '"') {
					if (*s != '"') break ;
					++s ;
				}
			}
			c->curtxt -= s - t ;
			if (ch == '"') {
				char *t1 = t ;
				while (*t++ = *s++)
					;
				s = t1 ;
				break ;
			}
			s = t ;
		}
		if (ch == '\n') {
			if (s != c->txt + c->curtxt)
				bug(f, "newline not at end of line") ;
			if (! (c->chkbacktick && backtick)) {
				c->txt[--c->curtxt] = '\0' ;
				addstring(c, "<br>\n") ;
			}
			c->curpos = c->curtxt ;
			if (! readuptonewline(c, f))
				err(f, "unexpected end of file") ;
			s = c->txt + c->curpos ;
		} else if (ch == '`') {
			backtick = ! backtick ;
		} else if (ch == '\0') {
			bug(f, "end of line does not have newline") ;
		}
	}

	*--s = '\0' ;

	c->curpos = s + 1 - c->txt ;
}

int getcsvfield(struct csvline *c, struct infile *f)
{
	int termc ;
	char *s = c->txt + c->curpos ;

	while (*s == '\0' || isspace(*s)) {
		if (*s == '\0' || *s == '\n') {
			*s = '\0' ;
			c->curpos = s - c->txt ;
			addfield(c) ;
			return 0 ;
		}
		++s ;
	}

	if (*s != '"') {
		c->curpos = s - c->txt ;
		addfield(c) ;
		getcsvnonqtfield(c) ;
	} else {
		c->curpos = s - c->txt + 1 ;
		addfield(c) ;
		getcsvqtfield(c, f) ;
	}

	s = c->txt + c->curpos ;

	termc = *s ;
	*s++ = '\0' ;
	while (termc && termc != '\n' && termc != ',') {
		if (! isspace(termc)) err(f, "text after quoted field") ;
		termc = *s++ ;
	}

	if (! termc) --s ;

	c->curpos = s - c->txt ;

	if (termc == ',') return 1 ;
	
	return 0 ;
}

int checknames(char *s)
{
	int c ;

	while (c = *s++) {
		if ((c == 'a' || c == 'A') && strncasecmp(s, "chim", 4) == 0)
			return 1 ;
		if ((c == 'z' || c == 'Z') && strncasecmp(s, "ack", 3) == 0)
			return 1 ;
	}
	return 0 ;
}

void fixnames(struct csvline *c, struct infile *f, int fld)
{
	char *s ;
	int n = 0 ;
	int i ;
	int any = 0 ;

	for (s = c->txt + c->fields[fld]; *s; ++s) {
		if (strncasecmp(s, "achim / involut", 15) == 0) n -= 6 ;
		else if (strncasecmp(s, "achim", 5) == 0) n += 4 ;
		else if (strncasecmp(s, "zack", 4) == 0) n += 5 ;
		else continue ;
		any = 1 ;
	}
	if (! any) return ;
	alloctxt(c, n) ;
	for (s = c->txt + c->fields[fld]; *s; ++s) {
		char *p, *r ;
		if (strncasecmp(s, "achim / involut", 15) == 0) {
			for (p = s + 9; p <= c->txt + c->curtxt; ++p)
				p[0] = p[6] ;
			c->curtxt -= 6 ;
			strncpy(s, "Student A", 9) ;
			continue ;
		}
		if (strncasecmp(s, "achim", 5) == 0) {
			r = "Student A" ;
			i = 4 ;
		} else if (strncasecmp(s, "zack", 4) == 0) {
			r = "Student Z" ;
			i = 5 ;
		} else {
			continue ;
		}
		for (p = c->txt + c->curtxt; p >= s; --p) p[i] = p[0] ;
		c->curtxt += i ;
		strncpy(s, r, strlen(r)) ;
	}
	for (i = c->curfield; --i > fld; ) c->fields[i] += n ;

	if (c->txt[c->curtxt] != '\0') bug(f, "name fix went long") ;
	for (i = 0; i < c->curfield-1; ++i) {
		if (strlen(c->txt + c->fields[i])
				>= c->fields[i+1] - c->fields[i]) {
			bug(f, "name fix field went long") ;
		}
	}
}

int getcsvline(struct csvline *c, struct infile *f)
{
	int i ;

	c->curtxt = 0 ;
	c->curpos = 0 ;
	c->curfield = 0 ;
	c->txt[0] = '\0' ;

	if (! readuptonewline(c, f))
		return 0 ;

	while (getcsvfield(c, f))
		;
	
	if (c->cvtnames) {
		if (checknames(c->txt + c->fields[CSV_CONTENT])) {
			fixnames(c, f, CSV_CONTENT) ;
		}
	}

	for (i = c->curfield; --i >= 0; )
		c->ptrfields[i] = c->txt + c->fields[i] ;
	
	return 1 ;
}


/*****************************************************************************
*
*	convert names to greek
*
******************************************************************************/

char *greekletters[] = {
	"Alpha",	/* A */
	"Beta",		/* B */
	"Gamma",	/* C */
	"Delta",	/* D */
	"Epsilon",	/* E */
	"Phi",		/* F */
	"Omega",	/* G */
	"Eta",		/* H */
	"Iota",		/* I */
	"Ja",		/* J */ /* Ja, Ja */
	"Kappa",	/* K */
	"Lambda",	/* L */
	"Mu",		/* M */
	"Nu",		/* N */
	"Omicron",	/* O */
	"Pi",		/* P */
	"Chi",		/* Q */
	"Rho",		/* R */
	"Sigma",	/* S */
	"Tau",		/* T */
	"Upsilon",	/* U */
	"Theta",	/* V */
	"Psi",		/* W */
	"Xi",		/* X */
	"Yi",		/* Y */ /* why not */
	"Zeta"		/* Z */
} ;

#define NUMLETTERS (sizeof(greekletters) / sizeof(greekletters[0]))

char *cvtusers[NUMLETTERS] ;

char *strsave(char *s)
{
	char *p ;

	if (! (p = malloc(strlen(s)+1)))
		return 0 ;
	return strcpy(p, s) ;
}

char *cvtname(char *name)
{
	int c, c0, cnt ;

	if (! cvtusers['A' - 'A']) {
		cvtusers['A' - 'A'] = strsave("Achim") ;
		cvtusers['Z' - 'A'] = strsave("Zack") ;
	}
	if (strncasecmp(name, "Coffee", 6) == 0)
		return "Coffee" ;
	if (strncasecmp(name, "Achim", 5) == 0)
		return "Alpha" ;
	if (strncasecmp(name, "Zack", 4) == 0)
		return "Zeta" ;
	
	c = *name ;
	if (! isalpha(c)) {
		c = 0 ;
	} else {
		c = toupper(c) ;
		c -= 'A' ;
	}
	
	if (c < 0 || c >= NUMLETTERS) c = 0 ;
	
	c0 = c ;
	cnt = 0 ;
	while (cvtusers[c]) {
		if (strcasecmp(name, cvtusers[c]) == 0)
			return greekletters[c] ;
		if (c >= NUMLETTERS - 1) c = 0 ;
		else ++c ;
		if (++cnt > NUMLETTERS) return greekletters[c0] ;
	}

	cvtusers[c] = strsave(name) ;
	
	return greekletters[c] ;
}


/*****************************************************************************
*
*	top level routines
*
******************************************************************************/

void outname(char fnambuf[], int len, char *infile)
{
	char *dn, *bn ;
	char dnambuf[FILENAME_MAX] ;
	char bnambuf[FILENAME_MAX] ;

	strncpy(dnambuf, infile, sizeof(dnambuf)) ;
	dnambuf[sizeof(dnambuf)-1] = '\0' ;
	strncpy(bnambuf, infile, sizeof(bnambuf)) ;
	bnambuf[sizeof(bnambuf)-1] = '\0' ;

	dn = dirname(bnambuf) ;
	bn = basename(dnambuf) ;
	if (! dn || ! *dn)
		err(NULL, "unable to process directory in file name") ;
	if (! bn || ! *bn)
		err(NULL, "unable to process file name") ;
	
	if (strcasecmp(bn + strlen(bn) - 4, ".csv") != 0)
		err(NULL, "input file name does not end with '.csv'") ;
	
	fnambuf[0] = '\0' ;

	if (strcmp(dn, ".") != 0) {
		strncat(fnambuf, dn, len-1) ;
#ifdef ONWINDOWS
		strncat(fnambuf, "\\", len-1) ;
#else
		strncat(fnambuf, "/", len-1) ;
#endif
	}

	if (strncmp(bn, "private-", 8) == 0) {
		strncat(fnambuf, bn, len-1) ;
	} else {
		strncat(fnambuf, "private-", len-1) ;
		strncat(fnambuf, bn, len-1) ;
	}

	fnambuf[len-1] = '\0' ;

	if (strcasecmp(fnambuf + strlen(fnambuf) - 4, ".csv") != 0)
		err(NULL, "'.csv' ending on input file name lost") ;
	
	strcpy(fnambuf + strlen(fnambuf) - 4, ".md") ;
}

int chkexists(char *fname)
{
	return access(fname, F_OK) == 0 ;
}

void parseargs(struct cvtinfo *ci, int argc, char *argv[])
{
	int i ;
	int errflag = 0 ;
	char fnambuf[2*FILENAME_MAX] ; /* should be long enough */
	
	ci->skipmin = 2 ;
	ci->showmin = 7 ;
	ci->fflag = 0 ;
	ci->qflag = 0 ;
	ci->flist = 0 ;
	ci->nfiles = 0 ;
	ci->cvtnames = 1 ;
	ci->cvthours = -9 ;
	ci->cvtbackticks = 1 ;

	for (i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-f") == 0) ci->fflag = 1 ;
		else if (strcmp(argv[i], "-q") == 0) ci->qflag = 1 ;
		else if (strcmp(argv[i], "-c") == 0) {
			ci->cvtnames = 0 ;
			ci->cvthours = 0 ;
			ci->cvtbackticks = 0 ;
		} else if (strcmp(argv[i], "-houroffset") == 0) {
			if (i >= argc - 1
			    || ! (isdigit(*argv[i+1]) || *argv[i+1] == '-'))
				err(NULL, "need numeric timezone adjust") ;
			ci->cvthours = atoi(argv[i+1]) ;
			++i ;
		} else if (strcmp(argv[i], "-skip") == 0) {
			if (i >= argc - 1 || ! isdigit(*argv[i+1]))
				err(NULL, "need numeric skip parameter") ;
			ci->skipmin = atoi(argv[i+1]) ;
			++i ;
		} else if (strcmp(argv[i], "-show") == 0) {
			if (i >= argc - 1 || ! isdigit(*argv[i+1]))
				err(NULL, "need numeric show parameter") ;
			ci->showmin = atoi(argv[i+1]) ;
			++i ;
		} else {
			if (ci->nfiles <= 0) {
				ci->flist = (char **)malloc(sizeof(char *)) ;
			} else {
				ci->flist = (char **)realloc(
					(void *)ci->flist,
					(ci->nfiles + 1) * sizeof(char *)) ;
			}
			if (! ci->flist) nomem(ci->qflag) ;
			ci->flist[ci->nfiles++] = argv[i] ;
		}
	}

#ifndef ONWINDOWS
	if (! ci->fflag) {
		for (i = 0; i < ci->nfiles; ++i) {
			outname(fnambuf, sizeof(fnambuf), ci->flist[i]) ;
			if (chkexists(fnambuf)) {
				fprintf(stderr,
	"cvtchat: output file '%s' already exists. Use '-f' to overwrite.\n",
						fnambuf) ;
				errflag = 1 ;
			}
		}
	}
#endif

	if (errflag) cvtexit(ci->qflag, 1) ;
}

FILE *openoutfile(struct cvtinfo *ci, int n)
{
	FILE *fp ;
	int existed ;
	char fnambuf[2*FILENAME_MAX] ;

	outname(fnambuf, sizeof(fnambuf), ci->flist[n]) ;

	existed = chkexists(fnambuf) ;

#ifdef ONWINDOWS
	if (existed && ! ci->fflag) {
		char ch ;
		do {
			char ibuf[32] ;
			fprintf(stderr, "'%s' exists. Overwrite? (y/n/q) ",
					fnambuf) ;
			fflush(stderr) ;
			ibuf[0] = '\0' ;
			fgets(ibuf, sizeof(ibuf), stdin) ;
			ch = tolower(ibuf[0]) ;
		} while (ch != '\0' && ch != 'y' && ch != 'n' && ch != 'q') ;
		if (ch == 'q') {
		    fprintf(stderr, "'q' typed at overwrite file prompt.\n") ;
		    cvtexit(ci->qflag, 1) ;
		}
		if (ch == '\0') {
		    fprintf(stderr, "EOF at overwrite file prompt.\n") ;
		    cvtexit(ci->qflag, 1) ;
		}
		if (ch == 'n') return NULL ;
	}
#endif

	/* open output file using 'b' flag so that '\n' will not be
	   converted to '\r' or '\r' '\n' on some systems, since
	   result will be taken to a Linux system where only '\n' is needed */
	if ((fp = fopen(fnambuf, "wb")) == NULL) {
		fprintf(stderr, "cvtchat: unable to write '%s'\n", fnambuf) ;
		cvtexit(ci->qflag, 1) ;
	}

	if (! ci->qflag) {
		fprintf(stderr, "Converting '%s' into '%s'.\n",
				ci->flist[n], fnambuf) ;
		if (existed) {
			fprintf(stderr, "Output '%s' being overwritten.\n",
				fnambuf) ;
		}
	}

	return fp ;
}

void dofile(struct cvtinfo *ci,
	    struct infile *f,
	    struct csvline *c,
	    FILE *fpout)
{
	struct outinfo of ;

	of.fpout = fpout ;
	of.prevtime = 0 ;
	of.prevauthor[0] = '\0' ;
	of.contentcnt = 0 ;
	of.bytecnt = 0 ;
	of.linecnt = 0 ;
	of.linemax = 0 ;

	f->curline = 0 ;

	if (ci->cvthours) {
		void putnl(struct outinfo *) ;
		fprintf(fpout,
		    "{%% comment %%}FIXCONVERT-HOURS=%d{%% endcomment %%}",
		    ci->cvthours) ;
		putnl(&of) ;
	}

	while (getcsvline(c, f)) {
		doline(&of, f, ci, c->curfield, c->ptrfields) ;
	}
	
	if (! ci->qflag) {
	    fprintf(stderr, "Processed %d messages totalling %d bytes.\n",
			of.contentcnt, of.bytecnt) ;
	    fprintf(stderr, "Wrote %d lines, longest line %d bytes.\n",
	    		of.linecnt, of.linemax) ;
	}
}
	
void procfiles(struct cvtinfo *ci)
{
	struct csvline *c = newcsv(ci->qflag, ci->cvtbackticks, ci->cvtnames) ;
	int i ;
	struct infile f ;

	f.qflag = ci->qflag ;

	if (ci->nfiles == 0) {
		f.fname = NULL ;
		f.fp = stdin ;
		if (! ci->qflag) {
			fprintf(stderr, "Converting <stdin> into <stdout>.\n");
		}
		dofile(ci, &f, c, stdout) ;
		return ;
	}
	
	for (i = 0; i < ci->nfiles; ++i) {
		FILE *fpout = NULL ;
		if (ci->nfiles <= 1) f.fname = NULL ;
		else f.fname = ci->flist[i] ;
		f.fp = fopen(ci->flist[i], "r") ;
		if (f.fp == NULL) {
			fprintf(stderr, "cvtchat: unable to read '%s'\n",
					ci->flist[i]) ;
			cvtexit(ci->qflag, 1) ;
		}
		fpout = openoutfile(ci, i) ;
		if (! fpout) continue ;
		dofile(ci, &f, c, fpout) ;
		fclose(f.fp) ;
		f.fp = NULL ;
		fclose(fpout) ;
	}
}

int main(int argc, char *argv[])
{
	struct cvtinfo ci ;

	parseargs(&ci, argc, argv) ;

	procfiles(&ci) ;

	cvtexit(ci.qflag, 0) ;

	return 0 ;
}


/*****************************************************************************
*
*	process a parsed line
*
******************************************************************************/

void putnl(struct outinfo *of)
{
	FILE *fpout = of->fpout ;

	putc('\n', fpout) ;
	++ of->linecnt ;
}

int getmonth(char *mon)
{
	char *mlist[] = {
		"jan", "feb", "mar", "apr", "may", "jun",
		"jul", "aug", "sep", "oct", "nov", "dec",
		0 } ;
	char **p ;
	for (p = mlist; *p; ++p)
		if (strncasecmp(mon, *p, strlen(*p)) == 0) return p - mlist ;
	return -1 ;
}

void doline(struct outinfo *of, struct infile *f, struct cvtinfo *ci,
	    int numfields, char *fields[])
{
	char *s ;
	time_t curtime ;
	long curday ;
	char *curauthor ;
	char *curdate ;
	char *curcontent ;
	int n ;
	int printwho ;
	int deltatime ;
	struct tm *tp ;
	char mon[12], ampm[4] ;
	struct tm tm ;

#ifdef TESTING_CSV_PARSER
	int i ;

	printf("numfields = %d\n", numfields) ;
	for (i = 0; i < numfields; ++i) {
		printf("fields[%d] = >%s<\n", i, fields[i]) ;
	}
	return ;
#endif

	if (numfields < 3)
		err(f, "insufficient fields in CSV file") ;
	
	curauthor = fields[CSV_AUTHOR] ;
	curdate = fields[CSV_DATE] ;
	curcontent = fields[CSV_CONTENT] ;

	if (strcmp(curauthor, "Author") == 0 && strcmp(curdate, "Date") == 0)
		return ;
	
	if (CSV_ATTACHMENTS < numfields
	    && *fields[CSV_ATTACHMENTS]
	    && strcmp(fields[CSV_ATTACHMENTS], "Attachments") != 0) {
	        fprintf(stderr, "Attachments: %s\n", fields[CSV_ATTACHMENTS]) ;
		err(f, "comment has Attachments") ;
	}

	if (CSV_REACTIONS < numfields
	    && *fields[CSV_REACTIONS]
	    && strcmp(fields[CSV_REACTIONS], "Reactions") != 0) {
	        fprintf(stderr, "Reactions: %s\n", fields[CSV_REACTIONS]) ;
		msg(f, "Warning: skipping reactions to comment.") ;
	}
	
/**
  "Coffee#7156", "30-May-2021 5:26:00 PM"
  -> {{ UserCoffee }}{{ "2021-5-30 5:26:00 PM" | date: "%l:%M %P" | strip }}{{ VarPostDate }}
  -> either inside or after liquid comment if same user as previous
**/

	if (! ci->cvtnames) {
		if (s = strchr(curauthor, '#')) *s = '\0' ;
		if (s = strchr(curauthor, ' ')) *s = '\0' ;
	}

	/* not using strptime() because it is not so portable */
	memset(&tm, 0, sizeof(tm)) ;
	tm.tm_isdst = -1 ;
	n = sscanf(curdate, "%d-%11[^-]-%d %d:%d:%d %3s",
		   &tm.tm_mday, mon, &tm.tm_year,
		   &tm.tm_hour, &tm.tm_min, &tm.tm_sec, ampm) ;
	if (n == 5) {
		/* try again without the seconds */
		n = sscanf(curdate, "%d-%11[^-]-%d %d:%d %3s",
			   &tm.tm_mday, mon, &tm.tm_year,
			   &tm.tm_hour, &tm.tm_min, ampm) ;
		tm.tm_sec = 0 ;
		if (n == 6) ++n ;
	}
	tm.tm_mon = getmonth(mon) ;
	if (n != 7 || tm.tm_mon < 0) {
		err(f, "invalid date/time") ;
	}
	if (strcasecmp(ampm, "am") == 0) {
		if (tm.tm_hour == 12) tm.tm_hour = 0 ;
	} else if (strcasecmp(ampm, "pm") == 0) {
		if (tm.tm_hour < 12) tm.tm_hour += 12 ;
	} else {
		err(f, "invalid am/pm in time") ;
	}
	tm.tm_year += 100 ;

	curtime = mktime(&tm) ;

	curtime += (60 * 60) * ci->cvthours ;
		
	printwho = 0 ;
	
	if (strcmp(curauthor, of->prevauthor) != 0) printwho = 1 ;

	if (of->prevtime == 0) deltatime = 0 ;
	else deltatime = curtime - of->prevtime ;

	if (deltatime > ci->skipmin * 60) printwho = 1 ;

	if (printwho) {
		char *useauthor ;
		char prebuf[128] ;
		char postbuf[128] ;
		char dbuf[64] ;
		char tbuf[32] ;

		/* sequential posts with different author names that convert
		   to the same name will still force out a User line **/
		if (ci->cvtnames) {
			useauthor = cvtname(curauthor) ;
		} else {
			useauthor = curauthor ;
		}
		prebuf[0] = '\0' ;
		postbuf[0] = '\0' ;
		if (strcmp(curauthor, of->prevauthor) == 0) {
			if (deltatime < ci->showmin * 60) {
				sprintf(prebuf,
						"{%% comment %%} thenwait %d ",
						(deltatime + 59) / 60) ;
				strcpy(postbuf, "{% endcomment %}") ;
			} else {
				sprintf(prebuf,
			    "{%% comment %%} thenwait %d {%% endcomment %%}",
					(deltatime + 59) / 60) ;
			}
		}

		memset(&tm, 0, sizeof(tm)) ;
		/* mktime uses the local time zone for conversion,
		   so use localtime for the reverse  conversion,
		   even though time zone that should be used is not clear */
		tp = localtime(&curtime) ;
		sprintf(dbuf, "%d-%d-%d %d:%02d:%02d %s",
			tp->tm_year + 1900, tp->tm_mon + 1, tp->tm_mday,
			tp->tm_hour == 0? 12:
			    (tp->tm_hour <= 12? tp->tm_hour: tp->tm_hour - 12),
			tp->tm_min, tp->tm_sec,
			tp->tm_hour < 12? "AM": "PM") ;

		putnl(of) ;
		fputs(prebuf, of->fpout) ;
		fprintf(of->fpout, "{{ User%s }}{{ \"%s\"", useauthor, dbuf) ;
		fputs(" | date: \"%l:%M %P\" | strip }}{{ VarPostDate }}",
			of->fpout) ;
		fputs(postbuf, of->fpout) ;
		putnl(of) ;
	}

	of->prevtime = curtime ;
	strcpy(of->prevauthor, curauthor) ;

	putnl(of) ;

	n = strlen(curcontent) ;
	if (n > of->linemax) of->linemax = n ;
	for (s = strchr(curcontent, '\n'); s; s = strchr(s+1, '\n'))
		++ of->linecnt ;
	
	fputs(curcontent, of->fpout) ;
	putnl(of) ;

	++ of->contentcnt ;
	of->bytecnt += strlen(curcontent) ;
}
