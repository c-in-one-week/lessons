---
layout: lesson
title: 5. A Fifth
---

<!---
5-a-fifth/index.md   lesson 5 in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/5/lesson.md -%}

{% include content/extra/5/comments.md %}
