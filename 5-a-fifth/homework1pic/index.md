---
layout: lesson
title: 5. A Fifth - Homework 1 Picture
---

<!---
5-a-fifth/homework1pic/index.md   lesson 5 homework 1 picture in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

{% include lessonhead.html %}

{%- include content/lessons/5/homework1pic.md %}
