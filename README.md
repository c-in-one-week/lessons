Learn C in 7 days, not too much each day.

The lesson names are not related to the content.

1. In the beginning
2. Number two
3. Tres amigos
4. Four square
5. A fifth
6. Highway 66
7. 7th Heaven

For fun and profit!


Gitlab c-in-one-week.com home:
	git@gitlab.com:c-in-one-week/lessons.git

Deployed production site files are kept in site-pages.git.

To further develop this in a clone of this site:
- Use `./install_dev` to make sure dev setup installed.
- Use `./build_dev` for local test builds.
These development scripts are not specific to c-in-one-week.
They may be helpful for developing any web site that uses jekyll.
These scripts set up a git pre-push hook so git push locally rebuilds
the production site and pushes it to the deployment repo.
These scripts were written under Debian.

A Gitlab runner is needed to push site changes through Gitlab CI/CD.
A local Gitlab runner is set up by `./create_runner name token`.
The corresponding runner on the Gitlab site must have already been
created within the c-in-one-week Gitlab project. Doing this requires
the relevant access to the Gitlab c-in-one-week project.

The file `./_FOLDERS.txt` gives the location of all content,
and all of the rest of the file locations in this tree.

Please let me know and push when you make it better!
My email account: info, domain: c-in-one-week.com

Lesson content is licensed under CC-BY 4.0
This text (README.md) is licensed under CC-BY 4.0
Development scripts are licensed under GPL-v2
