---
layout: front
title: Lessons
pageTitle: C In One Week
---

<!---
index.md   top level Lessons page in c-in-one-week
Copyright (c) 2021  T Shaw
This work is licensed under a CC BY 4.0 license.
-->

<p>
<br/>
<center>Learn C in One Week!</center><br/>
<center>7 quick lessons, no charge at all.</center><br/>
<center><strong>For Fun and Profit!</strong></center><br/>
</p>

<p>
Learn C in 7 days, not too much each day.<br/>
<br/>
The lesson names are not related to the content.<br/>
<br/>
 {% for item in site.data.lessons %}
  <a href="{{ item.link }}">{{ item.name }}</a><br/>
 {% endfor %}
<br/>
For fun and profit!<br/>
<small>
<br/>
This site was created from scratch in just a few days, less than one week. I also got my 2nd dose during that time, so I was out of it some of those days. I wrote most of the lessons the prior week. This site is very rough! See
{%- comment -%} A markdown link here does not work; too much html disables md?
{%- endcomment %}
<a href="/source/" title="This site's source">Source</a>
for some things that need fixing, and how you can help fix them!
</small>
</p>
