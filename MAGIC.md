Really, just a few small tricks.
But this took me days to implement.

Deployed site content magically updates if someone is looking
at a site page when new content is pushed.

The trick: hooks are put into deployed site pages so that they
check periodically if new content has been deployed. This check
is relatively efficient: there is just a site build number at
the site root, which is bumped at each deployment. Then, only
if a new deployment has occured, a hash of the page being viewed
is compared against a hash on the site. Then, if they do not
match, the new content is loaded in the background.

The site pages use a timer to check the site build number in
a browser and system independent way. The time between checks
is based on the time of the last build. If the site has not
been rebuilt for months, it does not check very often.
If the site was rebuilt minutes ago, it checks quite often.

For this to work without glitches, all of the assets of the site,
such as images on the site pages, or any scripts or CSS, are
versioned automatically when changed source for them is pushed.
The asset names are automatically changed to the version names
in links to the assets in site content. Old assets are kept for
at least 7 days after they are replaced, up to 10 versions each,
so if a user clicks on something involving an asset on a page
which has not been updated yet, the old asset is still there.

This magic is independent of the lesson content pages.
It is performed by the javascript reloadpages.js.
Numbering and versioning is done by CI/CD deployment scripts.

Lesson content is licensed under CC-BY 4.0
This text (MAGIC.md) is licensed under CC-BY 4.0
Development scripts are licensed under GPL-v2
